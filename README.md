# Kayhan Space Pathfinder SDK

This repository is released under the APACHE 2.0 license. By using this repository, you agree to the terms of that license, and acknowledge that you are doing so at your own risk. While Looker has developed and tested this code internally, we cannot guarantee that the open-source tools used by the scripts in this repository have not been modified with malicious code.

## Overview
This repository contains:

- Pathfinder Python libraries
- Guide
- Examples