SDK Changelog
^^^^^^^^^^^^^

1.6.1
#####################

**Comments**

**Added**

* Added more fields to Ephemeris model, such as `ephemeris_source` and `data_format`.

**Changed**

**Fixed**

1.6.0
#####################

**Comments**

Support for avoidance maneuver trigger configuration and management has been added, see https://app.kayhan.io/docs/pathfinder/preferences/triggers/. Additionally, administration support is now available through the SDK, see https://app.kayhan.io/docs/pathfinder/administration/, and asset management is now supported, see https://app.kayhan.io/docs/pathfinder/asset-management/.

**Added**

* Added list, create, update and delete functionality for avoidance maneuver triggers.
* Added additional examples to documentation related to screenables, including screening against catalogs, items within catalogs, the ephemeris repository, and more.
* Added support for screening against the operational ephemeris repository, see :doc:`pages/sdk/screening`.
* Added new documentation page for Triggers, see :doc:`pages/sdk/trigger`.
* Adding support for general administration features, such as organization, user and api key management, see https://app.kayhan.io/docs/pathfinder/administration/
* Added new documentation page for Administration, see :doc:`pages/sdk/administration`.
* Adding support for general asset management features, such as constellation and RSO management , see https://app.kayhan.io/docs/pathfinder/asset-management/
* Added new documentation page for Administration, see :doc:`pages/sdk/assetmanagement`.

**Changed**

* Moved ``screening`` related documentation from ``getting started`` page to ``screening`` page.
* Modified ``create_screening`` in SDK and create ``screening`` in CLI to add new parameter for utilizing the ephemeris repository as a screenable.
* Modified existing CLI DELETE actions to include a confirmation step.

**Fixed**

* Corrected changelog version from last release, it is correctly shown as ``1.5.0`` instead of ``1.4.6``.
* Added ``profile_id`` information to ``kayhan.sdk.avoidance.avoidance_client.delete_maneuver_profile``

1.5.0
#####################

**Comments**

This release focuses on support for functionality related to Pathfinder's avoidance maneuver capabilities. Support has been added for creating maneuver profiles, adding maneuver tradespaces and manually generating avoidance maneuvers. Additionally new attributes and actions have been added for ephemeris.

**Added**

* Added support to set ``context`` and ``designation`` for ephemeris, see https://app.kayhan.io/docs/pathfinder/operational-data/#ephemeris-metadata
* Added support for avoidance maneuver profile configuration and management, see :doc:`pages/sdk/avoidance`
* Added support for appending avoidance maneuver tradespaces to existing scenarios
* Added support for manually generating an avoidance maneuver scenario for a CDM without an existing maneuver scenario, see :doc:`pages/sdk/avoidance`
* Added support for generating an ephemeris from an avoidance maneuver plan, which by default will designate the new ephemeris as `OPERATIONAL`, see https://app.kayhan.io/docs/pathfinder/conjunction-events/mitigation/#building-on-operational-cdm-reports
* Added support for designating existing ephemeris as `OPERATIONAL`, see https://app.kayhan.io/docs/pathfinder/operational-data/#operational-ephemeris

**Changed**

* Added ``ignition_duration_hr`` to ``kayhan.sdk.avoidance.models.Maneuver``, ``ignition_duration`` will be deprecated in a future release
* Moved ``GeminiScreening`` model from ``kayhan.sdk.avoidance.models`` to ``kayhan.sdk.screening.models``

**Fixed**

* Fixed ``list_ephemerides`` omitting count parameter

**Deprecated**

.. warning::
    Items here will be removed in a future release, please update accordingly.

* ``ignition_duration`` is marked for deprecation in ``kayhan.sdk.avoidance.models.Maneuver``, please use ``ignition_duration_hr`` instead.
