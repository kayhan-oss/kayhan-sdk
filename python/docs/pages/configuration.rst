Configuration
^^^^^^^^^^^^^

Configuration Methods
#####################

Settings and configuration for the Kayhan SDK, such as credentials and timeouts, can be configured in three different ways:

**Configuration File**


The recommended way for most users to configure the Kayhan SDK is through a JSON configuration file. By setting values in this file, you can set any of the configurations exposed in the :py:mod:`settings <kayhan.sdk.settings>` module.

By default, the SDK looks in your current directory for a file called ``config.json``. However, you can set the ``KAYHAN_CONFIG_PATH`` variable in your shell environment to tell the SDK to load an explicit config file.

To use the configuration file, you can set a key in the ``config.json`` for any of the fields available in :py:mod:`settings <kayhan.sdk.settings>`, with the setting's name capitalized. For example, to configure the ``auth_username`` variable in a config file, you can set:

.. code:: json

    {
        "AUTH_USERNAME": "me@myorganization.space"
    }

------------

**Environment variables**

You can also set any field in :py:mod:`settings <kayhan.sdk.settings>` using environment variables. To set a given field, you can set an environment variable with the setting's name in caps prefixed with the string ``KAYHAN_``. For exmaple, to configure the ``auth_username`` variable in a config file, you can set:

.. code:: bash

    KAYHAN_AUTH_USERNAME="me@myorganization.space"

------------

**Python Code**

You can also directly set any field in :py:mod:`settings <kayhan.sdk.settings>` at runtime in a Python script which uses the Kayhan SDK. For example, to configure the ``auth_username`` variable, you can use:

.. code:: python

    kayhan.sdk.settings.settings.auth_username = "me@myorganization.space"

Authentication
##############

Because the Kayhan SDK is an API client for the Kayhan Pathfinder API suite, use of the Kayhan SDK requires authentication with the Kayhan platform. You may authenticate with the Kayhan SDK in one of two ways:

1. With a username/password flow, using ``settings.auth_method = "user"`` **(default)**
    * ``settings.auth_user`` is required (typically your email address)
    * ``settings.auth_password`` is required

2. With a machine-to-machine client ID / client secret, using ``settings.auth_method = "token"``
    * ``settings.auth_id`` is required (your client id)
    * ``settings.auth_secret`` is required (your client secret)
