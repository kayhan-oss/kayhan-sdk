.. _SDK Trigger:

Triggers
=========

Overview
########

Triggers are utilized in Pathfinder to automate advanced features that generate some resource based on certain conditions being met, such a automatically generating an avoidance maneuver if risk thresholds are exceeded within a specific window near TCA. For additional information, see `Trigger Documentation <https://app.kayhan.io/docs/pathfinder/preferences/triggers/>`_.

.. note::
    Additional trigger types and automation features will be added in future releases of Pathfinder and the Kayhan SDK.

Trigger configuration
#####################

Outside some of the more self explanatory attributes, such as ``title``, triggers include some complex attributes such as ``types`` and ``assets`` that are utilized to determine which conditions execute the trigger and what assets are impacted by the trigger.

``types`` is a list of trigger types that are being applied to the trigger record, however only one trigger type is available today, that being ``AVOIDANCE_MANEUVER``. Within an individual type, other fields include ``max_miss_distance``, ``max_miss_distance_unit``, ``min_collision_probability``, ``md_pc_logical_operator``, ``max_window_under_tca`` and ``max_window_under_tca_unit``.

In order for a trigger to execute, some conditions need to be configured. Two attributes that must be provided are ``max_window_under_tca`` and ``max_window_under_tca_unit``, this value indicates the window before TCA that should be considered for trigger execution if other thresholds are met. The window can be between 1 hour and 5 days, inclusive. 

Once the window is established, both miss distance and probably of collision, or either one, can be added as conditions for trigger execution. If using both values, ``md_pc_logical_operator`` can either be set to ``AND`` or ``OR``, indicating if both thresholds must be met or only one to execute the trigger.

A list of ``assets`` must also be provided to indicate which assets will be impacted by a trigger once it executes. Within an individual asset configuration, the following attributes are present: ``constellation_slug``, ``include``, ``exclude`` and ``link_constellation``. 

The ``constellation_slug`` is used to identify which constellation the assets belong to. Both the ``include`` and ``exclude`` properties are optional lists, where each element in the list is the NORAD ID of the spacecraft. These lists can be utilized to fine tune which assets within a constellation are affected by the trigger. If both lists are omitting or set to ``None``, ``link_constellation`` will default to ``True``. Linking a constellation means that all assets that are added to the constellation in the future will also be associated with this trigger.

.. warning::
     Only one ``linked`` trigger per ``constellation_slug`` can exist at a time, and creating a new one will override other non-linked triggers for that specific trigger ``type``. 

In practice, it may make more sense to create additional constellations and move assets to specific constellations that align with specific triggers, see `Asset Management Documentation <https://app.kayhan.io/docs/pathfinder/asset-management/>`_.

.. note::
    Asset Management functionality is only available in the `Pathfinder web application <https://app.kayhan.io/asset_management/assets>`_ at this time, but support will be added to the Kayhan SDK in the future for constellation management.

