Getting Started - SDK
=====================

Calling the SDK
###############

Once you have installed the SDK into your Python environment, you can import the ``kayhan`` package:

.. code-block:: python

    import kayhan

Most of the core operations using the Kayhan SDK are performed using the ``KayhanClient`` object, which is designed to be used as a context manager.

.. code-block:: python

    from kayhan.sdk.client import KayhanClient

    with KayhanClient() as client:
        screenings = client.screening.list_screenings()
