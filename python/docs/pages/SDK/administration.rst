.. _SDK Administration:

Administration
==============

Overview
########

Administration features allow for users, organizations and API keys to be managed, see `Administration Documentation <https://app.kayhan.io/docs/pathfinder/administration/>`_.

Users
#####

Users can be added and maintained, password reset requests can be executed, and users can be deactivated or re-activated, for additional information, see  `Administration Documentation - User Accounts <https://app.kayhan.io/docs/pathfinder/administration/#user-accounts>`_.

To create a new user for your organization, provide a valid configuration:

.. code-block:: python

    ...
    user = client.administration.create_organization_user(
        role="USER"
        username='new user',
        email='new_user@org.com',
    )

.. note::
    There are two roles available for users to be classified as, ``USER`` and ``ADMIN``. An ``ADMIN`` can edit information, permissions and the role of users and other admins.

Organization
############

Organization configuration allows for setting or updating a point of contact, opting into or out of coordination or directory listings, controlling ephemeris repository access, and setting risk thresholds for probability of collision and/or miss distance values for use within the Pathfinder suite. 

To learn more about coordinated functionality, the Operator Directory, Operational ephemeris and the Ephemeris Repository, see `Coordination Documentation <https://app.kayhan.io/docs/pathfinder/preferences/coordination/>_`.

For additional information about risk thresholds, their purpose and configuration methods, see `Risk Threshold Documentation <https://app.kayhan.io/docs/pathfinder/preferences/thresholds/>_`.

.. note::
    Currently, risk thresholds are most useful in the `Pathfinder web application <https://app.kayhan.io/assets>`_ as they help determine what conjunctions are deemed most important. In the future, these thresholds will also be tied to avoidance maneuver generation and notifications.

API Keys
########

API keys can be added, updated, removed or have secrets rotated, these API keys can be used for automating processes within Pathfinder, such as automated ephemeris uploads to the Ephemeris Repository. For additional information, see `Administration Documentation - API Keys <https://app.kayhan.io/docs/pathfinder/administration/#api-keys>`_.

To create a new API key, simply pass a name for the key:

.. code-block:: python

    ...
    api_key = client.administration.create_api_key(
        name="api_key"
    )

.. warning::
     ``client_secret`` is only returned once after a new API key is created, it will not be available after and if the secret is lost, the secret will need to be rotated in order to continue to use the key.