.. _SDK Asset Management:

Asset Management
================

Overview
########

Asset Management provides organizations and users the ability to create and maintain constellations and to maintain RSOs or assets within those constellations, see `Asset Management Documentation <https://app.kayhan.io/docs/pathfinder/asset-management/>`_.

Constellations
##############

Constellations are used for grouping an organization's assets, each asset or RSO can belong to a single constellation, and certain functionality, such as notifications or avoidance maneuver triggers, can be easily applied to a single constellation, see  `Asset Management Documentation - Constellations <https://app.kayhan.io/docs/pathfinder/asset-management/#constellations>`_.

Note that all organizations are generated with a single, default constellation in Pathfinder. The default constellation initially houses all organization assets, but those assets can be moved between new constellations. The default constellation cannot be deleted, but other constellations can be.

To create a new constellation for your organization, provide a valid configuration:

.. code-block:: python

    ...
    constellation = client.asset_management.create_constellation(
        name="decayed assets constellation"
    )

.. note::
    Utilize ``client.asset_management.batch_update_rsos`` to bulk re-assign asset ownership to a constellation.


RSOs | Assets
#############

Assets, or RSOs, are the individual objects that belong to an organization and also belong to a single constellation owned by the organization, see `Asset Management Documentation - Assets <https://app.kayhan.io/docs/pathfinder/asset-management/#assets>`_ for additional details related to assets.

To update a list of assets, provide a list asset configuration objects:

.. code-block:: python

    ...
    updated_rsos = client.asset_management.batch_update_rsos(
        rsos_config=[
            {
                "norad_cat_id": 12345,
                "constellation": {
                    "slug": "decayed assets constellation",
                },
                "mass_kg": 55,
            },
            {
                "norad_cat_id": 67891,
                "constellation": {
                    "slug": "decayed assets constellation",
                },
            },
        ]
    )


