SDK
===

Overview
########
The Kayhan SDK is a Python client library used to interact with Kayhan's web-based spaceflight safety services. You can use it to automate the workflow of contacting Kayhan's APIs on your organization's behalf to perform screenings and other spaceflight safety operations so that your team only needs to get manually involved when action is needed to protect your spacecraft.

.. toctree::
    :maxdepth: 4

    Getting Started <gettingstarted>
    Screening <screening>
    Propagation <propagation>
    Avoidance Maneuvers <avoidance>
    Triggers <trigger>
    Asset Management <assetmanagement>
    Administration <administration>
