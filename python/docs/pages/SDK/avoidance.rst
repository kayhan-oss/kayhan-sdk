Avoidance Maneuvers
===================

Pathfinder's Avoidance Maneuver system can suggest appropriate maneuver plans to mitigate the risk of an upcoming conjunction.

.. note::
    Using Pathfinder's Avoidance Maneuver system currently requires your constellation to authorize Kayhan Pathfinder to access its Conjunction Data Message (CDM) data
    from `Space-Track <https://www.space-track.org>`_. For more information, please contact us at help@kayhan.space.

Triggers
########

Pathfinder's Avoidance Maneuver system can be configured to autonomously generate a Tradespace of maneuver suggestions upon receipt of a CDM
which violates a given safety threshold (e.g. its collision probability (PC) is above a given "safe" value). 

Triggers can be configured using the `Pathfinder web application <https://app.kayhan.io/settings/triggers>`_, the ``create trigger`` command with the CLI or the SDK :py:mod:`create_trigger <kayhan.sdk.avoidance.avoidance_client.AvoidanceClient.create_trigger>`. An example using the SDK is provided below:

.. code-block:: python

    ...
    trigger = client.avoidance.create_trigger(
        slug='constellation_slug',
        title='Avoidance Maneuver Trigger',
        include='1234,5678',
        exclude='3456',
        max_miss_distance=2.5,
        max_miss_distance_unit='km'
    )

See :ref:`Pathfinder triggers <SDK Trigger>` for additional information about configuration and management of Avoidance Maneuver Triggers.

Avoidance Maneuver Profiles
###########################

While Triggers are used to configure when an Avoidance Maneuver scenario is automatically generated, Profiles are used to determine the type of scenario generation that occurs. Avoidance Maneuver Profiles can be configured using the `Pathfinder web application <https://app.kayhan.io/asset_management/maneuver_profiles>`_ or the SDK. Once created, profiles can be applied to spacecraft and are used to determine the algorithm and other constraints that should be applied during avoidance maneuver generation.

.. code-block:: python

    avoidance_maneuver_profile = client.avoidance.create_maneuver_profile(
        name,
        maneuver_algo_type,
        maneuver_algo,
        burn_type,
        tradespace_algorithm,
        fidelity_type,
        fidelity_unit,
        fidelity_value
    )


Spacecraft Constraints and Configuration
########################################

Each spacecraft and mission has a set of physical characteristics and operational constraints which may impact the ability of the spacecraft to perform a particular maneuver plan.
Pathfinder requires some of these values to ensure that the plans generated are usable by the operator.

In addition, Pathfinder allows you to set a unique configuration of parameters for the Avoidance Maneuver system for each spacecraft in your constellation. This includes selecting
an appropriate avoidance maneuver algorithm (either a Fixed Delta-V, our Optimal Avoidance Maneuver (OAM) algorithm or Differential Drag) as well as the configuring parameters for the selected
algorithm. 

At present, these parameters can be configured within the `Pathfinder web application <https://app.kayhan.io/asset_management/assets>`_ or by Kayhan's operations team in coordination with operators. Please contact help@kayhan.space for assistance in setting up your constellation for Avoidance Maneuver generation.

Avoidance Maneuver Scenarios
############################

In Pathfinder, each case of the Avoidance Maneuver system is a **Scenario**. A Scenario is run either manually or automatically based on a CDM and generates one or more 
**Tradespaces** of potential maneuver plans to mitigate the risk of the conjunction.

Each Tradespace consists of a set of **Maneuver Plan** options, each of which could be executed by the spacecraft.
A maneuver plan contains information about the suggested maneuvers themselves, as well as metrics on how the plan would change the predicted conjunction if the plan were executed (e.g. the mitigated collision probability).

.. tip::
    A **Scenario** contains one or more **Tradespaces**.

    A **Tradespace** contains one or more **Maneuver Plans**.

    A **Maneuver Plan** contains one or more **Maneuvers**.

A Scenario might contain multiple Tradespaces, each with its own configuration. For example, one tradespace might be configured to generate posigrade maneuvers, while another might be configured to generate retrograde maneuvers. 

.. note::
    For Differential Drag maneuvers, special grouping logic can be applied to indicate that a series of ``Maneuver Plans`` belong together to indicate the full duration of the drag. If the ``Maneuver`` within the plan has the exact same ``ignition_epoch`` as other maneuvers within other plans, they are members of the same group. Within the group, each maneuver will also have an ``ignition_duration_hr`` property, indicating how long the drag must occur in order to achieve the provided outcomes. This value can be used to determine the grouping order, for example, if two maneuver plans have the same ignition_epoch and one has an ignition_duration_hr of 2 and the other has an ignition_duration_hr of 6, the latter of these two shows what values, such as ``collision_probability`` or ``miss_distance_km`` would be after the 6 hour mark.

.. note::
    Maneuver Plans may consist of one or more maneuvers. Currently, Pathfinder supports generating plans with either Single or Paired maneuvers.
    Paired maneuvers can be useful for spacecraft with very weak thrust capabilities, allowing the spacecraft to execute two distinct maneuvers
    in succession to achieve a larger overall orbit modification.

Finding the Scenario for a CDM
##############################

To access the maneuver plans generated for a given CDM, you can first locate the Scenarios which have been generated for that CDM. In most cases, there will only be one scenario per CDM.

.. code-block:: python

    cdm_id = 12345678 # the CDM_ID of the CDM from space-track.org
    cdm_key = 'CDM_12345678' # the CDM_Key of the CDM generated by Kayhan Space
    scenarios = client.avoidance.list_scenarios_for_cdm(cdm_id) # or cdm_key

    try:
        scenario = scenarios[0]
    except IndexError:
        raise ValueError("CDM has no Avoidance Maneuver Scenarios.") from None

.. note::
    The ``cdm_id`` field refers to the ``CDM_ID`` field given to CDMs on Space-Track. This is also the same unique ID that CDMs show on the Pathfinder web application. ``cdm_key`` can also be used in place of a cdm_id, and is generally the preferred identifier since CDMs that are not sourced from space-track.org may not have a ``cdm_id``.

Creating a new Scenario with a Tradespace
#########################################

If a Trigger has been previously created and applied to an asset, and the appropriate conditions have been met, an avoidance maneuver scenario will automatically be generated for a CDM. In addition to these autogenerated maneuvers, a manually generated avoidance maneuver scenario can be created for a CDM that does not yet have a scenario. 

The preferred method for creating a manually generated avoidance maneuver is to identify a CDM and submit a request for a new scenario with a valid tradespace configuration, which includes a profile_id for the avoidance maneuver profile and appropriate RSO attributes.

.. code-block:: python
    from kayhan.sdk.avoidance import models

    cdm_id = 12345678 # the CDM_ID of the CDM from space-track.org
    cdm_key = 'CDM_12345678' # the CDM_Key of the CDM generated by Kayhan Space
    profile_id = 'avoidance_profile_1' # Avoidance Maneuver Profile ID

    config = models.TradespaceRun(
        profile_id = profile_id,
        rso_physical_attributes=models.RSOPhysicalAttributes(
            mass_kg=1.0,
            thrust_n=1.0,
            isp_s=1.0,
            override_hbr_m=5.0
        )
    )

    scenario = client.avoidance.create_scenario_with_tradespace_configuration(cdm_key_or_id, config)

Adding a new Tradespace to an existing Scenario
###############################################

If a Scenario already exists, a new Tradespace can be added to the list of tradespaces within that scenario by providing the scenario_id and a valid configuration.

.. code-block:: python
    from kayhan.sdk.avoidance import models

    scenario_id = 'scenario_1' # Avoidance Maneuver Scenario ID
    profile_id = 'avoidance_profile_1' # Avoidance Maneuver Profile ID
        config = models.TradespaceRun(
        profile_id = profile_id,
        rso_physical_attributes=models.RSOPhysicalAttributes(
            mass_kg=1.0,
            thrust_n=1.0,
            isp_s=1.0,
            override_hbr_m=5.0
        )
    )

    scenario = client.avoidance.append_scenario_tradespace(scenario_id, config)

Choosing a Maneuver Plan
########################

Once we have the generated scenario, we can then retrieve its Tradespaces and inspect the Maneuver Plans contained within. 
In this simple example, we print the predicted collision probability and miss distance of all suggested maneuver plans to the terminal output for manual review.

.. code-block:: python

    tradespaces = client.avoidance.list_tradespaces(scenario.id)
    for tradespace in tradespaces:
        plans = client.avoidance.list_maneuver_plans(tradespace.id, scenario.id)
        for plan_index, plan in enumerate(plans):
            print(f"{tradespace.name} Maneuver Plan {plan_index} | PC: {plan.collision_probability}     Miss Distance: {plan.miss_distance_km}")

It is up to you to choose a maneuver plan appropriate to your spacecraft, mission, and operational constraints. Depending on the Tradespace Algorithm used, there may be many maneuver suggestions to choose from. 

Using the sorting functionality in the SDK can make choosing a suitable plan much easier in many cases. For example, you might choose to sort the maneuver plans from each tradespace by predicted collision probability so that only plans with the lowest resulting PC are considered:

.. code-block:: python   

    from kayhan.sdk.utils import SortDirection

    tradespaces = client.avoidance.list_tradespaces(scenario.id)
    for tradespace in tradespaces:
        plans = client.avoidance.list_maneuver_plans(
            tradespace.id,
            scenario.id,
            sort_field="collision_probability",
            sort_direction=SortDirection.ASC
        )
        best_plan = plans[0]
        print(f"{tradespace.name} Best Maneuver Plan | PC: {best_plan.collision_probability}     Miss Distance: {best_plan.miss_distance_km}")

Screening a Maneuver Plan
#########################

Once you have chosen an appropriate maneuver plan which sufficiently mitigates the risk of collision and works with your mission's constraints, you can use Pathfinder to screen the maneuver plan against the rest of the catalog for tertiary conjunctions. This gives you assurance that by executing a particular avoidance maneuver plan, your spacecraft is not predicted to inadvertently cause a new conjunction with another object nearby.

.. code-block:: python

    screening = client.avoidance.create_maneuver_plan_screening(
        plan.id,
        tradespace.id,
        scenario.id
    )
    screening = client.screening.await_screening_completion(screening)
    conjunctions = client.screening.list_conjunctions(screening.id)

The object returned by ``create_maneuver_plan_screening`` is a regular Kayhan SDK ``Screening`` object, and can be used just like any other ``Screening`` with the functions in :py:mod:`ScreeningClient <kayhan.sdk.screening.screening_client.ScreeningClient>`. For instance, in the above example, :py:mod:`await_screening_completion <kayhan.sdk.screening.screening_client.ScreeningClient.await_screening_completion>` is used to wait on the screening to be complete so that its results can be viewed.

Downloading a Maneuver Plan
###########################

Once a Maneuver Plan has been selected, it can be downloaded into a common format to be used throughout the rest of your operations process. This might include delivering the maneuver plan to Space-Track for final screening, ingestion into an external toolkit for further review, or simply uploading the maneuver plan to the spacecraft to be executed. At present, the Kayhan SDK offers both the CCSDS Orbit Parameter Message (OPM) format as well as a plain JSON format.

.. code-block:: python

    opm_file_content = client.avoidance.download_maneuver_plan(
        plan.id,
        tradespace.id,
        scenario.id,
        "opm",
        f"OPM_AVOIDANCE_MANEUVER_PLAN_{tradespace.name}.txt"
    )
    print(opm_file_content.read())

.. warning::
    Kayhan Space strongly recommends that any maneuver plan be screened before using it for any operational purpose.

Operationalizing a Maneuver Plan
###########################

To streamline and simplify flight operations, an Ephemeris file can also be generated from an existing Maneuver Plan. By default, execution of this function :py:mod:`create_ephemeris_from_maneuver_plan <kayhan.sdk.avoidance.avoidance_client.AvoidanceClient.create_ephemeris_from_maneuver_plan>` will designate it as ``OPERATIONAL``, see `Pathfinder Documentation - Operational CDM Reports <https://app.kayhan.io/docs/pathfinder/conjunction-events/mitigation/#building-on-operational-cdm-reports>`_ for more detail on what this designation means in terms of Operational CDM Reports. Alternatively, a different designation can be applied to the ephemeris, see `Pathfinder Documentation - Operational Ephemeris <https://app.kayhan.io/docs/pathfinder/operational-data/#ephemeris-metadata>`_ for more details. 

.. code-block:: python

    ephemeris = client.avoidance.create_ephemeris_from_maneuver_plan(
        scenario.id,
        tradespace.id,
        plan.id
    )
    print(f"{ephemeris.id} designation: {ephemeris.designation}")