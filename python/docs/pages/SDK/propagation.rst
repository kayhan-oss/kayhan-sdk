.. _SDK Propagation:

Propagation
===========

Overview
########

If you would prefer to delegate propagation of ephemeris to Kayhan, you can use the propagation features of the Kayhan SDK. Kayhan will generate a predictive ephemeris based on the epoch state produced by your Orbit Determination software using a high fidelity numerical propagator. 

.. note::
    Additional configuration options for propagation will be added in future releases of the Kayhan SDK.

The Propagation Resource
########################

Similar to ``Screening``, the SDK introduces the ``Propagation`` resource, which can be created using :py:mod:`create_propagation <kayhan.sdk.screening.propagation_client.PropagationClient.create_propagation>`. For example:

.. code-block:: python

    ...
    from kayhan.sdk.propagation.models import PropagationConfiguration
    config = PropagationConfiguration(target_duration=86400, timestep=60)
    propagation = client.propagation.create_propagation(config)

.. note::
    The ``config`` parameter of ``create_propagation`` is optional. If unspecified, a default configuration will be generated, with ``target_duration`` set to 5 days and ``timestep`` set to 60 seconds.

Much like screenings, propagations are also asynchronous jobs which are run on Kayhan Pathfinder's servers. They can be submitted using :py:mod:`submit_propagation <kayhan.sdk.screening.propagation_client.PropagationClient.submit_propagation>` and awaited using :py:mod:`await_propagation_completion <kayhan.sdk.screening.propagation_client.PropagationClient.await_propagation_completion>`. You can also use the ``submit`` parameter of ``create_propagation`` to automatically submit the propagation upon creation, if it has been fully configured using the other parameters of ``create_propagation``.

CCSDS Orbit Parameter Message (OPM)
###################################

.. include:: ../../common/opm/opm.rst

To create a propagation based on an OPM, you can use the ``opm_file`` parameter of ``create_propagation``. For example:

.. code-block:: python

    propagation = client.propagation.create_propagation(opm_file="USER_OPM_FILE.txt")

.. include:: ../../common/opm/parameters.rst

Screening using Propagations
############################

The Screening module of the SDK supports using Propagations to screen natively. To use a propagation in a screening, you can pass the ``Propagation`` object in either the ``primaries`` or ``secondaries`` list to ``create_screening``. For example:

.. code-block:: python

    ...
    propagation = client.propagation.create_propagation(opm_file="USER_OPM_FILE.txt")
    screening = client.screening.create_screening(
        primaries=[propagation],
        add_best_secondary_catalog=True,
        submit=True
    )

.. note::
    If a propagation is added as a primary or secondary to a screening, it is automatically submitted and processed when the screening is submitted. You do not need to manually submit or await a propagation that is attached to a screening as a primary or secondary; you may simply create the propagation, configure it, attach it to the screening, and submit the screening.
    
    Kayhan Pathfinder will automatically process all propagations on which the screening depends and use the resulting ephemerides during screening processing.


Downloading the Ephemeris Results of a Propagation
##################################################

You can also download the result content of a propagation as an ephemeris file using the :ref:`ephemeris file download <Download Ephemeris>` feature.

For example, to create and run a propagation using an OPM file and download the results as an OEM ephemeris file:

.. code-block:: python

    # Create the propagation, configure using an OPM file, and submit for processing
    propagation = client.propagation.create_propagation(opm_file="USER_OPM_FILE.txt", submit=True)

    # Await completion of the propagation
    propagation = client.propagation.await_propagation_completion(propagation)

    # Download the results of the propagation as an OEM-formatted ephemeris file
    ephemeris_content = client.screening.download_ephemeris(propagation.ephemeris_id, file_format="OEM")

    # Read the contents of the ephemeris file and print the content of the file to stdout
    print(ephemeris_content.read())
