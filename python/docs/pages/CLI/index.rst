CLI
===

Overview
########

The Kayhan CLI is a command-line application for quickly performing common tasks using the Kayhan SDK. 

Whereas the Kayhan SDK Python library is best used for writing scripts used in automated systems, the Kayhan CLI is best used as a tool for power users to interact with Kayhan services quickly and efficiently without needing to write Python code.

.. toctree::
    :maxdepth: 4

    Getting Started <gettingstarted>
    Propagation <propagation>
