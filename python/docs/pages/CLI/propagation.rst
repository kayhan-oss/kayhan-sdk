Propagation
===========

Creating a Propagation
######################

Propagations can be created using the CLI using ``kayhan create propagation``:

.. code-block:: bash

    kayhan create propagation --duration-sec 86400 --timestep-sec 60

Much like screenings, propagations are also asynchronous jobs which are run on Kayhan Pathfinder's servers. They can be submitted using ``kayhan submit propagation [id]`` and awaited using ``kayhan await propagation [id]``.

You can also use the ``--submit`` flag of ``create propagation`` to automatically submit the propagation upon creation if it has been fully configured using the other parameters of ``create_propagation``, and the ``--wait`` flag to await completion if ``submit`` was also supplied.

CCSDS Orbit Parameter Message (OPM)
###################################

.. include:: ../../common/opm/opm.rst

To create a propagation using an OPM, you can use the ``--opm`` flag of ``kayhan create propagation``:

.. code-block:: bash

    kayhan create propagation --opm USER_OPM_FILE.txt

.. include:: ../../common/opm/parameters.rst

Screening using Propagations
############################

The Screening module of the SDK supports using Propagations to screen natively. To use a propagation in a screening based on an OPM, you can use the ``--opm`` flag of ``kayhan create screening``:

.. code-block:: bash

    kayhan create screening --opm USER_OPM_FILE.txt

This will automatically create a screening and attached propagation (as a primary) configured using the OPM. You may also choose to use the ``--submit`` and ``--wait`` flags to automatically submit and await completion of both the propagation and screening:

.. code-block:: bash

    kayhan create screening --opm USER_OPM_FILE.txt --submit --wait

    # Or, equivalently, using short flags
    kayhan create screening --opm USER_OPM_FILE.txt -sw

Downloading the Ephemeris Results of a Propagation
##################################################

Coming soon!