Getting Started - CLI
=====================

Invoking the CLI
################

If you have installed the SDK using ``pip``, the Kayhan cli should already be installed in your path. You can verify this using:

.. code-block:: bash

    $ kayhan version
    kayhan version 1.0.0

One-Line Screening
##################

.. include:: ../../common/ephemeris/ephemeris_formats.rst

With the Kayhan CLI, you can upload an ephemeris file and submit a screening with a single command:

.. code-block:: bash

    kayhan create screening --pid 25544 --pef MEME_25544_ISS_Ephemeris_2022181_OEM.txt --pfmt oem --wait


Let's review the parameters being used:

``--pid``, short for "primary ID", is the NORAD Catalog ID of the object which the uploaded ephemeris file describes. 

``--pef``, short for "primary ephemeris file", is the path to the ephemeris file being uploaded.

``--pfmt``, short for "primary ephemeris format", is the file format of the ephemeris file being uploaded.

.. note::
    .. include:: ../../common/ephemeris/ephemeris_upload_requirements.rst

``--wait`` indicates that the operation should block until the screening has been processed on Pathfinder servers and wait to exit until the screening is completed. 

You may also choose to instead use the ``--submit`` flag, which will submit the screening to be processed and return immediately. In this case, the screening can then be awaited subsequently using ``kayhan await screening``.

Take note of the ``id`` field of the created screening - this ID is used to retrieve the results of the screening once it is complete.

Retrieving Results
##################

Once a screening is complete, you can retrieve the resulting conjunctions using ``kayhan list conjunctions``:

.. code:: bash
    
    kayhan list conjunctions [screening_id]

Listing Entities
################

You can use the ``kayhan`` list command to list any entity in the Pathfinder screening API, including your screenings:

.. code:: bash

    $ kayhan list screenings

    ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━┳━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━┓
    ┃ Id                                   ┃ Title     ┃ Status  ┃ Created At                ┃ Conjunctions Count ┃ Primary Ids Preview ┃
    ┡━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━╇━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━╇━━━━━━━━━━━━━━━━━━━━━┩
    │ dd44f906-1e9c-40cd-b97f-e6d791b39b96 │ Screening │ SUCCESS │ 2022-06-22 14:50:24+00:00 │ 12                 │ [25544]             │
    └──────────────────────────────────────┴───────────┴─────────┴───────────────────────────┴────────────────────┴─────────────────────┘

.. note::

    In the Screening list table with rich formatting, the values in the ``Id`` column are automatic hyperlinks to the Pathfinder web application on supported terminal clients (``ctrl-click`` / ``cmd-click`` to open in your web browser). Since Pathfinder screenings all run on Pathfinder servers, you can view the same screening results and information on the Pathfinder website easily after configuring and running them using the SDK or CLI.

Output Formatting
#################

By default, the Kayhan CLI uses rich output formatting including tables and progress indicators for human readability. However, you can also configure the CLI to output plain JSON data for machine readability or redirecting to a file (e.g. for logging purposes).

To set the output format to json, use the ``--output-format`` (``-o``) flag of the ``kayhan`` base command:

.. code:: bash

    kayhan --output-format json ...

.. note::

    The ``--output-format`` flag must be specified after the base ``kayhan`` command but before any subcommands. For example, the following would be invalid:

    .. code:: bash

        kayhan list screenings --output-format json

To disable interactive progress indicators in the terminal output, you can use the ``--no-progress`` flag of the ``kayhan`` base command:

.. code:: bash

    kayhan --no-progress ...

These settings can also be persistently configured in the settings module (see :doc:`../configuration`) as ``settings.output_format`` and ``settings.show_progress`` respectively.

.. note::

    The default rich CLI output format only displays a curated subset of information for each resource type for maximum readability. To view all fields of a resource and inspect in detail, it's best to use ``--output-format json``.

Downloading an Ephemeris
########################

You can also download the content of an ephemeris file on the Kayhan Pathfinder servers. This includes:

* Owner/Operator ephemerides that you have uploaded or have access to
* Ephemerides created during propagation using the :ref:`Pathfinder propagation features <SDK Propagation>`
* Ephemerides from any catalogs to which you have access

To download an ephemeris, all you need is its unique identifier (``id``) and the file format in which you want the ephemeris to be downloaded.
Then, you can use ``kayhan download ephemeris`` to download the file content.

.. note::
    Currently, only the ``OEM`` and ``NASA`` formats are supported for ephemeris file downloads (case insensitive).

For example, to upload an ephemeris file and download it in another format:

.. code-block:: bash

    # Upload an OEM ephemeris to Kayhan
    kayhan create ephemeris MEME_25544_OEM_EPH.txt oem 25544

    # The above command will show the metadata of the uploaded file,
    # including an "id" field containing the ephemeris's new unique id.

    # To download it in another format (in this case, the NASA format):

    kayhan download ephemeris ${EPHEMERIS_ID_FROM_UPLOAD} -o MEME_25544_NASA_EPH.txt -f nasa 

    # Where EPHEMERIS_ID_FROM_UPLOAD is the unique identifier of the
    # ephemeris uploaded using the previous command.

    # Here is an example of how the command would look after substituting the id:

    kayhan download ephemeris 0608a3fa-5005-421e-lmeu-4a168932562f \
        -o MEME_25544_NASA_EPH.txt -f nasa 

This downloads the ephemeris using the desired format (specified using the ``-f`` flag) and saves the downloaded file to the requested output path (specified using the ``-o`` flag).

.. note::
    If the ``-o`` (``--output-file``) flag is not specified, the content of the file is printed to stdout (terminal output).
