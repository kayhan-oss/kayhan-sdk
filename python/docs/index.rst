Kayhan SDK |version| Documentation
===================================

This is the documentation for the Kayhan Space Software Development Kit (SDK) and Command-Line Interface (CLI).

The Kayhan SDK is a Python client library for interacting with Kayhan Space web services. It can be used to easily integrate Kayhan Space services such as ephemeris screening into automated scripts and processes using Python.

The Kayhan SDK also comes with a Command-Line Interface (CLI) for quickly interacting with SDK functionality from the command line.

Installation
^^^^^^^^^^^^
The Kayhan SDK is currently distributed as a Python package which can be installed using ``pip`` or any ``pypi`` package manager.

.. code-block:: bash

    $ pip install kayhan --extra-index-url=https://gitlab.com/api/v4/projects/34366588/packages/pypi/simple

Quickstart
^^^^^^^^^^
 
SDK
###
.. code-block:: python

    from kayhan.sdk.client import KayhanClient
    with KayhanClient() as client:
        # Upload an O/O Ephemeris to Kayhan
        ephemeris = client.screening.create_ephemeris(
            "MEME_25544_ISS_Ephemeris_2022181_OEM.txt",
            file_format="oem",
            norad_cat_id=25544
        )

        # Create a Screening for the uploaded Ephemeris and submit it
        screening = client.screening.create_screening(
            primaries=[ephemeris],
            add_best_secondary_catalog=True,
            submit=True
        )

        # Await asynchronous processing of the Screening on Kayhan's servers
        screening = client.screening.await_screening_completion(screening)

        # Retrieve the conjunctions detected by the Screening
        conjunctions = client.screening.list_conjunctions(screening.id)

CLI
###
.. code-block:: bash

    # Create and submit a screening for an O/O ephemeris file, save the metadata to a JSON file, and await completion
    $ kayhan -o json create screening --pid 25544 --pef MEME_25544_ISS_Ephemeris_2022181_OEM.txt --pfmt oem --wait > my_screening.json
    # Retrieve the conjunctions detected by the Screening after completion
    $ jq ".id" -r my_screening.json | xargs kayhan list conjunctions


Configuration
#############
The Kayhan SDK has a number of user-configurable global settings. See :doc:`pages/configuration` for further details.

----

* :ref:`genindex`
* :ref:`modindex`

----

.. toctree::
    :maxdepth: 10
    :hidden:

    API Reference <pages/autogen/kayhan>
    pages/CLI/index
    pages/SDK/index
    pages/configuration
    pages/changelog
