Creating a propagation from an OPM file has the following implications:

* The initial state of the propagation is set to the state vector supplied in the OPM.
* If a covariance matrix is specified in the OPM, the initial covariance of the propagation is set to the covariance matrix supplied in the OPM.
* If any maneuvers are specified in the OPM, the maneuvers are added to the propagation.
* If the spacecraft parameters section is present in the OPM, this information is added to the propagation.
* If the propagation does not already contain a value for ``start_time``, the epoch of the state vector is set as the propagation's ``start_time``.


.. warning::
    As per CCSDS 502.0-B-2 3.2.4.9, the spacecraft parameters section **must be supplied** if the OPM contains maneuvers. Additionally, as per sections 3.2.4.5 & 3.2.4.6, drag and solar radiation pressure forces are only taken into account if the values for drag and SRP coefficient supplied in the OPM are nonzero.

    These settings have a large impact on the trajectory of a spacecraft, particularly in low earth orbit. To ensure the fidelity of the resulting predictive ephemeris, the Kayhan SDK **requires** the Spacecraft Parameters section of the OPM, even if the file contains no maneuvers.
    
    Please take care to supply the best possible estimates for the values in the Spacecraft Parameters section when supplying an OPM for propagation.