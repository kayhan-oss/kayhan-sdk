from pygments.style import Style
from pygments.token import Comment, Keyword, Name, Number, String


class KayhanPygmentsStyleLight(Style):
    background_color = "#F4F4F4"

    default_style = ""
    styles = {
        Comment: "italic #595959",
        Keyword: "bold #0066FF",
        Name: "#161616",
        Name.Function: "bold #393939",
        String: "#00BBBB",
        Number: "bold #0066FF",
    }


class KayhanPygmentsStyleDark(Style):
    background_color = "#262626"

    default_style = ""
    styles = {
        Comment: "italic #797979",
        Keyword: "bold #78aaff",
        Name: "#F4F4F4",
        Name.Function: "bold #6F6F6F",
        Name.Class: "bold #D1BCFF",
        String: "#9ef0f0",
        Number: "bold #78aaff",
    }
