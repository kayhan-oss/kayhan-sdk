# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from datetime import datetime
from importlib import metadata

sys.path.insert(0, os.path.abspath("."))
sys.path.insert(0, os.path.abspath(".."))

# -- Project information -----------------------------------------------------

project = "Kayhan SDK"
year = datetime.today().year
copyright = f"{year}, Kayhan Space"
author = "Derek Strobel"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.coverage",
    "sphinx_autodoc_typehints",
    "sphinx.ext.mathjax",
]
# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

version = metadata.version("kayhan")

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "furo"
html_title = "SDK Documentation"
html_theme_options = {
    "dark_logo": "text_white.svg",
    "light_css_variables": {
        "font-stack": "IBM Plex Sans",
        "font-stack--monospace": "IBM Plex Mono",
        "color-foreground-primary": "#161616",
        "color-brand-primary": "#7D48ED",
        "color-brand-content": "#7D48ED",
        "color-background-secondary": "#F4F4F4",
        "color-inline-code-background": "#F4F4F4",
        "color-api-background": "#F4F4F4",
        "color-code-background": "#F4F4F4",
        "color-api-name": "#7D48ED",
        "color-api-pre-name": "#7D48ED",
        # "color-link": ""
    },
    "light_logo": "text_black.svg",
    "dark_css_variables": {
        "font-stack": "IBM Plex Sans",
        "font-stack--monospace": "IBM Plex Mono",
        "color-foreground-primary": "#F4F4F4",
        "color-brand-primary": "#B491FF",
        "color-brand-content": "#B491FF",
        "color-background-primary": "#161616",
        "color-background-secondary": "#262626",
        "color-inline-code-background": "#262626",
        "color-api-background": "#262626",
        "color-code-background": "#262626",
        "color-api-name": "#B491FF",
        "color-api-pre-name": "#B491FF",
        "color-link": "#B491FF",
        "color-link--hover": "#D1BCFF",
    },
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

# Include constructors in autoclass
autoclass_content = "both"

# use unqualified type hint names to prevent huge type strints in docs.
python_use_unqualified_type_names = True


sys.path.append(os.path.abspath("./_pygments"))
pygments_style = "style.KayhanPygmentsStyleLight"
pygments_dark_style = "style.KayhanPygmentsStyleDark"


def setup(app):
    app.add_css_file("styles.css")
