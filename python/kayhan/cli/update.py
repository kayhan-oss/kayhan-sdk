import json
from typing import List, Optional

import typer
from typer import Abort

from kayhan.cli.utils import (
    cli_output,
    cli_output_model,
    validate_model_from_input,
)

from kayhan.sdk.administration import models as administration_models
from kayhan.sdk.asset_management import models as asset_management_models
from kayhan.sdk.avoidance import models as avm_models
from kayhan.sdk.client import KayhanClient

app = typer.Typer()

# Avoidance Maneuver


@app.command(
    help=f"maneuver_algo_type cannot be modified in an existing profile, attributes of maneuver_algo with the --maneuver-algorithm- prefix can be modified. An example of the DIFF_DRAG maneuver_algo type includes the following flags: --maneuver-algorithm-drag-profile --maneuver-algorithm-max-drag-time-h --maneuver-algorithm-drag-area-min-m-2 --maneuver-algorithm-drag-area-max-m-2",
    context_settings={"allow_extra_args": True, "ignore_unknown_options": True},
)
def maneuver_profile(
    ctx: typer.Context,
    profile_id: str,
    name: str = typer.Option(None, "--name"),
    burn_type: Optional[avm_models.BurnType] = None,
    tradespace_algorithm: avm_models.TradespaceAlgorithm = typer.Option(
        None, "--tradespace-algorithm"
    ),
    fidelity_type: Optional[avm_models.TradespaceFidelityType] = None,
    fidelity_unit: str = typer.Option(
        None,
        "--fidelity-unit",
        help="min | hr | day",
    ),
    fidelity_value: float = typer.Option(None, "--fidelity-value"),
):
    algorithm_models = [
        (
            avm_models.ManeuverAlgorithm.FIXED_DV,
            avm_models.ManeuverAlgorithmConfigurationFixedDV,
        ),
        (
            avm_models.ManeuverAlgorithm.OPTIMAL,
            avm_models.ManeuverAlgorithmConfigurationOptimal,
        ),
        (
            avm_models.ManeuverAlgorithm.DIFF_DRAG,
            avm_models.ManeuverAlgorithmConfigurationDiffDrag,
        ),
    ]

    maneuver_algo_dict = None
    maneuver_algo_type = None

    for algo_type, model_class in algorithm_models:
        try:
            maneuver_algo_dict = validate_model_from_input(
                ctx, model_class, "--maneuver-algorithm-"
            )
            maneuver_algo_type = algo_type
            break
        except ValueError:
            continue

    if not maneuver_algo_dict:
        typer.echo(
            "Error: No valid configuration for any supported Maneuver Algorithms was provided."
        )
        raise typer.Exit(code=1)

    with KayhanClient() as client:
        existing_profile = client.avoidance.get_maneuver_profile(profile_id)

        configuration = avm_models.ProfileConfiguration(
            name=name if name else existing_profile.name,
            maneuver_algo_type=(
                maneuver_algo_type.upper()
                if maneuver_algo_type
                else existing_profile.maneuver_algo_type
            ),
            maneuver_algo=(
                maneuver_algo_dict
                if maneuver_algo_dict is not None
                else existing_profile.maneuver_algo
            ),
            burn_type=(
                burn_type.upper()
                if burn_type
                else (
                    existing_profile.burn_type.upper()
                    if existing_profile.burn_type
                    else None
                )
            ),
            tradespace_algorithm=(
                tradespace_algorithm.upper()
                if tradespace_algorithm
                else existing_profile.tradespace_algorithm.upper()
            ),
            fidelity_type=(
                fidelity_type.upper()
                if fidelity_type
                else (
                    existing_profile.fidelity_type.upper()
                    if existing_profile.fidelity_type
                    else None
                )
            ),
            fidelity_unit=(
                fidelity_unit if fidelity_unit else existing_profile.fidelity_unit
            ),
            fidelity_value=(
                fidelity_value if fidelity_value else existing_profile.fidelity_value
            ),
        )

        updated_profile = client.avoidance.update_maneuver_profile(
            profile_id=profile_id, config=configuration
        )

    cli_output_model(updated_profile)


@app.command()
def operationalize_ephemeris(
    ephemeris_id: str = typer.Option(None, "--ephemeris-id", "--id")
):
    with KayhanClient() as client:
        resource = client.screening.operationalize_ephemeris(ephemeris_id)
        cli_output_model(resource)


@app.command(
    help="A constellation can only be linked to one trigger at a time, creating a new linked constellation will replace previous unlinked configurations. If link=False, constellation can be applied to any number of triggers."
)
def trigger(
    trigger_id: str = typer.Option(None, "--trigger-id", "--id"),
    slug: Optional[str] = typer.Option(None, "--slug", help="constellation slug"),
    title: Optional[str] = typer.Option(None, "--title"),
    include: Optional[str] = typer.Option(
        None, "--include", help="comma separated, e.g. '1234,5678'"
    ),
    exclude: Optional[str] = typer.Option(
        None, "--exclude", help="comma separated, e.g. '1234,5678'"
    ),
    link: Optional[bool] = typer.Option(
        False,
        "--link",
        help="will default to True if both include and exclude are None",
    ),
    notes: Optional[str] = typer.Option(None, "--notes"),
    is_paused: Optional[bool] = typer.Option(False, "--paused"),
    max_miss_distance_km: Optional[float] = typer.Option(
        None,
        "--max-miss-distance",
        "--max-md",
        help="greater than 1 meter and less than 5 kilometers, inclusive",
    ),
    min_collision_probability: Optional[float] = typer.Option(
        None,
        "--min-collision-probability",
        "--min-pc",
        help="greater than 1e-5 and less than 1, inclusive",
    ),
    md_pc_logical_operator: Optional[avm_models.LogicalOperator] = typer.Option(
        None, "--op"
    ),
    max_window_under_tca_hr: Optional[float] = typer.Option(
        1,
        "--max-window-under-tca",
        "--tca-w",
        help="greater than 1 hour and less than 5 days (120hr), inclusive",
    ),
):
    include_list = include.split(",") if include else None
    exclude_list = exclude.split(",") if exclude else None
    linked = True if include_list is None and exclude_list is None else link

    with KayhanClient() as client:
        existing_trigger = client.avoidance.get_trigger(trigger_id)

        configuration = avm_models.TriggerConfiguration(
            title=title if title else existing_trigger.title,
            notes=notes if notes else existing_trigger.notes,
            is_paused=(
                is_paused if is_paused is not None else existing_trigger.is_paused
            ),
            types=[
                avm_models.TriggerDetail(
                    type=avm_models.TriggerType.AVOIDANCE_MANEUVER,
                    max_miss_distance=(
                        max_miss_distance_km
                        if max_miss_distance_km
                        else existing_trigger.types[0].max_miss_distance
                    ),
                    max_miss_distance_unit=avm_models.LengthUnit.KILOMETER,
                    min_collision_probability=(
                        min_collision_probability
                        if min_collision_probability
                        else existing_trigger.types[0].min_collision_probability
                    ),
                    md_pc_logical_operator=(
                        md_pc_logical_operator
                        if md_pc_logical_operator
                        else existing_trigger.types[0].md_pc_logical_operator
                    ),
                    max_window_under_tca=(
                        max_window_under_tca_hr
                        if max_window_under_tca_hr
                        else existing_trigger.types[0].max_window_under_tca
                    ),
                    max_window_under_tca_unit=avm_models.TimeUnit.HOURS,
                )
            ],
            assets=[
                avm_models.TriggerAsset(
                    constellation_slug=(
                        slug if slug else existing_trigger.assets[0].constellation_slug
                    ),
                    include=(
                        include_list if include else existing_trigger.assets[0].include
                    ),
                    exclude=(
                        exclude_list if exclude else existing_trigger.assets[0].exclude
                    ),
                    link_constellation=(
                        linked
                        if link is not None
                        else existing_trigger.assets[0].link_constellation
                    ),
                )
            ],
        )
        trigger = client.avoidance.update_trigger(
            trigger_id=trigger_id,
            config=configuration,
        )

    cli_output_model(trigger)


# Administration


def organization(
    contact_name: Optional[str] = typer.Option(None, "--contact-name", "--name"),
    contact_phone: Optional[str] = typer.Option(None, "--contact-phone", "--phone"),
    contact_email: Optional[str] = typer.Option(None, "--contact-email", "--email"),
    space_track_contact_name: Optional[str] = typer.Option(
        None, "--space-track-contact-name", "--st-name"
    ),
    space_track_contact_phone: Optional[str] = typer.Option(
        None, "--space-track-contact-phone", "--st-phone"
    ),
    space_track_contact_email: Optional[str] = typer.Option(
        None, "--space-track-contact-email", "--st-email"
    ),
    pc_high_risk: Optional[float] = typer.Option(None, "--pc-high-risk", "--pc-h"),
    pc_low_risk: Optional[float] = typer.Option(None, "--pc-low-risk", "--pc-l"),
    md_high_risk_km: Optional[float] = typer.Option(
        None, "--md-high-risk", "--md-h", help="km"
    ),
    md_low_risk_km: Optional[float] = typer.Option(
        None, "--md-low-risk", "--md-l", help="km"
    ),
    coordination_participant: Optional[bool] = typer.Option(
        None, "--coordination-participant", "--cp"
    ),
    directory_listing: Optional[bool] = typer.Option(
        None, "--directory-listing", "--dl"
    ),
    share_operational_ephemeris: Optional[bool] = typer.Option(
        None, "--share-operational-ephemeris", "--soe"
    ),
    website: Optional[str] = typer.Option(None, "--website"),
    country: Optional[str] = typer.Option(None, "--country"),
):
    with KayhanClient() as client:
        existing_organization = client.administration.get_organization()

        configuration = administration_models.OrganizationConfiguration(
            contact_name=(
                contact_name if contact_name else existing_organization.contact_name
            ),
            contact_phone=(
                contact_phone if contact_phone else existing_organization.contact_phone
            ),
            contact_email=(
                contact_email if contact_email else existing_organization.contact_email
            ),
            space_track_contact_name=(
                space_track_contact_name
                if space_track_contact_name
                else existing_organization.space_track_contact_name
            ),
            space_track_contact_phone=(
                space_track_contact_phone
                if space_track_contact_phone
                else existing_organization.space_track_contact_phone
            ),
            space_track_contact_email=(
                space_track_contact_email
                if space_track_contact_email
                else existing_organization.space_track_contact_email
            ),
            pc_high_risk=(
                pc_high_risk if pc_high_risk else existing_organization.pc_high_risk
            ),
            pc_low_risk=(
                pc_low_risk if pc_low_risk else existing_organization.pc_low_risk
            ),
            md_high_risk_km=(
                md_high_risk_km
                if md_high_risk_km
                else existing_organization.md_high_risk_km
            ),
            md_low_risk_km=(
                md_low_risk_km
                if md_low_risk_km
                else existing_organization.md_low_risk_km
            ),
            coordination_participant=(
                coordination_participant
                if coordination_participant
                else existing_organization.coordination_participant
            ),
            directory_listing=(
                directory_listing
                if directory_listing
                else existing_organization.directory_listing
            ),
            share_operational_ephemeris=(
                share_operational_ephemeris
                if share_operational_ephemeris
                else existing_organization.share_operational_ephemeris
            ),
            website=website if website else existing_organization.website,
            country=country if country else existing_organization.country,
        )
        organization = client.administration.update_organization(
            config=configuration,
        )

    cli_output_model(organization)


@app.command()
def user(
    user_id: str = typer.Option(None, "--user-id", "--id"),
    role: Optional[administration_models.Role] = typer.Option(None, "--role", "--r"),
    username: Optional[str] = typer.Option(None, "--username", "--n"),
    email: Optional[str] = typer.Option(None, "--email", "--e"),
):
    with KayhanClient() as client:
        existing_user = client.administration.get_user(user_id)

        configuration = administration_models.UserProfileConfiguration(
            role=role if role else existing_user.role,
            username=username if username else existing_user.username,
            email=email if email else existing_user.email,
        )
        user = client.administration.update_user(
            user_id=user_id,
            config=configuration,
        )

    cli_output_model(user)


@app.command(help="These actions can be applied to a single user or a list of users")
def batch_action_users(
    user_ids: str = typer.Option(
        None,
        "--user-ids",
        "--ids",
        help="comma separated, e.g. 1234,5678 | pass in a single ID without a comma to execute action for single user",
    ),
    action_type: administration_models.BatchUserActionType = typer.Option(
        None, "--action-type", "--a"
    ),
):
    stripped_user_ids = user_ids.split(",")
    with KayhanClient() as client:
        res = client.administration.request_batch_action_users(
            user_ids=stripped_user_ids, action_type=action_type
        )

    cli_output(res)


@app.command()
def api_key(
    resource_id: str = typer.Option(None, "--resource-id", "--id"),
    name: str = typer.Option(None, "--name", "--n"),
):
    with KayhanClient() as client:
        res = client.administration.update_api_key(api_key_id=resource_id, name=name)

    cli_output_model(res)


@app.command()
def api_key_rotate_secret(
    resource_id: str = typer.Option(None, "--resource-id", "--id"),
):
    try:
        action = typer.confirm(
            "Are you sure you want to rotate the API key secret?", abort=True
        )
    except Abort:
        cli_output("Aborting API key rotation action.")
        raise typer.Exit()

    if action:
        with KayhanClient() as client:
            res = client.administration.rotate_api_key_secret(api_key_id=resource_id)

        cli_output_model(res)


# Asset Management


@app.command()
def constellation(
    constellation_slug: str = typer.Option(None, "--constellation-slug", "--s"),
    name: str = typer.Option(None, "--name", "--n"),
):
    with KayhanClient() as client:
        existing_constellation = client.asset_management.get_constellation(
            constellation_slug
        )
        configuration = asset_management_models.ConstellationConfiguration(
            name=name if name else existing_constellation.name,
            slug=existing_constellation.slug,
        )
        constellation = client.asset_management.update_constellation(
            constellation_slug=constellation_slug,
            config=configuration,
        )

    cli_output_model(constellation)


@app.command(help="These updates can be applied to a single RSO or a list of RSOs")
def batch_update_rsos(
    rsos_norad_ids: str = typer.Option(
        None,
        "--rsos-norad-ids",
        "--ids",
        help="comma separated, e.g. 12345,67890 | pass in a single ID without a comma to execute action for single RSO",
    ),
    slug: str = typer.Option(
        None,
        "--slug",
        "--s",
        help="The constellation slug to move the RSOs to",
    ),
    mass_kg: Optional[float] = typer.Option(None, "--mass-kg", "--mass"),
    thrust_n: Optional[float] = typer.Option(None, "--thrust-n", "--thrust"),
    isp_s: Optional[float] = typer.Option(None, "--isp-s", "--isp"),
    override_maneuverable: Optional[bool] = typer.Option(
        None,
        "--override-maneuverable",
        "--o-man",
        help="Indicates if maneuverable value from Space-Track should be overridden",
    ),
    override_hbr_m: Optional[float] = typer.Option(
        None,
        "--override-hbr-m",
        "--o-hbr",
        help="Indicates if HBR value from Space-Track should be overridden with updated value",
    ),
    srp_coefficient_m: Optional[float] = typer.Option(
        None, "--srp-coefficient-m", "--srp-c"
    ),
    srp_area_m: Optional[float] = typer.Option(None, "--srp-area-m", "--srp-a"),
    drag_coefficient_m: Optional[float] = typer.Option(
        None, "--drag-coefficient-m", "--drag-c"
    ),
    drag_area_m: Optional[float] = typer.Option(None, "--drag-area-m", "--drag-a"),
    mobility_type: Optional[asset_management_models.MobilityType] = typer.Option(
        None, "--mobility-type", "--m"
    ),
    on_board_autonomous: Optional[bool] = typer.Option(
        None, "--on-board-autonomous", "--auto"
    ),
):
    individual_rsos = rsos_norad_ids.split(",")
    with KayhanClient() as client:
        rsos_config_list = []

        for rso_id in individual_rsos:
            existing_rso = client.asset_management.get_rso(rso_id)

            existing_constellation_slug = (
                existing_rso.constellation.slug if existing_rso.constellation else None
            )

            constellation_data = asset_management_models.RSOConstellationUpdate(
                slug=slug if slug else existing_constellation_slug
            )

            configuration = asset_management_models.RSOConfiguration(
                norad_cat_id=existing_rso.norad_cat_id,
                constellation=constellation_data,
                mass_kg=mass_kg if mass_kg is not None else existing_rso.mass_kg,
                thrust_n=thrust_n if thrust_n is not None else existing_rso.thrust_n,
                isp_s=isp_s if isp_s is not None else existing_rso.isp_s,
                override_maneuverable=(
                    override_maneuverable
                    if override_maneuverable is not None
                    else existing_rso.override_maneuverable
                ),
                override_hbr_m=(
                    override_hbr_m
                    if override_hbr_m is not None
                    else existing_rso.override_hbr_m
                ),
                srp_coefficient_m=(
                    srp_coefficient_m
                    if srp_coefficient_m is not None
                    else existing_rso.srp_coefficient_m
                ),
                srp_area_m=(
                    srp_area_m if srp_area_m is not None else existing_rso.srp_area_m
                ),
                drag_coefficient_m=(
                    drag_coefficient_m
                    if drag_coefficient_m is not None
                    else existing_rso.drag_coefficient_m
                ),
                drag_area_m=(
                    drag_area_m if drag_area_m is not None else existing_rso.drag_area_m
                ),
                mobility_type=(
                    mobility_type
                    if mobility_type is not None
                    else existing_rso.mobility_type
                ),
                on_board_autonomous=(
                    on_board_autonomous
                    if on_board_autonomous is not None
                    else existing_rso.on_board_autonomous
                ),
            )

            rsos_config_list.append(configuration)

        res = client.asset_management.batch_update_rsos(rsos_config=rsos_config_list)

    for rso in res:
        cli_output_model(rso)
