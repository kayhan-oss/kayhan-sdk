import typer

from kayhan.cli.utils import cli_output, cli_output_model
from kayhan.sdk.client import KayhanClient

app = typer.Typer()


# Screening


@app.command()
def ephemeris(resource_id: str):
    """Retrieve an Ephemeris by ID."""
    with KayhanClient() as client:
        resource = client.screening.get_ephemeris(resource_id)
        cli_output_model(resource)


@app.command()
def screening(resource_id: str):
    """Retrieve a Screening by ID."""
    with KayhanClient() as client:
        resource = client.screening.get_screening(resource_id)
        cli_output_model(resource)


@app.command()
def catalog(resource_id: str):
    """Retrieve a Catalog by ID."""
    with KayhanClient() as client:
        resource = client.screening.get_catalog(resource_id)
        cli_output_model(resource)


@app.command()
def conjunction_ccsds(screening_id: str, conjunction_id: str):
    """Retrieve a single CCSDS formatted Conjunction by Screening ID."""
    with KayhanClient() as client:
        resource = client.screening.get_conjunction_ccsds(screening_id, conjunction_id)
        cli_output(resource)


# Propagation


@app.command()
def propagation(resource_id: str):
    """Retrieve a Propagation by ID."""
    with KayhanClient() as client:
        resource = client.propagation.get_propagation(resource_id)
        cli_output_model(resource)


# Avoidance Maneuver


@app.command()
def scenario(resource_id: str):
    """Retrieve an Avoidance Maneuver Scenario by ID."""
    with KayhanClient() as client:
        resource = client.avoidance.get_scenario(resource_id)
        cli_output_model(resource)


@app.command()
def tradespace(tradespace_id: str, scenario_id: str):
    """Retrieve an Avoidance Maneuver Scenario Tradespace by ID."""
    with KayhanClient() as client:
        resource = client.avoidance.get_tradespace(tradespace_id, scenario_id)
        cli_output_model(resource)


@app.command()
def plan(plan_id: str, tradespace_id: str, scenario_id: str):
    """Retrieve an Avoidance Maneuver Plan by ID."""
    with KayhanClient() as client:
        resource = client.avoidance.get_maneuver_plan(
            tradespace_id, scenario_id, plan_id
        )
        cli_output_model(resource)


@app.command()
def avm_screening(plan_id: str, scenario_id: str, tradespace_id: str):
    """Retrieve an Avoidance Maneuver Plan Screening."""
    with KayhanClient() as client:
        resource = client.avoidance.get_maneuver_plan_screening(
            plan_id, scenario_id, tradespace_id
        )
        cli_output_model(resource)


@app.command()
def maneuver_profile(resource_id: str):
    """Retrieve an Avoidance Maneuver Profile by ID."""
    with KayhanClient() as client:
        resource = client.avoidance.get_maneuver_profile(resource_id)
        cli_output_model(resource)


@app.command()
def trigger(resource_id: str):
    """Retrieve an Avoidance Maneuver Trigger by ID."""
    with KayhanClient() as client:
        resource = client.avoidance.get_trigger(resource_id)
        cli_output_model(resource)


# Administration


@app.command()
def profile():
    """Retrieve User Profile of authenticated user."""
    with KayhanClient() as client:
        resource = client.administration.get_profile()
        cli_output_model(resource)


@app.command()
def organization():
    """Retrieve Organization of authenticated user."""
    with KayhanClient() as client:
        resource = client.administration.get_organization()
        cli_output_model(resource)


@app.command()
def user(resource_id: str):
    """Retrieve User Profile by ID."""
    with KayhanClient() as client:
        resource = client.administration.get_user(resource_id)
        cli_output_model(resource)


@app.command()
def api_key(resource_id: str):
    """Retrieve API Key by ID."""
    with KayhanClient() as client:
        resource = client.administration.get_api_key(resource_id)
        cli_output_model(resource)


# Asset Management


@app.command()
def constellation(resource_id: str):
    """Retrieve Constellation by ID (slug)."""
    with KayhanClient() as client:
        resource = client.asset_management.get_constellation(resource_id)
        cli_output_model(resource)


@app.command()
def default_constellation():
    """Retrieve Organization's default Constellation"""
    with KayhanClient() as client:
        resource = client.asset_management.get_default_constellation()
        cli_output_model(resource)


@app.command()
def rso(resource_id: str):
    """Retrieve RSO by ID (NORAD ID)."""
    with KayhanClient() as client:
        resource = client.asset_management.get_rso(resource_id)
        cli_output_model(resource)
