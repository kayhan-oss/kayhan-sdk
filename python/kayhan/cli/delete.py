import typer

from typer import Abort

from kayhan.cli.utils import cli_output

from kayhan.sdk.client import KayhanClient

app = typer.Typer()

# Avoidance Maneuver


@app.command()
def maneuver_profile(
    resource_id: str,
):
    try:
        action = typer.confirm(
            "Are you sure you want to delete this profile?", abort=True
        )
    except Abort:
        cli_output("Aborting maneuver profile delete action.")
        raise typer.Exit()

    if action:
        with KayhanClient() as client:
            res = client.avoidance.delete_maneuver_profile(profile_id=resource_id)

        cli_output(res)


@app.command()
def trigger(
    resource_id: str,
):
    try:
        action = typer.confirm(
            "Are you sure you want to delete this trigger?", abort=True
        )
    except Abort:
        cli_output("Aborting trigger delete action.")
        raise typer.Exit()

    if action:
        with KayhanClient() as client:
            res = client.avoidance.delete_trigger(trigger_id=resource_id)

        cli_output(res)


# Administration


@app.command()
def api_keys(
    resource_ids: str = typer.Option(
        None,
        "--ids",
        help="comma separated, e.g. 1234,5678 | pass in a single ID without a comma to execute action for single API key",
    ),
):
    try:
        action = typer.confirm(
            "Are you sure you want to delete these API keys?", abort=True
        )
    except Abort:
        cli_output("Aborting API keys batch delete action.")
        raise typer.Exit()

    if action:
        stripped_api_key_ids = resource_ids.split(",")
        with KayhanClient() as client:
            res = client.administration.delete_api_keys(ids=stripped_api_key_ids)

        cli_output(res)


# Asset Management


@app.command(help="Provide constellation 'slug' for resource_id")
def constellation(
    resource_id: str,
):
    try:
        action = typer.confirm(
            "Are you sure you want to delete this constellation?", abort=True
        )
    except Abort:
        cli_output("Aborting constellation delete action.")
        raise typer.Exit()

    if action:
        with KayhanClient() as client:
            res = client.asset_management.delete_constellation(
                constellation_slug=resource_id
            )

        cli_output(res)
