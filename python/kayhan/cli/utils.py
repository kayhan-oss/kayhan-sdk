import inspect
import json
import sys
import rich
import typer

from enum import Enum
from typing import TYPE_CHECKING, Callable, List, Optional, TextIO, Type, Union

from pydantic import BaseModel
from rich.console import Console
from rich.progress import Progress, SpinnerColumn, TextColumn
from rich.table import Table

from kayhan.sdk.administration import models as administration_models
from kayhan.sdk.asset_management import models as asset_management_models
from kayhan.sdk.avoidance import models as avoidance_models
from kayhan.sdk.propagation import models as propagation_models
from kayhan.sdk.screening import models as screening_models

if TYPE_CHECKING:
    from kayhan.sdk.client import KayhanClient

from kayhan.sdk.settings import OutputFormat, settings


class FetchingSpinner:
    global_enable = True

    def __enter__(self):
        self.progress = None
        if (
            settings.cli.show_progress
            and settings.cli.is_cli
            and FetchingSpinner.global_enable
        ):
            self.progress = Progress(
                SpinnerColumn(),
                TextColumn("[progress.description]Fetching data..."),
                transient=True,
            )
            self.progress.__enter__()
            self.progress.add_task(description="Fetching data...", total=None)

        return self

    def __exit__(self, *exc_args):
        if self.progress is not None:
            self.progress.__exit__(*exc_args)


def screening_await_cli(client: "KayhanClient", screening: screening_models.Screening):
    if settings.cli.output_format == OutputFormat.RICH and settings.cli.show_progress:
        FetchingSpinner.global_enable = False
        gen = client.screening.await_screening_completion_percent_generator(screening)

        with Progress() as progress:
            task = progress.add_task("Running screening...", total=100)
            try:
                while True:
                    pct = next(gen)
                    progress.update(task, completed=pct)
            except StopIteration as stop:
                screening = stop.value
        FetchingSpinner.global_enable = True
    else:
        screening = client.screening.await_screening_completion(screening)
    return screening


def attribute_seek(base: BaseModel, path: str):
    try:
        for key in path.split("."):
            base = getattr(base, key)
        if isinstance(base, Enum):
            base = base.value
        return base
    except AttributeError:
        return None
    except TypeError:
        return None


def rich_hyperlink_field(path: str, url_format: str) -> str:
    class _hyperlink:
        def __call__(self, base: BaseModel):
            value = attribute_seek(base, path)
            url = url_format.format(value)
            return f"[link={url}]{value}[/link]"

        @property
        def field_name(self):
            return path

    return _hyperlink()


def resolve_field(
    base: BaseModel, operator: Union[str, Callable[[BaseModel], str]]
) -> str:
    if isinstance(operator, str):
        value = attribute_seek(base, operator)
    else:
        value = operator(base)
    if value is None:
        return "N/A"
    return str(value)


def resolve_field_name(field):
    def fmt(n):
        return n.replace("_", " ").replace(".", " ").title()

    try:
        return fmt(field)
    except AttributeError:
        return fmt(field.field_name)


def cli_output(
    d: Union[BaseModel, dict],
    out: TextIO = sys.stdout,
    indent: int = 2,
):
    if settings.cli.output_format == OutputFormat.JSON:
        rich.print(f"{json.dumps(d, indent=indent)}", file=out)
    elif settings.cli.output_format == OutputFormat.RICH:
        rich.print(d, file=out)


def cli_output_list(
    d: List[BaseModel],
    out: TextIO = sys.stdout,
    indent=2,
    fields: Optional[List[str]] = None,
):
    ds = [json.loads(i.json()) for i in d]

    if fields is None:
        try:
            fields = FIELDS[type(d[0])]
        except KeyError:
            try:
                fields = ds[0].keys()
            except IndexError:
                fields = []

    if settings.cli.output_format == OutputFormat.JSON:
        rich.print(json.dumps(ds, indent=indent), file=out)
    elif settings.cli.output_format == OutputFormat.RICH:
        console = Console(file=out)

        if len(d) == 0:
            rich.print("[bold red]No results found.[/bold red]", file=out)
        else:
            fields_pretty = [resolve_field_name(f) for f in fields]
            table = Table(*fields_pretty)
            for item in d:
                table.add_row(*[resolve_field(item, f) for f in fields])
            console.print(table)


def cli_output_model(
    model: BaseModel,
    out: TextIO = sys.stdout,
    indent=2,
    exclude_none=True,
    fields: Optional[List[str]] = None,
):
    if fields is None:
        try:
            fields = FIELDS[type(model)]
        except KeyError:
            try:
                fields = model.__fields__.keys()
            except IndexError:
                fields = []

    if settings.cli.output_format == OutputFormat.JSON:
        rich.print(model.json(indent=indent, exclude_none=exclude_none), file=out)
    elif settings.cli.output_format == OutputFormat.RICH:
        console = Console(file=out)

        fields_pretty = [resolve_field_name(f) for f in fields]
        table = Table(*fields_pretty)
        table.add_row(*[resolve_field(model, f) for f in fields])
        console.print(table)


def display_class_keys_and_values(cls_or_module):
    help_text = f"\n{cls_or_module.__name__} Keys and Allowed Values:\n\n"

    if inspect.isclass(cls_or_module):
        if hasattr(cls_or_module, "__annotations__"):
            for attr_name, attr_value in cls_or_module.__annotations__.items():
                default_value = getattr(cls_or_module, attr_name, None)
                if default_value:
                    help_text += (
                        f"  {attr_name}: {attr_value} (Default: {default_value})\n\n"
                    )
                else:
                    help_text += f"  {attr_name}: {attr_value}\n\n"
        else:
            help_text += "  (No type annotations found)\n\n"
    elif inspect.ismodule(cls_or_module):
        for name, obj in inspect.getmembers(cls_or_module):
            if inspect.isclass(obj):
                help_text += f"\nClass {name}:\n"
                help_text += display_class_keys_and_values(obj)
    else:
        help_text += "Not a class or module.\n"

    return help_text


def validate_model_from_input(
    ctx: typer.Context, model: Type[BaseModel], flag_prefix: str
):
    validation_data = {}

    for arg_idx, extra_arg in enumerate(ctx.args):
        if extra_arg.startswith(flag_prefix):
            key_value_pair = extra_arg[len(flag_prefix) :].replace("-", "_")

            if "=" in key_value_pair:
                key, value = key_value_pair.split("=", 1)
                validation_data[key] = value
            else:
                if arg_idx + 1 < len(ctx.args):
                    validation_data[key_value_pair] = ctx.args[arg_idx + 1]

    try:
        validated_model = model.parse_obj(validation_data)
        return validated_model
    except Exception:
        raise ValueError(f"Invalid values provided for {model}")


FIELDS = {
    screening_models.Ephemeris: [
        "id",
        "norad_cat_id",
        "solution_time",
        "has_covariance",
        "notes",
        "hbr_m",
        "context",
        "designation",
    ],
    screening_models.Screening: [
        rich_hyperlink_field("id", settings.pathfinder_base_url + "screenings/{}"),
        "title",
        "screening_type",
        "status",
        "created_at",
        "conjunctions_count",
        "primary_ids_preview",
        "percent_complete",
        "coverage_level",
    ],
    screening_models.Conjunction: [
        "primary.norad_cat_id",
        "secondary.norad_cat_id",
        "tca",
        "miss_distance_km",
        "collision_probability",
    ],
    screening_models.Screenable: [
        "ephemeris.id",
        "ephemeris.norad_cat_id",
        "catalog_id",
        "norad_cat_id",
        "coverage_level",
    ],
    screening_models.Catalog: [
        "id",
        "catalog_type",
        "epoch",
        "ready",
        "archived",
    ],
    propagation_models.Propagation: [
        "id",
        "status",
        "created_at",
        "start_time",
        "end_time",
        "target_duration",
        "timestep",
        "ephemeris_id",
    ],
    avoidance_models.Profile: [
        rich_hyperlink_field(
            "id", settings.pathfinder_base_url + "asset_management/maneuver_profiles/{}"
        ),
        "name",
        "maneuver_algo_type",
        "burn_type",
        "tradespace_algorithm_type",
        "id",
        "fidelity_type",
        "fidelity_value",
        "fidelity_unit",
    ],
    avoidance_models.Tradespace: [
        "name",
        "tradespace_algorithm",
        "fidelity_type",
        "fidelity_value",
        "fidelity_unit",
        "algorithm_type",
        "algo_setting",
        "total_plans",
        "completed_plans",
        "profile_id",
    ],
    avoidance_models.Trigger: [
        rich_hyperlink_field(
            "id", settings.pathfinder_base_url + "settings/triggers/{}"
        ),
        "title",
        "notes",
        "types",
        "assets",
        "is_paused",
        "triggered_count",
        "last_triggered_at",
    ],
    administration_models.UserProfile: [
        "id",
        "organization.name",
        "service_tier",
        "role",
        "email",
        "auth_id",
        "logins_count",
    ],
    administration_models.Organization: [
        rich_hyperlink_field(
            "id", settings.pathfinder_base_url + "coordination/org_profile"
        ),
        "slug",
        "name",
        "level",
        "service_tier",
        "org_type",
        "gamut_service_tier",
        "directory_listing",
        "share_operational_ephemeris",
        "space_track_fetch_enabled",
    ],
    administration_models.APIKey: [
        rich_hyperlink_field(
            "id", settings.pathfinder_base_url + "administration/api_keys"
        ),
        "username",
        "client_id",
        "client_secret",
        "created_at",
    ],
    administration_models.OrganizationDirectoryRead: [
        rich_hyperlink_field(
            "id", settings.pathfinder_base_url + "coordination/operator_directory"
        ),
        "directory_name",
        "type",
        "contact_name",
        "contact_phone",
        "contact_email",
        "website",
        "country",
    ],
    asset_management_models.Constellation: [
        rich_hyperlink_field(
            "id", settings.pathfinder_base_url + "asset_management/assets"
        ),
        "name",
        "slug",
        "organization.name",
        "is_default",
        "can_delete",
        "rso_count",
    ],
    asset_management_models.RSO: [
        rich_hyperlink_field(
            "norad_cat_id",
            settings.pathfinder_base_url + "asset_management/assets/{}/edit_asset",
        ),
        "object_name",
        "object_type",
        "constellation.name",
        "mobility_type",
        "maneuverable",
        "maneuverability_classification",
        "is_decayed",
        "hbr_m",
    ],
}
