from typing import Optional

import typer

from kayhan.cli import utils
from kayhan.sdk.client import KayhanClient

app = typer.Typer()
count_option = typer.Option(None, "-n", "--count")

# Screening


@app.command()
def ephemerides(count: Optional[int] = count_option, catalog_id: Optional[str] = None):
    with KayhanClient() as client:
        if catalog_id:
            resources = client.screening.list_catalog_ephemerides(
                catalog_id, count=count
            )
            utils.cli_output_list(resources)
        else:
            resources = client.screening.list_ephemerides(count=count)
            utils.cli_output_list(resources)


@app.command()
def screenings(count: Optional[int] = count_option, show_archived: bool = False):
    with KayhanClient() as client:
        resources = client.screening.list_screenings(
            count=count, show_archived=show_archived
        )
        utils.cli_output_list(resources)


@app.command()
def catalogs(count: Optional[int] = count_option, latest: bool = False):
    with KayhanClient() as client:
        resources = client.screening.list_catalogs(count=count, latest=latest)
        utils.cli_output_list(resources)


@app.command()
def conjunctions(screening_id: str, count: Optional[int] = count_option):
    with KayhanClient() as client:
        resources = client.screening.list_conjunctions(
            screening_id=screening_id, count=count
        )
        utils.cli_output_list(resources)


@app.command()
def screening_primaries(screening_id: str, count: Optional[int] = count_option):
    with KayhanClient() as client:
        resources = client.screening.list_screening_primaries(
            screening_id=screening_id, count=count
        )
        utils.cli_output_list(resources)


@app.command()
def screening_secondaries(screening_id: str, count: Optional[int] = count_option):
    with KayhanClient() as client:
        resources = client.screening.list_screening_secondaries(
            screening_id=screening_id, count=count
        )
        utils.cli_output_list(resources)


@app.command()
def ephemeris_formats():
    with KayhanClient() as client:
        resources = client.screening.list_ephemeris_formats()
        utils.cli_output(resources)


@app.command()
def conjunctions_ccsds(resource_id: str, count: Optional[int] = count_option):
    with KayhanClient() as client:
        resources = client.screening.list_conjunctions_ccsds(resource_id, count)
        utils.cli_output(resources)


# Avoidance Maneuver


@app.command()
def scenarios(count: Optional[int] = count_option, cdm_id: Optional[int] = None):
    with KayhanClient() as client:
        if cdm_id is None:
            resources = client.avoidance.list_scenarios(count=count)
        else:
            resources = client.avoidance.list_scenarios_for_cdm(cdm_id, count=count)
        if len(resources) > 0:
            utils.cli_output_list(resources)
        else:
            typer.echo("Empty List")


@app.command()
def tradespaces(scenario_id: str, count: Optional[int] = count_option):
    with KayhanClient() as client:
        resources = client.avoidance.list_tradespaces(
            scenario_id=scenario_id, count=count
        )
        utils.cli_output_list(resources)


@app.command()
def plans(tradespace_id: str, scenario_id: str):
    with KayhanClient() as client:
        resources = client.avoidance.list_maneuver_plans(
            tradespace_id=tradespace_id, scenario_id=scenario_id
        )
        utils.cli_output_list(resources)


@app.command()
def maneuver_profiles(count: Optional[int] = count_option):
    with KayhanClient() as client:
        resources = client.avoidance.list_maneuver_profiles(count=count)
        utils.cli_output_list(resources)


@app.command()
def triggers(count: Optional[int] = count_option):
    with KayhanClient() as client:
        resources = client.avoidance.list_triggers(count=count)
        utils.cli_output_list(resources)


# Administration


@app.command()
def organization_users(count: Optional[int] = count_option):
    with KayhanClient() as client:
        resources = client.administration.list_organization_users(count=count)
        utils.cli_output_list(resources)


@app.command()
def api_keys(count: Optional[int] = count_option):
    with KayhanClient() as client:
        resources = client.administration.list_api_keys(count=count)
        utils.cli_output_list(resources)


@app.command()
def directory(count: Optional[int] = count_option):
    with KayhanClient() as client:
        resources = client.administration.list_organizations_directory(count=count)
        utils.cli_output_list(resources)


@app.command()
def directory_countries():
    with KayhanClient() as client:
        resources = client.administration.list_directory_countries()
        utils.cli_output(resources)


# Asset Management


@app.command()
def constellations(count: Optional[int] = count_option):
    with KayhanClient() as client:
        resources = client.asset_management.list_organization_constellations(
            count=count
        )
        utils.cli_output_list(resources)


@app.command()
def constellation_rsos(constellation_slug: str, count: Optional[int] = count_option):
    with KayhanClient() as client:
        resources = client.asset_management.list_constellation_rsos(
            constellation_slug=constellation_slug, count=count
        )
        utils.cli_output_list(resources)


@app.command()
def rsos(count: Optional[int] = count_option):
    with KayhanClient() as client:
        resources = client.asset_management.list_rsos(count=count)
        utils.cli_output_list(resources)
