from datetime import datetime
from io import StringIO
from pathlib import Path
from typing import List, Optional

import shutil
import typer

from kayhan.cli.utils import (
    cli_output_model,
    screening_await_cli,
    validate_model_from_input,
)

from kayhan.sdk.administration import models as administration_models
from kayhan.sdk.asset_management import models as asset_management_models
from kayhan.sdk.avoidance import models as avm_models
from kayhan.sdk.client import KayhanClient
from kayhan.sdk.propagation import models as prop_models
from kayhan.sdk.screening import models as screening_models

app = typer.Typer()

# Screening


@app.command()
def ephemeris(
    file: Path,
    file_format: str = typer.Argument(...),
    norad_cat_id: Optional[int] = None,
    comments: Optional[str] = None,
    hbr_m: Optional[float] = None,
    context: Optional[screening_models.EphemerisContext] = None,
    designation: Optional[screening_models.EphemerisDesignation] = None,
):

    with KayhanClient() as client:
        resource = client.screening.create_ephemeris(
            file,
            file_format=file_format,
            filename=file.name,
            norad_cat_id=norad_cat_id,
            comments=comments,
            hbr_m=hbr_m,
            context=context,
            designation=designation,
        )
        cli_output_model(resource)


@app.command()
def ephemeris_from_maneuver_plan(
    scenario_id: str = typer.Option(None, "--scenario-id", "--sid"),
    tradespace_id: str = typer.Option(None, "--tradespace-id", "--tid"),
    plan_id: str = typer.Option(None, "--plan-id", "--pid"),
    designation: Optional[screening_models.EphemerisDesignation] = typer.Option(
        None, "--designation", "--d"
    ),
):

    with KayhanClient() as client:
        resource = client.avoidance.create_ephemeris_from_maneuver_plan(
            scenario_id, tradespace_id, plan_id, designation
        )
        cli_output_model(resource)


@app.command()
def screening(
    primary_ephemeris_file: Optional[Path] = typer.Option(
        None, "--primary-ephemeris-file", "--pef"
    ),
    primary_ephemeris_format: Optional[str] = typer.Option(
        None, "--primary-ephemeris-format", "--pfmt"
    ),
    primary_norad_cat_id: Optional[int] = typer.Option(
        None, "--primary-norad-cat-id", "--pid"
    ),
    primary_ephemeris_ids: Optional[List[str]] = None,
    secondary_ephemeris_ids: Optional[List[str]] = None,
    primary_opm_file: Optional[Path] = typer.Option(None, "--opm"),
    catalog_id: Optional[str] = None,
    threshold_radius_km: float = 15.0,
    threshold_radius_active_km: Optional[float] = None,
    threshold_radius_manned_km: Optional[float] = None,
    threshold_radius_debris_km: Optional[float] = None,
    default_secondary_hbr_m: float = 5.0,
    propagation_start_time: Optional[datetime] = None,
    propagation_duration: Optional[float] = None,
    propagation_timestep: Optional[float] = None,
    launch_window_start: Optional[datetime] = None,
    launch_window_end: Optional[datetime] = None,
    launch_window_cadence_s: Optional[float] = None,
    type: screening_models.ScreeningType = screening_models.ScreeningType.ON_ORBIT,
    auto_archive: bool = False,
    include_primary_vs_primary: bool = False,
    title: Optional[str] = "Screening",
    notes: Optional[str] = None,
    submit: bool = typer.Option(False, "--submit", "-s"),
    use_best_catalog: bool = True,
    use_ephemeris_repository: bool = False,
    wait: bool = typer.Option(
        False,
        "--wait",
        "-w",
        help=(
            "Whether to wait for the screening to complete or return after creating and"
            " submitting the asynchronous screening job."
        ),
    ),
):

    if wait and not submit:
        # Cannot wait without submitting
        submit = True

    with KayhanClient() as client:

        primaries = [
            screening_models.Screenable(ephemeris_id=i) for i in primary_ephemeris_ids
        ]
        secondaries = [
            screening_models.Screenable(ephemeris_id=i) for i in secondary_ephemeris_ids
        ]

        if primary_opm_file is not None:
            with open(primary_opm_file) as opm_f:
                propagation = client.propagation.create_propagation(opm_file=opm_f)
            primaries.append(screening_models.Screenable(propagation_id=propagation.id))

        catalog = None

        if use_best_catalog:
            catalog = client.screening.get_latest_catalog()
            secondaries.append(screening_models.Screenable(catalog_id=catalog.id))

        elif catalog_id is not None:
            catalog = client.screening.get_catalog(catalog_id)

        if use_ephemeris_repository:
            secondaries.append(
                screening_models.Screenable(
                    ephemeris_group=screening_models.EphemerisGroup.OPERATOR_REPOSITORY
                )
            )

        if primary_ephemeris_file:
            primary_ephemeris = client.screening.create_ephemeris(
                primary_ephemeris_file,
                file_format=primary_ephemeris_format,
                norad_cat_id=primary_norad_cat_id,
            )
            primaries.append(
                screening_models.Screenable(ephemeris_id=primary_ephemeris.id)
            )
        elif primary_norad_cat_id:
            if catalog:
                primaries.append(
                    screening_models.Screenable(
                        catalog_id=catalog.id, norad_cat_id=primary_norad_cat_id
                    )
                )
            else:
                raise ValueError(
                    "primary_norad_cat_id can only be used if"
                    "primary_ephemeris_file or catalog_id is set"
                )

        config = screening_models.ScreeningConfiguration(
            threshold_radius_km=threshold_radius_km,
            threshold_radius_active_km=threshold_radius_active_km,
            threshold_radius_manned_km=threshold_radius_manned_km,
            threshold_radius_debris_km=threshold_radius_debris_km,
            default_secondary_hbr_m=default_secondary_hbr_m,
            submit=submit,
            notes=notes,
            propagation_duration=propagation_duration,
            propagation_start_time=propagation_start_time,
            propagation_timestep=propagation_timestep,
            auto_archive=auto_archive,
            title=title,
            include_primary_vs_primary=include_primary_vs_primary,
            screening_type=type.upper(),
            launch_window_start=launch_window_start,
            launch_window_end=launch_window_end,
            launch_window_cadence_s=launch_window_cadence_s,
        )

        screening = client.screening.create_screening(
            config,
            primaries=primaries,
            submit=submit,
            secondaries=secondaries,
        )

        screening = client.screening.get_screening(screening.id)

        if wait:
            screening = screening_await_cli(client, screening)

        cli_output_model(screening)


@app.command()
def propagation(
    opm_file: Optional[Path] = typer.Option(None, "--opm-file", "--opm"),
    submit: bool = typer.Option(False, "--submit", "-s"),
    target_duration_s: Optional[float] = typer.Option(None, "--duration-sec", "-d"),
    timestep_s: Optional[float] = typer.Option(None, "--timestep-sec", "-t"),
    purpose: Optional[str] = typer.Option(None),
    wait: bool = typer.Option(
        False,
        "--wait",
        "-w",
        help=(
            "Whether to wait for the propagation to complete or return after creating"
            " and submitting the asynchronous propagation job."
        ),
    ),
):
    configuration = prop_models.PropagationConfiguration(
        timestep=timestep_s, target_duration=target_duration_s, purpose=purpose
    )

    opm_file_content = StringIO()
    if opm_file is not None:
        with open(opm_file) as f:
            shutil.copyfileobj(f, opm_file_content)
            opm_file_content.seek(0)
    else:
        opm_file_content = None

    with KayhanClient() as client:
        propagation = client.propagation.create_propagation(
            configuration, opm_file=opm_file_content, submit=submit
        )

        if wait:
            propagation = client.propagation.await_propagation_completion(propagation)

    cli_output_model(propagation)


# Avoidance Maneuver


@app.command()
def scenario(
    name: str = typer.Option(None, "--name"),
    autogenerated: bool = typer.Option(None, "--auto-generated", "--ag"),
    cdm_id: int = typer.Option(None, "--cdm-id", "--cid"),
    constellation: str = typer.Option(None, "--constellation", "--c"),
    asset_id: str = typer.Option(None, "--asset-id", "--aid"),
    asset_norad_cat_id: int = typer.Option(None, "--asset-norad-cat-id"),
):
    with KayhanClient() as client:
        configuration = avm_models.ScenarioConfiguration(
            name=name,
            autogenerated=autogenerated,
            cdm_id=cdm_id,
            constellation=constellation,
            asset_id=asset_id,
            asset_norad_cat_id=asset_norad_cat_id,
        )
        scenario = client.avoidance.create_scenario(config=configuration)

    cli_output_model(scenario)


@app.command()
def scenario_with_tradespace(
    cdm_key_or_id: str = typer.Option(
        None, "--cdm-id", help="CDM Key or ID can be used"
    ),
    profile_id: str = typer.Option(
        None, "--profile-id", help="ID of avoidance maneuver profile to use"
    ),
    override_hbr_m: float = typer.Option(None, "--hbr", help="m"),
    override_maneuverable: Optional[bool] = typer.Option(
        None,
        "--override-maneuverable",
    ),
    mass_kg: Optional[float] = typer.Option(None, "--mass", help="kg"),
    thrust_n: Optional[float] = typer.Option(None, "--thrust", help="n"),
    isp_s: Optional[float] = typer.Option(None, "--isp", help="s"),
    srp_coefficient_m: Optional[float] = typer.Option(None, "--srp-coeff"),
    srp_area_m: Optional[float] = typer.Option(None, "--srp-area", help="m"),
    drag_coefficient_m: Optional[float] = typer.Option(None, "--drag-coeff"),
    drag_area_m: Optional[float] = typer.Option(None, "--drag-area", help="m"),
):
    with KayhanClient() as client:
        configuration = avm_models.TradespaceRun(
            profile_id=profile_id,
            rso_physical_attributes=avm_models.RSOPhysicalAttributes(
                mass_kg=mass_kg,
                thrust_n=thrust_n,
                isp_s=isp_s,
                override_maneuverable=override_maneuverable,
                override_hbr_m=override_hbr_m,
                srp_coefficient_m=srp_coefficient_m,
                srp_area_m=srp_area_m,
                drag_coefficient_m=drag_coefficient_m,
                drag_area_m=drag_area_m,
            ),
        )
        scenario = client.avoidance.create_scenario_with_tradespace_configuration(
            cdm_key_or_id=cdm_key_or_id,
            config=configuration,
        )

    cli_output_model(scenario)


@app.command()
def tradespace(
    scenario_id: str = typer.Argument(...),
    name: str = typer.Option(None, "--name"),
    start_time: datetime = typer.Option(None, "--start-time", "--st"),
    end_time: datetime = typer.Option(None, "--end-time", "--et"),
    tradespace_algorithm: str = typer.Option(None, "--tradespace-algorithm", "--ta"),
    fidelity_type: str = typer.Option(None, "--fidelity-type", "--ftype"),
    fidelity_value: int = typer.Option(None, "--fidelity-value", "--fval"),
    fidelity_unit: str = typer.Option(None, "--fidelity-unit", "--funit"),
    algorithm_type: str = typer.Option(None, "--algorithm-type", "--atype"),
):
    with KayhanClient() as client:
        configuration = avm_models.TradespaceConfiguration(
            name=name,
            start_time=start_time,
            end_time=end_time,
            tradespace_algorithm=tradespace_algorithm,
            fidelity_type=fidelity_type,
            fidelity_value=fidelity_value,
            fidelity_unit=fidelity_unit,
            algorithm_type=algorithm_type,
        )
        tradespace = client.avoidance.add_scenario_tradespace(
            scenario_id, config=configuration
        )

    cli_output_model(tradespace)


@app.command()
def tradespace_append(
    scenario_id: str = typer.Option(
        None,
        "--scenario-id",
        help="ID of avoidance maneuver scenario to add tradespace to",
    ),
    profile_id: str = typer.Option(
        None, "--profile-id", help="ID of avoidance maneuver profile to use"
    ),
    override_hbr_m: float = typer.Option(None, "--hbr", help="m"),
    override_maneuverable: Optional[bool] = typer.Option(
        None,
        "--override-maneuverable",
    ),
    mass_kg: Optional[float] = typer.Option(None, "--mass", help="kg"),
    thrust_n: Optional[float] = typer.Option(None, "--thrust", help="n"),
    isp_s: Optional[float] = typer.Option(None, "--isp", help="s"),
    srp_coefficient_m: Optional[float] = typer.Option(None, "--srp-coeff"),
    srp_area_m: Optional[float] = typer.Option(None, "--srp-area", help="m"),
    drag_coefficient_m: Optional[float] = typer.Option(None, "--drag-coeff"),
    drag_area_m: Optional[float] = typer.Option(None, "--drag-area", help="m"),
):
    with KayhanClient() as client:
        configuration = avm_models.TradespaceRun(
            profile_id=profile_id,
            rso_physical_attributes=avm_models.RSOPhysicalAttributes(
                mass_kg=mass_kg,
                thrust_n=thrust_n,
                isp_s=isp_s,
                override_maneuverable=override_maneuverable,
                override_hbr_m=override_hbr_m,
                srp_coefficient_m=srp_coefficient_m,
                srp_area_m=srp_area_m,
                drag_coefficient_m=drag_coefficient_m,
                drag_area_m=drag_area_m,
            ),
        )
        scenario = client.avoidance.append_scenario_tradespace(
            scenario_id=scenario_id,
            config=configuration,
        )

    cli_output_model(scenario)


@app.command(
    help=f"maneuver_algo_type and maneuver_algo are determined by attributes of maneuver_algo with the --maneuver-algorithm- prefix. For example, to submit a DIFF_DRAG type, supply values for the following flags: --maneuver-algorithm-drag-profile --maneuver-algorithm-max-drag-time-h --maneuver-algorithm-drag-area-min-m-2 --maneuver-algorithm-drag-area-max-m-2",
    context_settings={"allow_extra_args": True, "ignore_unknown_options": True},
)
def maneuver_profile(
    ctx: typer.Context,
    name: str = typer.Option(None, "--name"),
    burn_type: Optional[avm_models.BurnType] = None,
    tradespace_algorithm: avm_models.TradespaceAlgorithm = typer.Option(
        None, "--tradespace-algorithm"
    ),
    fidelity_type: Optional[avm_models.TradespaceFidelityType] = None,
    fidelity_unit: str = typer.Option(
        None,
        "--fidelity-unit",
        help="min | hr | day",
    ),
    fidelity_value: float = typer.Option(None, "--fidelity-value"),
):
    algorithm_models = [
        (
            avm_models.ManeuverAlgorithm.FIXED_DV,
            avm_models.ManeuverAlgorithmConfigurationFixedDV,
        ),
        (
            avm_models.ManeuverAlgorithm.OPTIMAL,
            avm_models.ManeuverAlgorithmConfigurationOptimal,
        ),
        (
            avm_models.ManeuverAlgorithm.DIFF_DRAG,
            avm_models.ManeuverAlgorithmConfigurationDiffDrag,
        ),
    ]

    maneuver_algo_dict = None
    maneuver_algo_type = None

    for algo_type, model_class in algorithm_models:
        try:
            maneuver_algo_dict = validate_model_from_input(
                ctx, model_class, "--maneuver-algorithm-"
            )
            maneuver_algo_type = algo_type
            break
        except ValueError:
            continue

    if not maneuver_algo_dict:
        typer.echo(
            "Error: No valid configuration for any supported Maneuver Algorithms was provided."
        )
        raise typer.Exit(code=1)

    with KayhanClient() as client:
        configuration = avm_models.ProfileConfiguration(
            name=name,
            maneuver_algo_type=maneuver_algo_type.upper(),
            maneuver_algo=maneuver_algo_dict,
            burn_type=burn_type.upper() if burn_type else None,
            tradespace_algorithm=tradespace_algorithm.upper(),
            fidelity_type=fidelity_type.upper() if fidelity_type else None,
            fidelity_unit=fidelity_unit,
            fidelity_value=fidelity_value,
        )
        maneuver_profile = client.avoidance.create_maneuver_profile(
            config=configuration
        )

    cli_output_model(maneuver_profile)


@app.command(
    help="A constellation can only be linked to one trigger at a time, creating a new linked constellation will replace previous unlinked configurations. If link=False, constellation can be applied to any number of triggers."
)
def trigger(
    slug: str = typer.Option(None, "--slug", help="constellation slug"),
    title: str = typer.Option(None, "--title"),
    include: Optional[str] = typer.Option(
        None, "--include", help="comma separated, e.g. '1234,5678'"
    ),
    exclude: Optional[str] = typer.Option(
        None, "--exclude", help="comma separated, e.g. '1234,5678'"
    ),
    link: Optional[bool] = typer.Option(
        False,
        "--link",
        help="will default to True if both include and exclude are None",
    ),
    notes: Optional[str] = typer.Option(None, "--notes"),
    is_paused: Optional[bool] = typer.Option(False, "--paused"),
    max_miss_distance_km: Optional[float] = typer.Option(
        None,
        "--max-miss-distance",
        "--max-md",
        help="greater than 1 meter and less than 5 kilometers, inclusive",
    ),
    min_collision_probability: Optional[float] = typer.Option(
        None,
        "--min-collision-probability",
        "--min-pc",
        help="greater than 1e-5 and less than 1, inclusive",
    ),
    md_pc_logical_operator: Optional[avm_models.LogicalOperator] = typer.Option(
        None, "--op"
    ),
    max_window_under_tca_hr: Optional[float] = typer.Option(
        1,
        "--max-window-under-tca",
        "--tca-w",
        help="greater than 1 hour and less than 5 days (120hr), inclusive",
    ),
):
    include_list = include.split(",") if include else None
    exclude_list = exclude.split(",") if exclude else None
    linked = True if include_list is None and exclude_list is None else link

    with KayhanClient() as client:
        configuration = avm_models.TriggerConfiguration(
            title=title,
            notes=notes,
            is_paused=is_paused,
            types=[
                avm_models.TriggerDetail(
                    type=avm_models.TriggerType.AVOIDANCE_MANEUVER,
                    max_miss_distance=max_miss_distance_km,
                    max_miss_distance_unit=avm_models.LengthUnit.KILOMETER,
                    min_collision_probability=min_collision_probability,
                    md_pc_logical_operator=md_pc_logical_operator,
                    max_window_under_tca=max_window_under_tca_hr,
                    max_window_under_tca_unit=avm_models.TimeUnit.HOURS,
                )
            ],
            assets=[
                avm_models.TriggerAsset(
                    constellation_slug=slug,
                    include=include_list,
                    exclude=exclude_list,
                    link_constellation=linked,
                )
            ],
        )
        trigger = client.avoidance.create_trigger(
            config=configuration,
        )

    cli_output_model(trigger)


# Administration


@app.command()
def user(
    role: administration_models.Role = typer.Option(None, "--role", "--r"),
    email: str = typer.Option(None, "--email", "--e"),
    username: str = typer.Option(None, "--username", "--n"),
):
    with KayhanClient() as client:
        configuration = administration_models.UserProfileConfiguration(
            role=role,
            email=email,
            username=username,
        )
        user = client.administration.create_organization_user(config=configuration)

    cli_output_model(user)


@app.command(
    help="client_secret will only be supplied when creating a new api_key, it cannot be retrieved later and the secret must be rotated if the original client_secret value cannot be provided."
)
def api_key(
    name: str = typer.Option(None, "--name", "--n"),
):
    with KayhanClient() as client:
        user = client.administration.create_api_key(name)

    cli_output_model(user)


# Asset Management


@app.command()
def constellation(
    name: str = typer.Option(None, "--name", "--n"),
    slug: Optional[str] = typer.Option(
        None,
        "--slug",
        "--s",
        help="Kayhan recommends that only 'name' be supplied so a slug can be generated automatically",
    ),
):
    with KayhanClient() as client:
        configuration = asset_management_models.ConstellationConfiguration(
            name=name, slug=slug
        )
        constellation = client.asset_management.create_constellation(
            config=configuration
        )

    cli_output_model(constellation)
