import json
from copy import deepcopy
from io import StringIO
from typing import TYPE_CHECKING, Collection, Dict, List, Optional, TextIO
from urllib.parse import urljoin

from kayhan.sdk.avoidance import models
from kayhan.sdk.screening import models as screening_models
from kayhan.sdk.settings import settings
from kayhan.sdk.utils import SortDirection, remove_none_vals

DEFAULT_SORT_DIRECTION = SortDirection.DESC
DEFAULT_SORT_FIELD = "created_at"
MANEUVER_SORT_FIELD = "initial_ignition_time"

if TYPE_CHECKING:
    from kayhan.sdk.client import KayhanClient


class AvoidanceClient:
    client: "KayhanClient"

    def __init__(self, client: "KayhanClient"):
        self.client = client

    def list_scenarios(
        self,
        filters: Optional[List[Dict]] = None,
        count: Optional[int] = None,
        sort_field: str = DEFAULT_SORT_FIELD,
        sort_direction: "SortDirection" = DEFAULT_SORT_DIRECTION,
    ) -> Collection[models.Scenario]:
        """List all avoidance maneuver scenarios to which the authenticated user has access.

        :param filters: Filters to apply to the list of resources, defaults to None.
        :type filters: Optional[List[Dict]], optional
        :param count: Maximum number of results to return. If None, return the entire
            collection. Defaults to None.
        :type count: Optional[int], optional
        :param sort_field: The field on which to sort results, defaults to DEFAULT_SORT_FIELD.
        :type sort_field: str, optional
        :param sort_direction: The direction in which to sort results, defaults to "asc".
        :type sort_direction: str, optional
        :return: The retrieved avoidance maneuver scenarios.
        :rtype: Collection[models.Scenario]
        """
        path = urljoin(settings.api_avm_url, "avoidance_maneuvers")
        items = self.client.request_list(
            path,
            filters=filters,
            count=count,
            sort_direction=sort_direction,
            sort_field=sort_field,
        )
        results = [models.Scenario.parse_obj(item) for item in items]
        return results

    def list_scenarios_for_cdm(
        self,
        cdm_id: int,
        filters: Optional[List[Dict]] = None,
        count: Optional[int] = None,
        sort_field: str = DEFAULT_SORT_FIELD,
        sort_direction: "SortDirection" = DEFAULT_SORT_DIRECTION,
    ) -> Collection[models.Scenario]:
        """List all avoidance maneuver scenarios to which the authenticated user has access
        for a given CDM. The cdm_id is given by the ``CDM_ID`` parameter of CDMs produced by
        18SpCS.

        This is a convenience function which filters the results of ``list_scenarios``
        to only include scenarios generated for a particular CDM.

        :param filters: Filters to apply to the list of resources, defaults to None.
        :type filters: Optional[List[Dict]], optional
        :param count: Maximum number of results to return. If None, return the entire
            collection. Defaults to None.
        :type count: Optional[int], optional
        :param sort_field: The field on which to sort results, defaults to DEFAULT_SORT_FIELD.
        :type sort_field: str, optional
        :param sort_direction: The direction in which to sort results, defaults to "asc".
        :type sort_direction: str, optional
        :return: The retrieved avoidance maneuver scenarios.
        :rtype: Collection[models.Scenario]
        """
        filters = deepcopy(filters)
        if filters is None:
            filters = []
        filters.append({"field": "cdm_id", "value": cdm_id})
        return self.list_scenarios(filters, count, sort_field, sort_direction)

    def get_scenario(self, id: str) -> models.Scenario:
        """Retrieve an avoidance maneuver scenario.

        :param id: ID of the avoidance maneuver scenario.
        :type id: str
        :return: The retrieved avoidance maneuver scenario.
        :rtype: models.Scenario
        """
        path = urljoin(settings.api_avm_url, f"avoidance_maneuvers/{id}")
        res = self.client.request_authenticated(path)
        data = res.json()
        return models.Scenario.parse_obj(data)

    def list_tradespaces(
        self,
        scenario_id: str,
        filters: Optional[List[Dict]] = None,
        count: Optional[int] = None,
        sort_field: str = DEFAULT_SORT_FIELD,
        sort_direction: "SortDirection" = DEFAULT_SORT_DIRECTION,
    ) -> Collection[models.Tradespace]:
        """List the tradespaces associated with an avoidance maneuver scenario.

        :param scenario_id: The id of the scenario.
        :type scenario_id: str
        :param filters: Filters to apply to the list of resources, defaults to None.
        :type filters: Optional[List[Dict]], optional
        :param count: Maximum number of results to return. If None, return the entire
            collection. Defaults to None.
        :type count: Optional[int], optional
        :param sort_field: The field on which to sort results, defaults to DEFAULT_SORT_FIELD.
        :type sort_field: str, optional
        :param sort_direction: The direction in which to sort results, defaults to "asc".
        :type sort_direction: str, optional
        :return: The retrieved collection of resources.
        :rtype: Collection[models.Tradespace]
        """
        path = urljoin(
            settings.api_avm_url, f"avoidance_maneuvers/{scenario_id}/tradespaces"
        )
        items = self.client.request_list(
            path,
            filters=filters,
            count=count,
            sort_field=sort_field,
            sort_direction=sort_direction,
        )
        results = [models.Tradespace.parse_obj(item) for item in items]
        return results

    def get_tradespace(self, tradespace_id: str, scenario_id: str) -> models.Tradespace:
        """Retrieve an avoidance maneuver tradespace.

        .. note::
            While ``scenario_id`` is currently a required
            parameter due to technical constraints, it will be made optional
            in a future release.

        :param tradespace_id: ID of the avoidance maneuver tradespace.
        :type tradespace_id: str
        :param scenario_id: ID of the avoidance maneuver scenario.
        :type scenario_id: str
        :return: The retrieved avoidance maneuver tradespace.
        :rtype: models.Scenario
        """
        path = urljoin(
            settings.api_avm_url,
            f"avoidance_maneuvers/{scenario_id}/tradespaces/{tradespace_id}",
        )
        res = self.client.request_authenticated(path)
        return models.Tradespace.parse_obj(res.json())

    def list_maneuver_plans(
        self,
        tradespace_id: str,
        scenario_id: str,
        filters: Optional[List[Dict]] = None,
        sort_field: str = MANEUVER_SORT_FIELD,
        sort_direction: "SortDirection" = DEFAULT_SORT_DIRECTION,
    ) -> Collection[models.ManeuverPlan]:
        """List all avoidance maneuver scenario plans to which the authenticated user has access.

        .. note::
            While ``scenario_id`` is currently a required
            parameter due to technical constraints, it will be made optional
            in a future release.

        :param tradespace_id: ID of the avoidance maneuver tradespace.
        :type tradespace_id: str
        :param scenario_id: ID of the avoidance maneuver scenario.
        :type scenario_id: str
        :param filters: Filters to apply to the list of resources, defaults to None.
        :type filters: Optional[List[Dict]], optional
        :param count: Maximum number of results to return. If None, return the entire
            collection. Defaults to None.
        :type count: Optional[int], optional
        :param sort_field: The field on which to sort results, defaults to MANEUVER_SORT_FIELD.
        :type sort_field: str, optional
        :param sort_direction: The direction in which to sort results, defaults to "asc".
        :type sort_direction: str, optional
        :return: The retrieved avoidance maneuver scenario plans.
        :rtype: Collection[models.ManeuverPlan]
        """
        path = urljoin(
            settings.api_avm_url,
            f"avoidance_maneuvers/{scenario_id}/tradespaces/{tradespace_id}/plans",
        )
        items = self.client.request_list(
            path,
            filters=filters,
            sort_field=sort_field,
            sort_direction=sort_direction,
        )
        results = []
        for item in items:
            if item["maneuvers_count"] != len(item["maneuvers_preview"]):
                plan = self.get_maneuver_plan(item["id"], tradespace_id, scenario_id)
            else:
                item["maneuvers"] = item["maneuvers_preview"]
                plan = models.ManeuverPlan.parse_obj(item)
            results.append(plan)
        return results

    def get_maneuver_plan(
        self, plan_id: str, tradespace_id: str, scenario_id: str
    ) -> models.ManeuverPlan:
        """Retrieve an avoidance maneuver scenario plan.

        .. note::
            While the ``scenario_id`` and ``tradespace_id`` are currently required
            parameters due to technical constraints, they will be made optional
            in a future release.

        :param plan_id: ID of the avoidance maneuver plan.
        :type plan_id: str
        :param tradespace_id: ID of the avoidance maneuver tradespace.
        :type tradespace_id: str
        :param scenario_id: ID of the avoidance maneuver scenario.
        :type scenario_id: str
        :return: The retrieved avoidance maneuver tradespace.
        :rtype: models.ManeuverPlan
        """
        path = urljoin(
            settings.api_avm_url,
            f"avoidance_maneuvers/{scenario_id}/tradespaces/{tradespace_id}/plans/{plan_id}",
        )
        res = self.client.request_authenticated(path)
        return models.ManeuverPlan.parse_obj(res.json())

    def create_maneuver_plan_screening(
        self, plan_id: str, scenario_id: str, tradespace_id: str
    ) -> screening_models.Screening:
        """Create a Pathfinder screening for an avoidance maneuver scenario plan.

        .. note::
            While ``scenario_id`` and ``tradespace_id`` are currently required
            parameters due to technical constraints, they will be made optional
            in a future release.

        :param plan_id: ID of the avoidance maneuver plan.
        :type plan_id: str
        :param scenario_id: ID of the avoidance maneuver scenario.
        :type scenario_id: str
        :param tradespace_id: ID of the avoidance maneuver tradespace.
        :type tradespace_id: str
        :return: The retrieved Pathfinder screening.
        :rtype: screening_models.Screening
        """
        path = urljoin(
            settings.api_avm_url,
            f"avoidance_maneuvers/{scenario_id}/tradespaces/{tradespace_id}/plans/{plan_id}/screening",
        )
        res = self.client.request_authenticated(path, method="POST")
        gemini_screening = screening_models.GeminiScreening.parse_obj(res.json())
        screening = self.client.screening.get_screening(gemini_screening.id)
        return screening

    def get_maneuver_plan_screening(
        self, plan_id: str, scenario_id: str, tradespace_id: str
    ) -> screening_models.Screening:
        """Retrieve an avoidance maneuver scenario plan's associated Pathfinder screening.

        .. note::
            While ``scenario_id`` and ``tradespace_id`` are currently required
            parameters due to technical constraints, they will be made optional
            in a future release.

        :param plan_id: ID of the avoidance maneuver plan.
        :type plan_id: str
        :param scenario_id: ID of the avoidance maneuver scenario.
        :type scenario_id: str
        :param tradespace_id: ID of the avoidance maneuver tradespace.
        :type tradespace_id: str
        :return: The retrieved Pathfinder screening.
        :rtype: screening_models.Screening
        """
        path = urljoin(
            settings.api_avm_url,
            f"avoidance_maneuvers/{scenario_id}/tradespaces/{tradespace_id}/plans/{plan_id}/screening",
        )
        res = self.client.request_authenticated(path)
        gemini_screening = screening_models.GeminiScreening.parse_obj(res.json())
        screening = self.client.screening.get_screening(gemini_screening.id)
        return screening

    def download_maneuver_plan(
        self,
        plan_id: str,
        scenario_id: str,
        tradespace_id: str,
        file_format: str,
        file_name: str,
    ) -> TextIO:
        """Download the content of an avoidance maneuver plan.

        .. note::
            While ``scenario_id`` and ``tradespace_id`` are currently required
            parameters due to technical constraints, they will be made optional
            in a future release.

        :param plan_id: The ID of the avoidance maneuver plan to retrieve.
        :type plan_id: str
        :param scenario_id: The ID of the associated avoidance maneuver scenario.
        :type scenario_id: str
        :param tradespace_id: The ID of the associated avoidance maneuver tradespace.
        :type tradespace_id: str

        :param file_format: The file format to download the avoidance maneuver plan in.

            .. note::
                Only the formats ``"json"``, ``"oem"`` and ``"opm"`` are currently supported.
                This field is case insensitive.

        :type file_format: str
        :return: A file-like object opened in text mode containing the contents
            of the formatted avoidance maneuver plan file.
        :rtype: TextIO
        """
        path = urljoin(
            settings.api_avm_url,
            f"avoidance_maneuvers/{scenario_id}/tradespaces/{tradespace_id}/plans/{plan_id}/screening/file",
        )
        params = {"format": file_format.lower(), "filename": file_name}
        res = self.client.request_authenticated(path, params=params)
        outbuf = StringIO()
        outbuf.write(res.content.decode())
        outbuf.seek(0)
        return outbuf

    def create_scenario_with_tradespace_configuration(
        self, cdm_key_or_id: str, config: models.TradespaceRun
    ) -> models.Scenario:
        """Manually generate a maneuver scenario for processing based on a CDM Key and submit. Submitting a maneuver scenario is an idempotent operation. Once a maneuver scenario has been submitted and entered the PENDING state, submitting it again will have no further effect.\n
        If the maneuver scenario as configured at submission fails validation, the maneuver scenario will return HTTP status 422 (Unprocessable Entity) and the maneuver scenario will not enter the PENDING state until the configuration is updated and the maneuver scenario is submitted again.\n
        This will automatically create a new tradespace and apply it to the newly created scenario.\n
        The profile.id in the config object will overwrite an existing maneuver profile associated with the RSO.

        :param cdm_key_or_id: The id or key of the CDM that will be associated with the new scenario.
        :type cdm_key_or_id: str
        :param config: The TradespaceRun configuration.
        :type config: models.TradespaceRun
        :return: Scenario that the tradespace belongs to.
        :rtype: models.Scenario
        """

        data = json.dumps(remove_none_vals(config.dict()), default=str)

        path = urljoin(
            settings.api_avm_url,
            f"avoidance_maneuvers/cdm/{cdm_key_or_id}/generate",
        )
        res = self.client.request_authenticated(
            path, method="PUT", json=json.loads(data)
        )

        scenario = models.Scenario.parse_obj(res.json())

        return scenario

    def create_scenario(
        self, config: Optional[models.ScenarioConfiguration]
    ) -> models.Scenario:
        """Create an avoidance maneuver scenario on the server.

        :param config: The configuration with which to create the scenario.
        :type config: Optional[models.ScenarioConfiguration]
        :return: The created avoidance manuever scenario.
        :rtype: models.Scenario
        """
        if config is None:
            config = models.ScenarioConfiguration()
        data = remove_none_vals(config.dict())
        path = urljoin(settings.api_avm_url, "avoidance_maneuvers")
        res = self.client.request_authenticated(path, json=data, method="POST")
        scenario = models.Scenario.parse_obj(res.json())

        return scenario

    def add_scenario_tradespace(
        self, scenario_id: str, config: models.TradespaceConfiguration
    ) -> models.Tradespace:
        """Add a Tradespace to an avoidance maneuver scenario, this will replace an existing Tradespace.

        :param scenario_id: The id of the scenario to add an avoidance maneuver tradespace to.
        :type scenario_id: str
        :param config: The Tradespace configuration.
        :type config: models.TradespaceConfiguration
        :return: Newly added avoidance maneuver tradespace.
        :rtype: models.Tradespace
        """

        data = json.dumps(remove_none_vals(config.dict()), default=str)

        path = urljoin(
            settings.api_avm_url, f"avoidance_maneuvers/{scenario_id}/tradespaces"
        )
        res = self.client.request_authenticated(
            path, method="POST", json=json.loads(data)
        )

        tradespace = models.Tradespace.parse_obj(res.json())

        return tradespace

    def append_scenario_tradespace(
        self, scenario_id: str, config: models.TradespaceRun
    ) -> models.Scenario:
        """Add a Tradespace to an avoidance maneuver scenario, this will add a new Tradespace to a list of existing Tradespaces. The profile.id in the config object will overwrite an existing maneuver profile associated with the RSO.

        :param scenario_id: The id of the scenario to add an avoidance maneuver tradespace to.
        :type scenario_id: str
        :param config: The TradespaceRun configuration.
        :type config: models.TradespaceRun
        :return: Scenario that the tradespace belongs to.
        :rtype: models.Scenario
        """

        data = json.dumps(remove_none_vals(config.dict()), default=str)

        path = urljoin(
            settings.api_avm_url,
            f"avoidance_maneuvers/{scenario_id}/tradespaces/append",
        )
        res = self.client.request_authenticated(
            path, method="POST", json=json.loads(data)
        )

        scenario = models.Scenario.parse_obj(res.json())

        return scenario

    def list_maneuver_profiles(
        self,
        filters: Optional[List[Dict]] = None,
        count: Optional[int] = None,
        sort_field: str = DEFAULT_SORT_FIELD,
        sort_direction: "SortDirection" = DEFAULT_SORT_DIRECTION,
    ) -> Collection[models.Profile]:
        """List all avoidance maneuver profiles to which the authenticated user has access.

        :param filters: Filters to apply to the list of resources, defaults to None.
        :type filters: Optional[List[Dict]], optional
        :param count: Maximum number of results to return. If None, return the entire
            collection. Defaults to None.
        :type count: Optional[int], optional
        :param sort_field: The field on which to sort results, defaults to DEFAULT_SORT_FIELD.
        :type sort_field: str, optional
        :param sort_direction: The direction in which to sort results, defaults to "asc".
        :type sort_direction: str, optional
        :return: The retrieved avoidance maneuver profiles.
        :rtype: Collection[models.Profile]
        """
        path = urljoin(settings.api_avm_url, "profiles")
        items = self.client.request_list(
            path,
            filters=filters,
            count=count,
            sort_direction=sort_direction,
            sort_field=sort_field,
        )
        results = [models.Profile.parse_obj(item) for item in items]
        return results

    def get_maneuver_profile(
        self,
        profile_id: str,
    ) -> models.Profile:
        """Retrieve an avoidance maneuver profile.

        :param profile_id: ID of the avoidance maneuver profile.
        :type profile_id: str
        :return: The retrieved avoidance maneuver profile.
        :rtype: models.Profile
        """
        path = urljoin(
            settings.api_avm_url,
            f"profiles/{profile_id}",
        )
        res = self.client.request_authenticated(path)
        return models.Profile.parse_obj(res.json())

    def create_maneuver_profile(
        self, config: models.ProfileConfiguration
    ) -> models.Profile:
        """Create an avoidance maneuver profile on the server.

        :param config: The configuration with which to create the profile.
        :type config: models.ProfileConfiguration
        :return: The created avoidance manuever profile.
        :rtype: models.Profile
        """
        data = remove_none_vals(config.dict())
        path = urljoin(settings.api_avm_url, "profiles")
        res = self.client.request_authenticated(path, json=data, method="POST")
        profile = models.Profile.parse_obj(res.json())

        return profile

    def update_maneuver_profile(
        self, profile_id: str, config: models.ProfileConfiguration
    ) -> models.Profile:
        """Update an avoidance maneuver profile on the server.

        :param config: The configuration with which to update the profile.
        :type config: models.ProfileConfiguration
        :return: The update avoidance manuever profile.
        :rtype: models.Profile
        """
        data = remove_none_vals(config.dict())
        path = urljoin(settings.api_avm_url, f"profiles/{profile_id}")
        res = self.client.request_authenticated(path, json=data, method="PATCH")
        profile = models.Profile.parse_obj(res.json())

        return profile

    def delete_maneuver_profile(self, profile_id: str) -> int:
        """Delete an avoidance maneuver profile on the server.

        :param profile_id: The ID of the profile to delete.
        :type profile_id: str
        :return: The HTTP status code of the DELETE request.
        :rtype: int
        """
        path = urljoin(settings.api_avm_url, f"profiles/{profile_id}")
        res = self.client.request_authenticated(path, method="DELETE")

        return res.status_code

    def create_ephemeris_from_maneuver_plan(
        self,
        scenario_id: str,
        tradespace_id: str,
        plan_id: str,
        designation: Optional[screening_models.EphemerisDesignation] = None,
    ) -> screening_models.Ephemeris:
        """Create an ephemeris from an avoidance maneuver plan and add to server.

        :param scenario_id: ID of the avoidance maneuver scenario.
        :type scenario_id: str
        :param tradespace_id: ID of the avoidance maneuver tradespace.
        :type tradespace_id: str
        :param plan_id: ID of the avoidance maneuver plan.
        :type plan_id: str
        :param designation: Designation applied to new ephemeris.
        :type designation: screening_models.EphemerisDesignation
        :return: The newly generated ephemeris.
        :rtype: models.Ephemeris
        """
        data = remove_none_vals(designation.dict()) if designation is not None else None
        path = urljoin(
            settings.api_avm_url,
            f"avoidance_maneuvers/{scenario_id}/tradespaces/{tradespace_id}/plans/{plan_id}/create_ephemeris",
        )
        res = self.client.request_authenticated(path, json=data, method="PUT")
        ephemeris = screening_models.Ephemeris.parse_obj(res.json())

        return ephemeris

    def list_triggers(
        self,
        filters: Optional[List[Dict]] = None,
        count: Optional[int] = None,
        sort_field: str = DEFAULT_SORT_FIELD,
        sort_direction: "SortDirection" = DEFAULT_SORT_DIRECTION,
    ) -> Collection[models.Trigger]:
        """List all avoidance maneuver triggers to which the authenticated user has access.

        :param filters: Filters to apply to the list of resources, defaults to None.
        :type filters: Optional[List[Dict]], optional
        :param count: Maximum number of results to return. If None, return the entire
            collection. Defaults to None.
        :type count: Optional[int], optional
        :param sort_field: The field on which to sort results, defaults to DEFAULT_SORT_FIELD.
        :type sort_field: str, optional
        :param sort_direction: The direction in which to sort results, defaults to "asc".
        :type sort_direction: str, optional
        :return: The retrieved avoidance maneuver scenarios.
        :rtype: Collection[models.Trigger]
        """
        path = urljoin(settings.api_avm_url, "triggers")
        items = self.client.request_list(
            path,
            filters=filters,
            count=count,
            sort_direction=sort_direction,
            sort_field=sort_field,
        )
        results = [models.Trigger.parse_obj(item) for item in items]
        return results

    def get_trigger(
        self,
        trigger_id: str,
    ) -> models.Trigger:
        """Retrieve an avoidance maneuver trigger.

        :param trigger_id: ID of the avoidance maneuver trigger.
        :type trigger_id: str
        :return: The retrieved avoidance maneuver trigger.
        :rtype: models.Trigger
        """
        path = urljoin(
            settings.api_avm_url,
            f"triggers/{trigger_id}",
        )
        res = self.client.request_authenticated(path)
        return models.Trigger.parse_obj(res.json())

    def create_trigger(self, config: models.TriggerConfiguration) -> models.Trigger:
        """Create an avoidance maneuver trigger on the server.

        :param config: The configuration with which to create the trigger.
        :type config: models.TriggerConfiguration
        :return: The created avoidance manuever trigger.
        :rtype: models.Trigger
        """
        data = remove_none_vals(config.dict())
        path = urljoin(settings.api_avm_url, "triggers")
        res = self.client.request_authenticated(path, json=data, method="POST")
        trigger = models.Trigger.parse_obj(res.json())

        return trigger

    def update_trigger(
        self, trigger_id: str, config: models.TriggerConfiguration
    ) -> models.Trigger:
        """Update an avoidance maneuver trigger on the server.

        :param config: The configuration with which to update the trigger.
        :type config: models.TriggerConfiguration
        :return: The update avoidance manuever trigger.
        :rtype: models.Trigger
        """
        data = remove_none_vals(config.dict())
        path = urljoin(settings.api_avm_url, f"triggers/{trigger_id}")
        res = self.client.request_authenticated(path, json=data, method="PATCH")
        trigger = models.Trigger.parse_obj(res.json())

        return trigger

    def delete_trigger(self, trigger_id: str) -> int:
        """Delete an avoidance maneuver trigger on the server.

        :return: The HTTP status code of the DELETE request.
        :rtype: int
        """
        path = urljoin(settings.api_avm_url, f"triggers/{trigger_id}")
        res = self.client.request_authenticated(path, method="DELETE")

        return res.status_code
