from typing import TYPE_CHECKING, Collection, Dict, List, Optional
from urllib.parse import urljoin

from kayhan.sdk.administration import models
from kayhan.sdk.settings import settings
from kayhan.sdk.utils import (
    SortDirection,
    remove_none_vals,
)

if TYPE_CHECKING:
    from kayhan.sdk.client import KayhanClient

DEFAULT_SORT_DIRECTION = SortDirection.DESC
DEFAULT_SORT_FIELD = "created_at"


class AdministrationClient:
    client: "KayhanClient"

    def __init__(self, client: "KayhanClient"):
        self.client = client

    def get_profile(self) -> models.UserProfile:
        """Get the user profile of the authenticated user.

        :return: The retrieved user profile.
        :rtype: models.UserProfile
        """
        path = urljoin(settings.api_authorization_url, f"profile")
        res = self.client.request_authenticated(path)
        data = res.json()
        return models.UserProfile.parse_obj(data)

    def get_organization(self) -> models.Organization:
        """Get the organization of the authenticated user.

        :return: The retrieved organization record.
        :rtype: models.Organization
        """
        path = urljoin(settings.api_authorization_url, f"profile/organization")
        res = self.client.request_authenticated(path)
        data = res.json()
        return models.Organization.parse_obj(data)

    def update_organization(
        self, config: models.OrganizationConfiguration
    ) -> models.Organization:
        """Update the organization of the authenticated user.

        :param config: The configuration with which to update the organization.
        :type config: models.OrganizationConfiguration
        :return: The updated organization record.
        :rtype: models.Organization
        """
        data = remove_none_vals(config.dict())
        path = urljoin(settings.api_authorization_url, f"profile/organization")
        res = self.client.request_authenticated(path, json=data, method="PATCH")
        return models.Organization.parse_obj(res.json())

    def list_organization_users(
        self,
        filters: Optional[List[Dict]] = None,
        count: Optional[int] = None,
        sort_field: str = DEFAULT_SORT_FIELD,
        sort_direction: "SortDirection" = DEFAULT_SORT_DIRECTION,
    ) -> Collection[models.UserProfile]:
        """Get a list of users within the organization of the authenticated user.

        :return: The retrieved list of users.
        :rtype: Collection[models.UserProfile]
        """
        path = urljoin(settings.api_authorization_url, f"profile/organization/users")
        api_key_filers = (
            [{"op": "eq", "field": "is_api_key", "value": False}]
            if filters is None
            else filters.append({"op": "eq", "field": "is_api_key", "value": False})
        )
        items = self.client.request_list(
            path,
            filters=api_key_filers,
            count=count,
            sort_direction=sort_direction,
            sort_field=sort_field,
        )
        results = [models.UserProfile.parse_obj(item) for item in items]
        return results

    def create_organization_user(
        self, config: models.UserProfileConfiguration
    ) -> models.UserProfile:
        """Create a new user within the organization of the authenticated user.

        :param config: The configuration with which to create the user.
        :type config: models.UserProfileConfiguration
        :return: The created user.
        :rtype: models.UserProfile
        """
        data = remove_none_vals(config.dict())
        path = urljoin(settings.api_authorization_url, f"profile/organization/users")
        res = self.client.request_authenticated(path, json=data, method="POST")
        return models.UserProfile.parse_obj(res.json())

    def get_user(self, user_id: str) -> models.UserProfile:
        """Get a user by ID from the server.

        :param user_id: The ID of the resource to retrieve.
        :type user_id: str
        :return: The retrieved user.
        :rtype: models.UserProfile
        """
        path = urljoin(settings.api_authorization_url, f"users/{user_id}")
        res = self.client.request_authenticated(path)
        return models.UserProfile.parse_obj(res.json())

    def update_user(
        self, user_id: str, config: models.UserProfileConfiguration
    ) -> models.UserProfile:
        """Update a user by ID on the server.

        :param user_id: The ID of the resource to retrieve.
        :type user_id: str
        :param config: The configuration with which to update the user.
        :type config: models.UserProfileConfiguration
        :return: The updated user.
        :rtype: models.UserProfile
        """
        data = remove_none_vals(config.dict())
        path = urljoin(settings.api_authorization_url, f"users/{user_id}")
        res = self.client.request_authenticated(path, json=data, method="PATCH")
        return models.UserProfile.parse_obj(res.json())

    def request_batch_action_users(
        self, user_ids: List[str], action_type: models.BatchUserActionType
    ) -> int:
        """Submit a batch request to trigger an action type.

        :param user_ids: The list of user ids associated with the batch request.
        :type user_ids: List[str]
        :param action_type: The action type to execute.
        :type action_type: models.BatchUserActionType
        :return: The HTTP status code of the PUT request.
        :rtype: int
        """
        path = urljoin(settings.api_authorization_url, f"users/{action_type.lower()}")
        res = self.client.request_authenticated(path, json=user_ids, method="PUT")
        return res.status_code

    def list_api_keys(
        self,
        filters: Optional[List[Dict]] = None,
        count: Optional[int] = None,
        sort_field: str = DEFAULT_SORT_FIELD,
        sort_direction: "SortDirection" = DEFAULT_SORT_DIRECTION,
    ) -> Collection[models.APIKey]:
        """Get a list of API keys from the server.

        :return: The retrieved list of API keys.
        :rtype: Collection[models.APIKey]
        """
        path = urljoin(settings.api_authorization_url, f"api_keys")
        items = self.client.request_list(
            path,
            filters=filters,
            count=count,
            sort_direction=sort_direction,
            sort_field=sort_field,
        )
        results = [models.APIKey.parse_obj(item) for item in items]
        return results

    def create_api_key(self, name: str) -> models.APIKey:
        """Create a new API key on the server.
            Note that `client_secret` is only provided during this POST action, it
            cannot be retrieved again in the future and should be saved for future reference.

        :param name: The name of the API key.
        :type name: str
        :return: The created API key.
        :rtype: models.APIKey
        """
        path = urljoin(settings.api_authorization_url, f"api_keys")
        res = self.client.request_authenticated(
            path, json={"username": name}, method="POST"
        )
        return models.APIKey.parse_obj(res.json())

    def delete_api_keys(self, ids: List[str]) -> int:
        """Delete API Keys from server.

        :param name: The list of API key IDs that will be deleted.
        :type name: List[str]
        :return: The HTTP status code of the DELETE request.
        :rtype: int
        """
        path = urljoin(settings.api_authorization_url, f"api_keys")
        res = self.client.request_authenticated(path, json=ids, method="DELETE")

        return res.status_code

    def get_api_key(
        self,
        api_key_id: str,
    ) -> models.APIKey:
        """Retrieve an API key.

        :param api_key_id: ID of the API key.
        :type api_key_id: str
        :return: The retrieved API key.
        :rtype: models.APIKey
        """
        path = urljoin(
            settings.api_authorization_url,
            f"api_keys/{api_key_id}",
        )
        res = self.client.request_authenticated(path)
        return models.APIKey.parse_obj(res.json())

    def update_api_key(self, api_key_id: str, name: str) -> models.APIKey:
        """Update the name of an existing API key

        :param api_key_id: The ID of the API key.
        :type api_key_id: str
        :param name: The new name of the API key.
        :type name: str
        :return: The updated API key.
        :rtype: models.APIKey
        """
        path = urljoin(settings.api_authorization_url, f"api_keys/{api_key_id}")
        res = self.client.request_authenticated(
            path, json={"username": name}, method="PATCH"
        )
        return models.APIKey.parse_obj(res.json())

    def rotate_api_key_secret(self, api_key_id: str) -> models.APIKey:
        """Rotate the client_secret of an existing API key

        :param api_key_id: The ID of the API key.
        :type api_key_id: str
        :return: The updated API key with a new client_secret, this cannot be retrieved again and should be stored safely for future use.
        :rtype: models.APIKey
        """
        path = urljoin(settings.api_authorization_url, f"api_keys/{api_key_id}/rotate")
        res = self.client.request_authenticated(path, method="PUT")
        return models.APIKey.parse_obj(res.json())

    def list_organizations_directory(
        self,
        filters: Optional[List[Dict]] = None,
        count: Optional[int] = None,
        sort_field: str = DEFAULT_SORT_FIELD,
        sort_direction: "SortDirection" = DEFAULT_SORT_DIRECTION,
    ) -> Collection[models.OrganizationDirectoryRead]:
        """Get a list of Organizations that have opted into the Coordination Directory from the server.

        :return: The retrieved list of Organizations.
        :rtype: Collection[models.OrganizationDirectoryRead]
        """
        path = urljoin(settings.api_authorization_url, f"directory/public/organization")
        items = self.client.request_list(
            path,
            filters=filters,
            count=count,
            sort_direction=sort_direction,
            sort_field=sort_field,
        )
        results = [models.OrganizationDirectoryRead.parse_obj(item) for item in items]
        return results

    def list_directory_countries(
        self,
    ) -> List[str]:
        """Get a list of Countries that can be used in Organization configuration from the server.

        :return: The retrieved list of countries.
        :rtype: List[str]
        """
        path = urljoin(
            settings.api_authorization_url, f"directory/originating_countries"
        )
        response = self.client.request_authenticated(path)
        results = response.json().get("countries", [])
        return results
