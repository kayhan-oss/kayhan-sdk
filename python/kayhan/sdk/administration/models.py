from datetime import datetime
from enum import Enum
from typing import TYPE_CHECKING, Optional
from uuid import UUID

from kayhan.sdk.models import Model

if TYPE_CHECKING:
    from kayhan.sdk.client import KayhanClient


class BatchUserActionType(str, Enum):
    DEACTIVATE = "DEACTIVATE"
    ACTIVATE = "ACTIVATE"
    RESET_PASSWORD = "RESET_PASSWORD"


class DirectoryType(str, Enum):
    USER = "USER"
    ORGANIZATION = "ORGANIZATION"
    ALL = "ALL"


class OrgLevel(str, Enum):
    STANDARD = "STANDARD"
    SUPER = "SUPER"


class OrgType(str, Enum):
    GOVERNMENT = "GOVERNMENT"
    ACADEMIA = "ACADEMIA"
    COMMERCIAL = "COMMERCIAL"
    HYBRID = "HYBRID"


class Role(str, Enum):
    USER = "USER"
    ADMIN = "ADMIN"


class ServiceTier(str, Enum):
    ESSENTIALS = "ESSENTIALS"
    PRO = "PRO"


class APIKey(Model):
    id: UUID
    client_id: str
    client_secret: Optional[str]
    created_at: Optional[datetime]
    username: str


class OrganizationConfiguration(Model):
    contact_name: Optional[str]
    contact_phone: Optional[str]
    contact_email: Optional[str]
    space_track_contact_name: Optional[str]
    space_track_contact_phone: Optional[str]
    space_track_contact_email: Optional[str]
    pc_high_risk: Optional[float]
    pc_low_risk: Optional[float]
    md_high_risk_km: Optional[float]
    md_low_risk_km: Optional[float]
    coordination_participant: Optional[bool]
    directory_listing: Optional[bool]
    share_operational_ephemeris: Optional[bool]
    website: Optional[str]
    country: Optional[str]


class Organization(OrganizationConfiguration):
    id: str
    name: str
    slug: str
    created_at: datetime
    space_track_org_name: str
    level: OrgLevel
    org_type: OrgType
    service_tier: ServiceTier
    gamut_service_tier: Optional[ServiceTier] = None
    rso_count: int
    ssa_sharing_agreement: bool
    pc_fetch_limit: Optional[float]
    md_fetch_limit: Optional[float]
    space_track_fetch_enabled: bool = True
    predictive_covariance_service_enabled: bool = False
    tasking_service_enabled: bool = False
    client: Optional["KayhanClient"] = None


class OrganizationDirectoryRead(OrganizationConfiguration):
    id: UUID
    name: str
    slug: str
    directory_name: str
    type: DirectoryType


class UserProfileConfiguration(Model):
    role: Role
    username: Optional[str]
    email: Optional[str] = None
    deactivated: bool = False
    is_api_key: bool = False


class UserProfile(UserProfileConfiguration):
    id: UUID
    created_at: datetime
    auth_id: Optional[str]
    service_tier: Optional[ServiceTier]
    organization: Optional[Organization]
    last_password_reset: Optional[datetime] = None
    updated_at: Optional[datetime] = None
    last_ip: Optional[str] = None
    last_login: Optional[datetime] = None
    logins_count: Optional[int] = None
