import requests

from kayhan.sdk.settings import settings


def refreshtoken(verbose=True):
    try:
        payload = "audience={}&grant_type=client_credentials&client_id={}&client_secret={}".format(
            settings.auth_audience, settings.auth_id, settings.auth_secret
        )
        headers = {"content-type": "application/x-www-form-urlencoded"}

        response = requests.post(
            settings.auth_url, data=payload, headers=headers, timeout=3.0
        )

        if response.status_code == 200:
            if verbose:
                print("Authentication done.")
            return response.json()["access_token"]
        else:
            if verbose:
                print(
                    "There was an issue refreshing the token. Please make sure your"
                    " credentials are properly set in the config file."
                )
            return False

    except Exception as f_exception:
        if verbose:
            print("An authentication error occurred: {}".format(str(f_exception)))
        return False
