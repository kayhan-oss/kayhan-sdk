import json

from typing import TYPE_CHECKING, Collection, Dict, List, Optional
from urllib.parse import urljoin

from kayhan.sdk.asset_management import models
from kayhan.sdk.settings import settings
from kayhan.sdk.utils import (
    SortDirection,
    kebab_case,
    remove_none_vals,
)

DEFAULT_SORT_DIRECTION = SortDirection.DESC
DEFAULT_SORT_FIELD = "created_at"

if TYPE_CHECKING:
    from kayhan.sdk.client import KayhanClient


class AssetManagementClient:
    client: "KayhanClient"

    def __init__(self, client: "KayhanClient"):
        self.client = client

    def get_constellation(self, constellation_slug: str) -> models.Constellation:
        """Get a constellation by constellation slug from the server.

        :param constellation_slug: The slug of the constellation resource to retrieve.
        :type constellation_slug: str
        :return: The retrieved constellation.
        :rtype: models.Constellation
        """
        path = urljoin(
            settings.api_authorization_url, f"constellations/{constellation_slug}"
        )
        res = self.client.request_authenticated(path)
        data = res.json()
        return models.Constellation.parse_obj(data)

    def get_default_constellation(self) -> models.Constellation:
        """Get the default constellation assigned to the organization of the authenticated user.

        :return: The retrieved constellation.
        :rtype: models.Constellation
        """
        path = urljoin(
            settings.api_authorization_url,
            f"profile/organization/default_constellation",
        )
        res = self.client.request_authenticated(path)
        data = res.json()
        return models.Constellation.parse_obj(data)

    def create_constellation(
        self, config: models.ConstellationConfiguration
    ) -> models.Constellation:
        """Create a constellation record on the server.

        :param config: The configuration with which to create the constellation.
        :type config: models.ConstellationConfiguration
        :return: The created constellation.
        :rtype: models.Constellation
        """
        if not config.slug:
            config.slug = kebab_case(config.name)

        data = remove_none_vals(config.dict())
        path = urljoin(
            settings.api_authorization_url, f"profile/organization/constellations"
        )
        res = self.client.request_authenticated(path, json=data, method="POST")
        return models.Constellation.parse_obj(res.json())

    def update_constellation(
        self, constellation_slug: str, config: models.ConstellationConfiguration
    ) -> models.Constellation:
        """Update a constellation record on the server.

        :param constellation_slug: The slug of the constellation resource to retrieve.
        :type constellation_slug: str
        :param config: The configuration with which to update the constellation.
        :type config: models.ConstellationConfiguration
        :return: The retrieved constellation.
        :rtype: models.Constellation
        """
        data = remove_none_vals(config.dict())
        path = urljoin(
            settings.api_authorization_url, f"constellations/{constellation_slug}"
        )
        res = self.client.request_authenticated(path, json=data, method="PATCH")
        return models.Constellation.parse_obj(res.json())

    def delete_constellation(self, constellation_slug: str) -> int:
        """Delete a constellation from the server.

        :param constellation_slug: The slug of the constellation resource to delete.
        :type constellation_slug: str
        :return: The HTTP status code of the DELETE request.
        :rtype: int
        """
        path = urljoin(
            settings.api_authorization_url, f"constellations/{constellation_slug}"
        )
        res = self.client.request_authenticated(path, method="DELETE")
        return res.status_code

    def list_organization_constellations(
        self,
        filters: Optional[List[Dict]] = None,
        count: Optional[int] = None,
        sort_field: str = DEFAULT_SORT_FIELD,
        sort_direction: "SortDirection" = DEFAULT_SORT_DIRECTION,
    ) -> Collection[models.Constellation]:
        """Get a list of constellations assigned to the organization of the authenticated user.

        :return: The retrieved list of constellations.
        :rtype: Collection[models.Constellation]
        """
        path = urljoin(
            settings.api_authorization_url, f"profile/organization/constellations"
        )
        items = self.client.request_list(
            path,
            filters=filters,
            count=count,
            sort_direction=sort_direction,
            sort_field=sort_field,
        )
        results = [models.Constellation.parse_obj(item) for item in items]
        return results

    def list_constellation_rsos(
        self,
        constellation_slug: str,
        filters: Optional[List[Dict]] = None,
        count: Optional[int] = None,
        sort_field: str = DEFAULT_SORT_FIELD,
        sort_direction: "SortDirection" = DEFAULT_SORT_DIRECTION,
    ) -> Collection[models.RSO]:
        """Get a list of RSOs within a constellation by constellation slug from the server.

        :param constellation_slug: The slug of the constellation resource to retrieve.
        :type constellation_slug: str
        :return: The retrieved list of RSOs.
        :rtype: Collection[models.RSO]
        """
        path = urljoin(
            settings.api_authorization_url, f"constellations/{constellation_slug}/rsos"
        )
        items = self.client.request_list(
            path,
            filters=filters,
            count=count,
            sort_direction=sort_direction,
            sort_field=sort_field,
        )
        results = [models.RSO.parse_obj(item) for item in items]
        return results

    def get_rso(self, rso_id: str) -> models.RSO:
        """Get an RSO by ID from the server.

        :param rso_id: The ID of the RSO resource to retrieve.
        :type rso_id: str
        :return: The retrieved RSO.
        :rtype: models.RSO
        """
        path = urljoin(settings.api_authorization_url, f"rsos/{rso_id}")
        res = self.client.request_authenticated(path)
        data = res.json()
        return models.RSO.parse_obj(data)

    def list_rsos(
        self,
        filters: Optional[List[Dict]] = None,
        count: Optional[int] = None,
        sort_field: str = DEFAULT_SORT_FIELD,
        sort_direction: "SortDirection" = DEFAULT_SORT_DIRECTION,
    ) -> Collection[models.RSO]:
        """Get a list of RSOs from the server.

        :return: The retrieved constellation RSOs.
        :rtype: Collection[models.RSO]
        """
        path = urljoin(settings.api_authorization_url, f"rsos")
        items = self.client.request_list(
            path,
            filters=filters,
            count=count,
            sort_direction=sort_direction,
            sort_field=sort_field,
        )
        results = [models.RSO.parse_obj(item) for item in items]
        return results

    def batch_update_rsos(
        self, rsos_config: List[models.RSOConfiguration]
    ) -> Collection[models.RSO]:
        """Submit a batch update request for a list of RSOs.

        :param rsos: The list of rso configurations associated with the batch request.
        :type rsos: List[models.RSOConfiguration]
        :return: The updated list of RSOs.
        :rtype: Collection[models.RSO]
        """
        data = json.dumps(
            [remove_none_vals(rso.dict()) for rso in rsos_config], default=str
        )
        path = urljoin(settings.api_authorization_url, f"rsos")
        res = self.client.request_authenticated(
            path, json=json.loads(data), method="PATCH"
        )
        results = [models.RSO.parse_obj(item) for item in res.json()]
        return results
