from datetime import datetime
from enum import Enum
from typing import TYPE_CHECKING, Optional
from uuid import UUID

from kayhan.sdk.models import Model
from kayhan.sdk.administration.models import Organization

if TYPE_CHECKING:
    from kayhan.sdk.client import KayhanClient


class MobilityType(str, Enum):
    DIFFERENTIAL_DRAG = "DIFFERENTIAL_DRAG"
    LOW_THRUST = "LOW_THRUST"
    OTHER = "OTHER"


class RSOStatus(str, Enum):
    ACTIVE = "ACTIVE"
    DEAD = "DEAD"


class RSOType(str, Enum):
    ROCKET_BODY = "ROCKET_BODY"
    PAYLOAD = "PAYLOAD"
    DEBRIS = "DEBRIS"


class SpacecraftSafetyClassification(str, Enum):
    NON_MANEUVERABLE = "NON_MANEUVERABLE"
    MINIMALLY_MANEUVERABLE = "MINIMALLY_MANEUVERABLE"
    MANEUVERABLE = "MANEUVERABLE"
    AUTOMATED = "AUTOMATED"
    CREWED = "CREWED"
    UNDETERMINED = "UNDETERMINED"


class ConstellationConfiguration(Model):
    name: str
    slug: Optional[str]


class Constellation(ConstellationConfiguration):
    id: UUID
    created_at: datetime
    organization: Organization
    is_default: bool
    can_delete: bool
    rso_count: int
    client: Optional["KayhanClient"] = None


class RSOConstellationUpdate(Model):
    slug: str


class RSOConfiguration(Model):
    norad_cat_id: int
    constellation: Optional[RSOConstellationUpdate] = None
    mass_kg: Optional[float] = None
    thrust_n: Optional[float] = None
    isp_s: Optional[float] = None
    override_maneuverable: Optional[bool] = None
    override_hbr_m: Optional[float] = None
    srp_coefficient_m: Optional[float] = None
    srp_area_m: Optional[float] = None
    drag_coefficient_m: Optional[float] = None
    drag_area_m: Optional[float] = None
    mobility_type: Optional[MobilityType] = None
    on_board_autonomous: Optional[bool] = None


class RSO(RSOConfiguration):
    id: UUID
    constellation: Optional[Constellation] = None
    maneuverable: Optional[bool] = None
    directory_maneuverable: Optional[bool] = None
    directory_last_updated: Optional[datetime] = None
    directory_status: Optional[RSOStatus] = None
    object_name: Optional[str] = None
    object_type: Optional[RSOType] = None
    international_designator: Optional[str] = None
    hbr_m: Optional[float] = None
    maneuverability_classification: SpacecraftSafetyClassification = (
        SpacecraftSafetyClassification.UNDETERMINED
    )
    is_decayed: Optional[bool] = None
    is_temporary: Optional[bool] = None
    last_updated_by_user: Optional[datetime] = None
    satcat_last_updated: Optional[datetime] = None
    client: Optional["KayhanClient"] = None
