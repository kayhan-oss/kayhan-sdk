import json as jsonlib
from typing import Any, Collection, Dict, List, Optional, Union
from urllib.error import HTTPError
from urllib.parse import urljoin

import requests

from kayhan.cli import utils as cli_utils
from kayhan.sdk.auth import refreshtoken
from kayhan.sdk.administration import models as administration_models
from kayhan.sdk.administration.administration_client import AdministrationClient
from kayhan.sdk.asset_management import models as asset_management_models
from kayhan.sdk.asset_management.asset_management_client import AssetManagementClient
from kayhan.sdk.avoidance import models as avoidance_models
from kayhan.sdk.avoidance.avoidance_client import AvoidanceClient
from kayhan.sdk.propagation import models as propagation_models
from kayhan.sdk.propagation.propagation_client import PropagationClient
from kayhan.sdk.screening import models as screening_models
from kayhan.sdk.screening.screening_client import ScreeningClient
from kayhan.sdk.settings import settings
from kayhan.sdk.utils import SortDirection


class KayhanClient:
    """Base Client class which may be used to make requests to Kayhan Space APIs.
    Intended to be used as a context manager with the ``with`` keyword:

    .. code-block:: python

        with KayhanClient() as client:
            client.request(...)

    If not used with the ``with`` keyword, it is recommended to explicitly close
    the Client with ``client.close()`` after the application is finished making requests.
    """

    session: requests.Session

    def __init__(self, default_page_size: int = 32):
        """
        :param default_page_size: Default page size to use when sending repeated listing
            requests to a collection endpoint, defaults to 10
        :type page_size: int, optional
        """
        self.session = requests.Session()
        self.authenticated = False
        self.default_page_size = default_page_size

    def __enter__(self):
        return self

    def __exit__(self, *exc_args):
        self.close()

    def close(self):
        """Close the client and any associated network connections."""
        self.session.close()

    def authenticate_session(self):
        """Authenticate the HTTP session with the authentication provider using the
            authentication method determined by ``kayhan.sdk.settings.auth_method`` .

        This is done automatically when using ``request_authenticated`` .
        """
        if settings.auth_method == "user":
            path = urljoin(settings.api_ca_prefix, "login")
            res = self.request(
                path,
                method="POST",
                data={
                    "username": settings.auth_username,
                    "password": settings.auth_password,
                    "grant_type": "password",
                },
            )
            try:
                token = res.json()["access_token"]
            except:
                raise ValueError(
                    f"Authentication Error ({res.status_code}): {res.text}"
                ) from None
            self.session.headers["Authorization"] = f"Bearer {token}"
        elif settings.auth_method == "token":
            token = refreshtoken()
            self.session.headers["Authorization"] = f"Bearer {token}"
        self.authenticated = True

    def request(
        self,
        path: str,
        method: str = "GET",
        data: Optional[dict] = None,
        json: Optional[dict] = None,
        params: Optional[dict] = None,
        files=None,
    ) -> requests.Response:
        """Make an HTTP request to a Kayhan Space API.
        .. warning:: For most use cases, this function should only be called internally by
        high level functions inside the SDK, such as those inside ``client.screening`` .
        """
        url = urljoin(settings.api_base_url, path)
        with cli_utils.FetchingSpinner():
            response = self.session.request(
                method,
                url,
                json=json,
                data=data,
                params=params,
                files=files,
                timeout=settings.default_timeout,
            )
        return response

    def request_authenticated(
        self,
        path: str,
        method: str = "GET",
        data: Optional[dict] = None,
        json: Optional[dict] = None,
        params: Optional[dict] = None,
        ignore_failure: bool = False,
        files=None,
    ) -> requests.Response:
        """Ensure that the HTTP session is authorized and make an HTTP request to
            a Kayhan Space API.

        .. warning:: For most use cases, this function should only be called internally by
            high level functions inside the SDK, such as those inside ``client.screening`` .
        """
        if not self.authenticated:
            self.authenticate_session()
        res = self.request(
            path, method, data=data, json=json, params=params, files=files
        )
        if not str(res.status_code).startswith("2") and not ignore_failure:
            raise HTTPError(path, res.status_code, res.text, res.headers, None)
        return res

    def request_list(
        self,
        path: str,
        filters: Optional[List[Dict]] = None,
        count: Optional[int] = None,
        params: Optional[dict] = None,
        sort_direction: Union[SortDirection, str] = SortDirection.ASC,
        sort_field: str = "id",
    ) -> Collection[Any]:
        """Request a list of resources from a Kayhan Space API and automatically page through
            the results if necessary.

        .. warning:: For most use cases, this function should only be called internally by
            high level functions inside the SDK, such as those inside ``client.screening`` .
        """
        if params is None:
            params = {}
        params["sort_field"] = sort_field
        if isinstance(sort_direction, str):
            sort_direction = SortDirection(sort_direction)
        params["sort_direction"] = sort_direction.value.lower()
        result_items = []
        if filters is not None:
            params["filters"] = jsonlib.dumps(filters)
        if count is not None:
            params["count"] = count
            result_items = self.request_authenticated(
                path, "GET", params=params
            ).json()["items"]
        else:
            params["count"] = self.default_page_size
            result_items = []
            offset = 0
            total_count = None
            while total_count is None or offset <= total_count:
                params["offset"] = offset
                res = self.request_authenticated(path, "GET", params=params)
                data = res.json()
                items = data["items"]
                result_items.extend(items)
                offset += self.default_page_size
                total_count = data["total_count"]

        for item in result_items:
            item["client"] = self

        return result_items

    @property
    def administration(self) -> "AdministrationClient":
        """Library containing SDK functions for interacting with the
        Kayhan Space Administration API."""
        return AdministrationClient(self)

    @property
    def asset_management(self) -> "AssetManagementClient":
        """Library containing SDK functions for interacting with the
        Kayhan Space Asset Management API."""
        return AssetManagementClient(self)

    @property
    def avoidance(self) -> "AvoidanceClient":
        """Library containing SDK functions for interacting with the
        Kayhan Space Avoidance Maneuver API."""
        return AvoidanceClient(self)

    @property
    def propagation(self) -> "PropagationClient":
        """Library containing SDK functions for interacting with the
        Kayhan Space Propagation API."""
        return PropagationClient(self)

    @property
    def screening(self) -> "ScreeningClient":
        """Library containing SDK functions for interacting with the
        Kayhan Space Screening API."""
        return ScreeningClient(self)


administration_models.Organization.update_forward_refs(KayhanClient=KayhanClient)
administration_models.OrganizationConfiguration.update_forward_refs(
    KayhanClient=KayhanClient
)
administration_models.UserProfile.update_forward_refs(KayhanClient=KayhanClient)
administration_models.UserProfileConfiguration.update_forward_refs(
    KayhanClient=KayhanClient
)

asset_management_models.Constellation.update_forward_refs(KayhanClient=KayhanClient)
asset_management_models.ConstellationConfiguration.update_forward_refs(
    KayhanClient=KayhanClient
)
asset_management_models.RSO.update_forward_refs(KayhanClient=KayhanClient)
asset_management_models.RSOConfiguration.update_forward_refs(KayhanClient=KayhanClient)
asset_management_models.RSOConstellationUpdate.update_forward_refs(
    KayhanClient=KayhanClient
)

avoidance_models.Scenario.update_forward_refs(KayhanClient=KayhanClient)
avoidance_models.ScenarioConfiguration.update_forward_refs(KayhanClient=KayhanClient)
avoidance_models.Tradespace.update_forward_refs(KayhanClient=KayhanClient)
avoidance_models.TradespaceConfiguration.update_forward_refs(KayhanClient=KayhanClient)
avoidance_models.Maneuver.update_forward_refs(KayhanClient=KayhanClient)
avoidance_models.ManeuverPlan.update_forward_refs(KayhanClient=KayhanClient)
avoidance_models.Profile.update_forward_refs(KayhanClient=KayhanClient)
avoidance_models.ProfileConfiguration.update_forward_refs(KayhanClient=KayhanClient)
avoidance_models.Trigger.update_forward_refs(KayhanClient=KayhanClient)
avoidance_models.TriggerConfiguration.update_forward_refs(KayhanClient=KayhanClient)

propagation_models.Propagation.update_forward_refs(KayhanClient=KayhanClient)
propagation_models.PropagationConfiguration.update_forward_refs(
    KayhanClient=KayhanClient
)

screening_models.Screening.update_forward_refs(KayhanClient=KayhanClient)
screening_models.Ephemeris.update_forward_refs(KayhanClient=KayhanClient)
screening_models.Catalog.update_forward_refs(KayhanClient=KayhanClient)
screening_models.Conjunction.update_forward_refs(KayhanClient=KayhanClient)
screening_models.Screenable.update_forward_refs(KayhanClient=KayhanClient)
screening_models.Conjunction.update_forward_refs(KayhanClient=KayhanClient)
screening_models.RSO.update_forward_refs(KayhanClient=KayhanClient)
screening_models.GeminiScreening.update_forward_refs(KayhanClient=KayhanClient)
