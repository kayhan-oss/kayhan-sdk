import json
import os
from enum import Enum
from pathlib import Path
from typing import Any, Dict, Optional
from urllib.parse import urljoin

from pydantic import AnyUrl, BaseModel, BaseSettings, Extra, Field, validator


class OutputFormat(str, Enum):
    JSON = "json"
    RICH = "rich"


CONFIG_PATH = None


def json_config_settings_source(settings: BaseSettings) -> Dict[str, Any]:
    """
    A simple settings source that loads variables from a JSON file
    at the project's root.

    Here we happen to choose to use the `env_file_encoding` from Config
    when reading `config.json`
    """
    try:
        if CONFIG_PATH is not None:
            env_path = CONFIG_PATH
        else:
            env_path = os.getenv("KAYHAN_CONFIG_PATH", "config.json")
        encoding = settings.__config__.env_file_encoding
        loaded_json = json.loads(Path(env_path).read_text(encoding))
        lowered_json = {key.lower(): val for key, val in loaded_json.items()}
        return lowered_json
    except FileNotFoundError:
        return {}


class CLISettings(BaseModel):
    is_cli: bool = False
    output_format: OutputFormat = OutputFormat.RICH
    show_progress: bool = True


class Settings(BaseSettings):
    auth_method: str = "user"
    auth_username: Optional[str] = None
    auth_password: Optional[str] = None
    auth_id: Optional[str] = None
    auth_secret: Optional[str] = None
    auth_audience: str = "https://api.kayhan.io"
    auth_url: AnyUrl = "https://ksc.auth0.com/oauth/token"
    pathfinder_base_url: AnyUrl = "https://app.kayhan.io"
    api_prefix: str = "api/"
    api_ca_prefix: str = "ca/"
    api_authorization_prefix: str = "auth/"
    api_avm_prefix: str = "avm/"
    api_ca_base_url_override: Optional[AnyUrl] = None
    api_authorization_base_url_override: Optional[AnyUrl] = None
    api_avm_base_url_override: Optional[AnyUrl] = None
    default_timeout: int = 1200
    cli: CLISettings = Field(default_factory=CLISettings)

    @validator("pathfinder_base_url")
    def pathfinder_base_url_must_end_with_slash(cls, v: str):
        if not v.endswith("/"):
            v = v + "/"
        return v

    class Config:
        env_file_encoding = "utf-8"
        extra = Extra.allow
        env_prefix = "kayhan_"

        @classmethod
        def customise_sources(
            cls,
            init_settings,
            env_settings,
            file_secret_settings,
        ):
            return (
                init_settings,
                json_config_settings_source,
                env_settings,
                file_secret_settings,
            )

    @property
    def api_ca_url(self) -> AnyUrl:
        base_url = self.api_base_url
        if self.api_ca_base_url_override is not None:
            base_url = self.api_ca_base_url_override
        return urljoin(base_url, self.api_ca_prefix)

    @property
    def api_avm_url(self) -> AnyUrl:
        base_url = self.api_base_url
        if self.api_avm_base_url_override is not None:
            base_url = self.api_avm_base_url_override
        return urljoin(base_url, self.api_avm_prefix)

    @property
    def api_authorization_url(self) -> AnyUrl:
        base_url = self.api_base_url
        if self.api_authorization_base_url_override is not None:
            base_url = self.api_authorization_base_url_override
        return urljoin(base_url, self.api_authorization_prefix)

    @property
    def api_base_url(self) -> AnyUrl:
        return urljoin(self.pathfinder_base_url, self.api_prefix)


settings = Settings()


def load_settings_file(config_path: Path):
    global CONFIG_PATH
    CONFIG_PATH = config_path

    new_settings = Settings()
    for field, value in new_settings.__dict__.items():
        setattr(settings, field, value)
