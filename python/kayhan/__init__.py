from importlib import metadata

from .sdk import *

__app_name__ = "kayhan"
__version__ = metadata.version(__package__)
