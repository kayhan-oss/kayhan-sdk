import json
from datetime import datetime as datetime
from io import BytesIO, StringIO
from pathlib import Path

import responses

from kayhan.sdk.propagation.models import Propagation
from kayhan.sdk.screening.models import (
    Catalog,
    Conjunction,
    Ephemeris,
    EphemerisSource,
    Screenable,
    Screening,
)
from kayhan.sdk.screening.screening_client import ScreeningClient

mock_base_url = "http://test.app.kayhan.io"


@responses.activate
def test_get_ephemeris_by_id(
    mock_screening_client: ScreeningClient, mock_ephemeris: Ephemeris
):
    mock_ephemeris_json = json.dumps(mock_ephemeris.dict())

    responses.add(
        responses.GET,
        f"{mock_base_url}/ephemerides/{mock_ephemeris.id}",
        json=json.loads(mock_ephemeris_json),
    )
    resp = mock_screening_client.get_ephemeris(id=mock_ephemeris.id)
    assert isinstance(resp, Ephemeris)


@responses.activate
def test_download_ephemeris(
    mock_screening_client: ScreeningClient, mock_ephemeris: Ephemeris
):
    mock_string_io_json = json.dumps(StringIO.__dict__, default=str)

    responses.add(
        responses.GET,
        f"{mock_base_url}/ephemerides/{mock_ephemeris.id}/file",
        json=json.loads(mock_string_io_json),
    )
    resp = mock_screening_client.download_ephemeris(
        id=mock_ephemeris.id, file_format="SP"
    )
    assert isinstance(resp, StringIO)


@responses.activate
def test_operationalize_ephemeris(
    mock_screening_client: ScreeningClient, mock_ephemeris: Ephemeris
):
    mock_ephemeris_json = json.dumps(mock_ephemeris.dict())

    responses.add(
        responses.PUT,
        f"{mock_base_url}/ephemerides/{mock_ephemeris.id}/operationalize",
        json=json.loads(mock_ephemeris_json),
    )
    resp = mock_screening_client.operationalize_ephemeris(
        ephemeris_id=mock_ephemeris.id
    )
    assert isinstance(resp, Ephemeris)


@responses.activate
def test_get_screening_by_id(
    mock_screening_client: ScreeningClient, mock_screening: Screening
):
    mock_screening_json = json.dumps(mock_screening.dict(), default=str)

    responses.add(
        responses.GET,
        f"{mock_base_url}/screenings/{mock_screening.id}",
        json=json.loads(mock_screening_json),
    )
    resp = mock_screening_client.get_screening(mock_screening.id)
    assert isinstance(resp, Screening)


@responses.activate
def test_get_catalog_by_id(
    mock_screening_client: ScreeningClient, mock_catalog: Catalog
):
    mock_catalog_json = json.dumps(mock_catalog.dict(), default=str)

    responses.add(
        responses.GET,
        f"{mock_base_url}/catalogs/{mock_catalog.id}",
        json=json.loads(mock_catalog_json),
    )
    resp = mock_screening_client.get_catalog(mock_catalog.id)
    assert isinstance(resp, Catalog)


@responses.activate
def test_list_ephemerides(
    mock_screening_client: ScreeningClient, mock_ephemeris: Ephemeris
):
    mock_ephemeris_json = json.dumps(
        {"items": [mock_ephemeris.dict()], "total_count": 1}
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/ephemerides",
        json=json.loads(mock_ephemeris_json),
    )
    resp = mock_screening_client.list_ephemerides(count=1)
    assert isinstance(resp[0], Ephemeris)
    assert len(resp) == 1


@responses.activate
def test_list_screenings(
    mock_screening_client: ScreeningClient, mock_screening: Screening
):
    mock_screening_json = json.dumps(
        {"items": [mock_screening.dict()], "total_count": 1},
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/screenings",
        json=json.loads(mock_screening_json),
    )
    resp = mock_screening_client.list_screenings(count=1)
    assert isinstance(resp[0], Screening)
    assert len(resp) == 1


@responses.activate
def test_list_catalogs(mock_screening_client: ScreeningClient, mock_catalog: Catalog):
    mock_catalog_json = json.dumps(
        {"items": [mock_catalog.dict()], "total_count": 1},
        default=str,
    )
    responses.add(
        responses.GET,
        f"{mock_base_url}/catalogs",
        json=json.loads(mock_catalog_json),
    )
    resp = mock_screening_client.list_catalogs(count=1)
    assert isinstance(resp[0], Catalog)
    assert len(resp) == 1


@responses.activate
def test_list_catalog_ephemerides(
    mock_screening_client: ScreeningClient, mock_catalog: Catalog
):
    mock_json = json.dumps(
        {
            "pagination": {"offset": 0, "count": 1},
            "items": [
                {
                    "id": "mock_id",
                    "norad_cat_id": 22646,
                    "solution_time": "2022-08-23T04:04:34Z",
                    "has_covariance": False,
                    "discontinuities": [],
                    "filename": None,
                    "comments": None,
                    "apogee_km": 7803.49,
                    "perigee_km": 7776.87,
                    "archived": False,
                    "owner_user_account_id": None,
                    "catalog": {
                        "catalog_type": "SP",
                        "epoch": "2022-08-23T00:00:00Z",
                        "archived": False,
                        "filename": "SPEphemeris_2022235.tar.gz",
                        "ready": True,
                        "id": "350ed725-13eb-4301-bfd0-0f7f16990e09",
                        "created_at": "2022-08-24T17:39:54Z",
                    },
                    "ephemeris_source": EphemerisSource.CATALOG,
                    "rso": {
                        "norad_cat_id": 22646,
                        "object_name": "COSMOS 2245",
                        "object_type": "PAYLOAD",
                    },
                }
            ],
            "total_count": 23612,
        }
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/catalogs/{mock_catalog.id}/ephemerides",
        json=json.loads(mock_json),
    )
    resp = mock_screening_client.list_catalog_ephemerides(id=mock_catalog.id, count=1)
    assert isinstance(resp[0], Ephemeris)
    assert len(resp) == 1


@responses.activate
def test_list_conjunctions(
    mock_screening_client: ScreeningClient,
    mock_screening: Screening,
    mock_conjunction: Conjunction,
):
    mock_conjunction_json = json.dumps(
        {"items": [mock_conjunction.dict()], "total_count": 1}, default=str
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/screenings/{mock_screening.id}/conjunctions",
        json=json.loads(mock_conjunction_json),
    )
    resp = mock_screening_client.list_conjunctions(
        screening_id=mock_screening.id, count=1
    )
    assert isinstance(resp[0], Conjunction)
    assert len(resp) == 1


@responses.activate
def test_list_screening_primaries(
    mock_screening_client: ScreeningClient,
    mock_screening: Screening,
    mock_screenable: Screenable,
):
    mock_screenable_json = json.dumps(
        {"items": [mock_screenable.dict()], "total_count": 1}, default=str
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/screenings/{mock_screening.id}/primaries",
        json=json.loads(mock_screenable_json),
    )
    resp = mock_screening_client.list_screening_primaries(
        screening_id=mock_screening.id, count=1
    )
    assert isinstance(resp[0], Screenable)
    assert len(resp) == 1


@responses.activate
def test_list_screening_secondaries(
    mock_screening_client: ScreeningClient,
    mock_screening: Screening,
    mock_screenable: Screenable,
):
    mock_screenable_json = json.dumps(
        {"items": [mock_screenable.dict()], "total_count": 1}, default=str
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/screenings/{mock_screening.id}/secondaries",
        json=json.loads(mock_screenable_json),
    )
    resp = mock_screening_client.list_screening_secondaries(
        screening_id=mock_screening.id, count=1
    )
    assert isinstance(resp[0], Screenable)
    assert len(resp) == 1


@responses.activate
def test_get_latest_catalog(
    mock_screening_client: ScreeningClient, mock_catalog: Catalog
):
    mock_catalog_json = json.dumps(
        {"items": [mock_catalog.dict()], "total_count": 1},
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/catalogs",
        json=json.loads(mock_catalog_json),
    )
    resp = mock_screening_client.get_latest_catalog()
    assert isinstance(resp, Catalog)
    assert resp.id == mock_catalog.id


@responses.activate
def test_add_screening_primary_from_catalog(
    mock_screening_client: ScreeningClient,
    mock_catalog: Catalog,
    mock_screening: Screening,
    mock_screenable: Screenable,
):
    mock_screenable_json = json.dumps(
        {
            "primary_sources_count": 3,
            "primary_sources_preview": [mock_screenable.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.POST,
        f"{mock_base_url}/screenings/{mock_screening.id}/primaries",
        json=json.loads(mock_screenable_json),
    )
    resp = mock_screening_client.add_screening_primary(
        screening=mock_screening,
        primary=mock_catalog,
        norad_cat_id=25544,
    )
    assert isinstance(resp, Screenable)


@responses.activate
def test_add_screening_primary_from_propagation(
    mock_screening_client: ScreeningClient,
    mock_screening: Screening,
    mock_screenable: Screenable,
    mock_propagation: Propagation,
):
    mock_screenable_json = json.dumps(
        {
            "primary_sources_count": 3,
            "primary_sources_preview": [mock_screenable.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.POST,
        f"{mock_base_url}/screenings/{mock_screening.id}/primaries",
        json=json.loads(mock_screenable_json),
    )
    resp = mock_screening_client.add_screening_primary(
        screening=mock_screening,
        primary=mock_propagation,
        norad_cat_id=25544,
    )
    assert isinstance(resp, Screenable)


@responses.activate
def test_add_screening_secondary_from_catalog(
    mock_screening_client: ScreeningClient,
    mock_catalog: Catalog,
    mock_screening: Screening,
    mock_screenable: Screenable,
):
    mock_screenable_json = json.dumps(
        {
            "primary_sources_count": 3,
            "primary_sources_preview": [mock_screenable.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.POST,
        f"{mock_base_url}/screenings/{mock_screening.id}/secondaries",
        json=json.loads(mock_screenable_json),
    )
    resp = mock_screening_client.add_screening_secondary(
        screening=mock_screening,
        secondary=mock_catalog,
        norad_cat_id=25544,
    )
    assert isinstance(resp, Screenable)


@responses.activate
def test_add_screening_secondary_from_propagation(
    mock_screening_client: ScreeningClient,
    mock_screening: Screening,
    mock_propagation: Propagation,
    mock_screenable: Screenable,
):
    mock_screenable_json = json.dumps(
        {
            "primary_sources_count": 3,
            "primary_sources_preview": [mock_screenable.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.POST,
        f"{mock_base_url}/screenings/{mock_screening.id}/secondaries",
        json=json.loads(mock_screenable_json),
    )
    resp = mock_screening_client.add_screening_secondary(
        screening=mock_screening,
        secondary=mock_propagation,
        norad_cat_id=25544,
    )
    assert isinstance(resp, Screenable)


@responses.activate
def test_create_ephemeris_from_bytes_io(
    mock_screening_client: ScreeningClient, mock_ephemeris: Ephemeris
):
    mock_ephemeris_json = json.dumps(mock_ephemeris.dict())

    responses.add(
        responses.POST,
        f"{mock_base_url}/ephemerides",
        json=json.loads(mock_ephemeris_json),
    )
    test_bytes_io: BytesIO = BytesIO(b"tests/data/example1.oem")
    resp = mock_screening_client.create_ephemeris(
        path_or_buf=test_bytes_io,
        file_format="OEM",
        norad_cat_id=25544,
    )
    assert isinstance(resp, Ephemeris)


@responses.activate
def test_create_ephemeris_from_file(
    mock_screening_client: ScreeningClient, mock_ephemeris: Ephemeris
):
    mock_ephemeris_json = json.dumps(mock_ephemeris.dict())

    responses.add(
        responses.POST,
        f"{mock_base_url}/ephemerides",
        json=json.loads(mock_ephemeris_json),
    )
    test_path: Path = Path("tests/data/example1.oem")
    resp = mock_screening_client.create_ephemeris(
        path_or_buf=test_path.absolute(),
        file_format="OEM",
        norad_cat_id=25544,
        filename="test",
    )
    assert isinstance(resp, Ephemeris)


@responses.activate
def test_create_screening(
    mock_screening_client: ScreeningClient, mock_screening: Screening
):
    mock_screening_json = json.dumps(mock_screening.dict(), default=str)

    responses.add(
        responses.POST,
        f"{mock_base_url}/screenings",
        json=json.loads(mock_screening_json),
    )
    resp = mock_screening_client.create_screening()
    assert isinstance(resp, Screening)


@responses.activate
def test_create_screening_best_secondary_catalog(
    mock_screening_client: ScreeningClient,
    mock_catalog: Catalog,
    mock_screening: Screening,
):
    mock_catalog_json = json.dumps(
        {"items": [mock_catalog.dict()], "total_count": 1},
        default=str,
    )
    mock_screening_json = json.dumps(mock_screening.dict(), default=str)

    responses.add(
        responses.GET,
        f"{mock_base_url}/catalogs",
        json=json.loads(mock_catalog_json),
    )
    responses.add(
        responses.POST,
        f"{mock_base_url}/screenings",
        json=json.loads(mock_screening_json),
    )
    resp = mock_screening_client.create_screening(add_best_secondary_catalog=True)
    assert isinstance(resp, Screening)


@responses.activate
def test_submit_screening(
    mock_screening_client: ScreeningClient, mock_screening: Screening
):
    mock_screening_json = json.dumps(mock_screening.dict(), default=str)

    responses.add(
        responses.PUT,
        f"{mock_base_url}/screenings/{mock_screening.id}/submit",
        json=json.loads(mock_screening_json),
    )
    resp = mock_screening_client.submit_screening(screening=mock_screening)
    assert resp.id == mock_screening.id
    assert isinstance(resp, Screening)


@responses.activate
def test_await_screening_completion(
    mock_screening_client: ScreeningClient, mock_screening: Screening
):
    mock_screening_json = json.dumps(mock_screening.dict(), default=str)

    responses.add(
        responses.GET,
        f"{mock_base_url}/screenings/{mock_screening.id}",
        json=json.loads(mock_screening_json),
    )
    resp = mock_screening_client.await_screening_completion(screening=mock_screening)
    assert isinstance(resp, Screening)


@responses.activate
def test_get_api_spec(mock_screening_client: ScreeningClient):
    mock_api_spec_json = json.dumps(
        {
            "openapi": "3.0.2",
            "info": {"title": "Kayhan Spaceflight Safety API"},
            "components": {
                "schemas": {
                    "EphemerisReadFileFormat": {
                        "title": "EphemerisReadFileFormat",
                        "enum": ["OEM", "NASA", "SP", "MOD_ITC"],
                        "description": "Supported Ephemeris file formats for reading",
                    },
                }
            },
        },
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/openapi.json",
        mock_api_spec_json,
    )
    resp = mock_screening_client.get_api_spec()
    assert resp["openapi"] == "3.0.2"
    assert resp["info"]["title"] == "Kayhan Spaceflight Safety API"


@responses.activate
def test_list_ephemeris_formats(mock_screening_client: ScreeningClient):
    mock_api_spec_json = json.dumps(
        {
            "openapi": "3.0.2",
            "info": {"title": "Kayhan Spaceflight Safety API"},
            "components": {
                "schemas": {
                    "EphemerisReadFileFormat": {
                        "title": "EphemerisReadFileFormat",
                        "enum": ["OEM", "NASA", "SP", "MOD_ITC"],
                        "description": "Supported Ephemeris file formats for reading",
                    },
                }
            },
        },
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/openapi.json",
        mock_api_spec_json,
    )
    resp = mock_screening_client.list_ephemeris_formats()

    for format in ["OEM", "NASA", "SP", "MOD_ITC"]:
        assert format in resp
