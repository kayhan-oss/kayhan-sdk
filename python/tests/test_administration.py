import json

import responses


from kayhan.sdk.administration.administration_client import AdministrationClient
from kayhan.sdk.administration.models import (
    APIKey,
    BatchUserActionType,
    Organization,
    OrganizationConfiguration,
    OrganizationDirectoryRead,
    UserProfile,
    UserProfileConfiguration,
)

mock_base_url = "http://test.app.kayhan.io"


@responses.activate
def test_get_profile(
    mock_administration_client: AdministrationClient,
    mock_user_profile: UserProfile,
):
    mock_user_profile_json = json.dumps(
        mock_user_profile.dict(),
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/profile",
        json=json.loads(mock_user_profile_json),
    )
    res = mock_administration_client.get_profile()
    assert isinstance(res, UserProfile)


@responses.activate
def test_get_organization(
    mock_administration_client: AdministrationClient,
    mock_user_organization: Organization,
):
    mock_user_organization_json = json.dumps(
        mock_user_organization.dict(),
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/profile/organization",
        json=json.loads(mock_user_organization_json),
    )
    res = mock_administration_client.get_organization()
    assert isinstance(res, Organization)


@responses.activate
def test_update_organization(
    mock_administration_client: AdministrationClient,
    mock_user_organization: Organization,
    mock_user_organization_config: OrganizationConfiguration,
):
    mock_user_organization_json = json.dumps(
        mock_user_organization.dict(),
        default=str,
    )

    responses.add(
        responses.PATCH,
        f"{mock_base_url}/profile/organization",
        json=json.loads(mock_user_organization_json),
    )
    res = mock_administration_client.update_organization(
        config=mock_user_organization_config
    )
    assert isinstance(res, Organization)


@responses.activate
def test_list_organization_users(
    mock_administration_client: AdministrationClient,
    mock_user_profile: UserProfile,
):
    mock_org_user_json = json.dumps(
        {
            "pagination": {"offset": 0, "count": 1},
            "items": [mock_user_profile.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/profile/organization/users",
        json=json.loads(mock_org_user_json),
    )
    res = mock_administration_client.list_organization_users(count=1)
    assert isinstance(res[0], UserProfile)
    assert len(res) == 1


@responses.activate
def test_create_organization_user(
    mock_administration_client: AdministrationClient,
    mock_user_profile: UserProfile,
    mock_user_profile_config: UserProfileConfiguration,
):
    mock_user_profile_config_json = json.dumps(
        mock_user_profile.dict(),
        default=str,
    )

    responses.add(
        responses.POST,
        f"{mock_base_url}/profile/organization/users",
        json=json.loads(mock_user_profile_config_json),
    )
    res = mock_administration_client.create_organization_user(
        config=mock_user_profile_config
    )
    assert isinstance(res, UserProfile)


@responses.activate
def test_get_user(
    mock_administration_client: AdministrationClient,
    mock_user_profile: UserProfile,
):
    mock_user_profile_json = json.dumps(
        mock_user_profile.dict(),
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/users/{mock_user_profile.id}",
        json=json.loads(mock_user_profile_json),
    )
    res = mock_administration_client.get_user(mock_user_profile.id)
    assert isinstance(res, UserProfile)


@responses.activate
def test_update_user(
    mock_administration_client: AdministrationClient,
    mock_user_profile: UserProfile,
    mock_user_profile_config: UserProfileConfiguration,
):
    mock_user_profile_config_json = json.dumps(
        mock_user_profile.dict(),
        default=str,
    )

    responses.add(
        responses.PATCH,
        f"{mock_base_url}/users/{mock_user_profile.id}",
        json=json.loads(mock_user_profile_config_json),
    )
    res = mock_administration_client.update_user(
        user_id=mock_user_profile.id, config=mock_user_profile_config
    )
    assert isinstance(res, UserProfile)


@responses.activate
def test_batch_action_users(
    mock_administration_client: AdministrationClient,
    mock_user_profile: UserProfile,
    action_type=BatchUserActionType.ACTIVATE,
):
    responses.add(
        responses.PUT,
        f"{mock_base_url}/users/{action_type.lower()}",
        json=[str(mock_user_profile.id)],
    )
    res = mock_administration_client.request_batch_action_users(
        user_ids=[str(mock_user_profile.id)], action_type=action_type
    )
    assert res == 200


@responses.activate
def test_list_api_keys(
    mock_administration_client: AdministrationClient,
    mock_api_key: APIKey,
):
    mock_api_key_json = json.dumps(
        {
            "pagination": {"offset": 0, "count": 1},
            "items": [mock_api_key.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/api_keys",
        json=json.loads(mock_api_key_json),
    )
    res = mock_administration_client.list_api_keys(count=1)
    assert isinstance(res[0], APIKey)
    assert len(res) == 1


@responses.activate
def test_create_api_key(
    mock_administration_client: AdministrationClient,
    mock_api_key: APIKey,
):
    mock_api_key_config = json.dumps(
        mock_api_key.dict(),
        default=str,
    )

    responses.add(
        responses.POST,
        f"{mock_base_url}/api_keys",
        json=json.loads(mock_api_key_config),
    )
    res = mock_administration_client.create_api_key(name="test key")
    assert isinstance(res, APIKey)


@responses.activate
def test_delete_api_key(
    mock_administration_client: AdministrationClient,
    mock_api_key: APIKey,
):
    mock_api_key_config = json.dumps(
        mock_api_key.dict(),
        default=str,
    )

    responses.add(
        responses.DELETE,
        f"{mock_base_url}/api_keys",
        json=json.loads(mock_api_key_config),
    )
    res = mock_administration_client.delete_api_keys(["test-key"])
    assert res == 200


@responses.activate
def test_get_api_key(
    mock_administration_client: AdministrationClient,
    mock_api_key: APIKey,
):
    mock_api_key_json = json.dumps(
        mock_api_key.dict(),
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/api_keys/{mock_api_key.id}",
        json=json.loads(mock_api_key_json),
    )
    res = mock_administration_client.get_api_key(mock_api_key.id)
    assert isinstance(res, APIKey)


@responses.activate
def test_update_api_key(
    mock_administration_client: AdministrationClient,
    mock_api_key: APIKey,
):
    mock_api_key_config_json = json.dumps(
        mock_api_key.dict(),
        default=str,
    )

    responses.add(
        responses.PATCH,
        f"{mock_base_url}/api_keys/{mock_api_key.id}",
        json=json.loads(mock_api_key_config_json),
    )
    res = mock_administration_client.update_api_key(
        api_key_id=mock_api_key.id, name="update-api-key"
    )
    assert isinstance(res, APIKey)


@responses.activate
def test_rotate_api_key_secret(
    mock_administration_client: AdministrationClient,
    mock_api_key: APIKey,
):
    mock_api_key_config_json = json.dumps(
        mock_api_key.dict(),
        default=str,
    )

    responses.add(
        responses.PUT,
        f"{mock_base_url}/api_keys/{mock_api_key.id}/rotate",
        json=json.loads(mock_api_key_config_json),
    )
    res = mock_administration_client.rotate_api_key_secret(api_key_id=mock_api_key.id)
    assert isinstance(res, APIKey)


@responses.activate
def test_organizations_directory(
    mock_administration_client: AdministrationClient,
    mock_organization_directory: OrganizationDirectoryRead,
):
    mock_organization_directory_json = json.dumps(
        {
            "pagination": {"offset": 0, "count": 1},
            "items": [mock_organization_directory.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/directory/public/organization",
        json=json.loads(mock_organization_directory_json),
    )
    res = mock_administration_client.list_organizations_directory(count=1)
    assert isinstance(res[0], OrganizationDirectoryRead)
    assert len(res) == 1
