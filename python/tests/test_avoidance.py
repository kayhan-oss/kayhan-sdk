import json
import responses

from io import StringIO

from kayhan.sdk.avoidance.avoidance_client import AvoidanceClient
from kayhan.sdk.avoidance.models import (
    ManeuverPlan,
    Profile,
    ProfileConfiguration,
    Scenario,
    ScenarioConfiguration,
    Tradespace,
    TradespaceConfiguration,
    TradespaceRun,
    Trigger,
    TriggerConfiguration,
)
from kayhan.sdk.screening.models import Screening, GeminiScreening, Ephemeris

mock_base_url = "http://test.app.kayhan.io"


@responses.activate
def test_list_scenarios(
    mock_avoidance_client: AvoidanceClient, mock_scenario: Scenario
):
    mock_scenario_json = json.dumps(
        {
            "pagination": {"offset": 0, "count": 1},
            "items": [mock_scenario.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/avoidance_maneuvers",
        json=json.loads(mock_scenario_json),
    )
    res = mock_avoidance_client.list_scenarios(count=1)
    assert isinstance(res[0], Scenario)
    assert len(res) == 1


@responses.activate
def test_list_scenarios_for_cdm(
    mock_avoidance_client: AvoidanceClient, mock_scenario: Scenario
):
    mock_scenario_json = json.dumps(
        {
            "pagination": {"offset": 0, "count": 1},
            "items": [mock_scenario.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/avoidance_maneuvers",
        json=json.loads(mock_scenario_json),
    )
    res = mock_avoidance_client.list_scenarios_for_cdm(1234567, count=1)
    assert isinstance(res[0], Scenario)
    assert len(res) == 1


@responses.activate
def test_get_scenario(mock_avoidance_client: AvoidanceClient, mock_scenario: Scenario):
    mock_scenario_json = json.dumps(
        mock_scenario.dict(),
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/avoidance_maneuvers/{mock_scenario.id}",
        json=json.loads(mock_scenario_json),
    )
    res = mock_avoidance_client.get_scenario(mock_scenario.id)
    assert isinstance(res, Scenario)


@responses.activate
def test_list_tradespaces(
    mock_avoidance_client: AvoidanceClient,
    mock_scenario: Scenario,
    mock_tradespace: Tradespace,
):
    mock_tradespace_json = json.dumps(
        {
            "pagination": {"offset": 0, "count": 1},
            "items": [mock_tradespace.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/avoidance_maneuvers/{mock_scenario.id}/tradespaces",
        json=json.loads(mock_tradespace_json),
    )

    res = mock_avoidance_client.list_tradespaces(scenario_id=mock_scenario.id, count=1)
    assert isinstance(res[0], Tradespace)
    assert len(res) == 1


@responses.activate
def test_get_tradespaces(
    mock_avoidance_client: AvoidanceClient,
    mock_scenario: Scenario,
    mock_tradespace: Tradespace,
):
    mock_tradespace_json = json.dumps(
        mock_tradespace.dict(),
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/avoidance_maneuvers/{mock_scenario.id}/tradespaces/{mock_tradespace.id}",
        json=json.loads(mock_tradespace_json),
    )

    res = mock_avoidance_client.get_tradespace(
        scenario_id=mock_scenario.id, tradespace_id=mock_tradespace.id
    )
    assert isinstance(res, Tradespace)


@responses.activate
def test_list_maneuver_plans(
    mock_avoidance_client: AvoidanceClient,
    mock_scenario: Scenario,
    mock_tradespace: Tradespace,
    mock_maneuver_plan: ManeuverPlan,
):
    mock_maneuver_plan_data = {
        "pagination": {"offset": 0, "count": 1},
        "items": [mock_maneuver_plan.dict()],
        "total_count": 1,
    }
    for item in mock_maneuver_plan_data["items"]:
        item["maneuvers_preview"] = item["maneuvers"]
    mock_maneuver_plan_json = json.dumps(
        mock_maneuver_plan_data,
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/avoidance_maneuvers/{mock_scenario.id}/tradespaces/{mock_tradespace.id}/plans",
        json=json.loads(mock_maneuver_plan_json),
    )

    res = mock_avoidance_client.list_maneuver_plans(
        scenario_id=mock_scenario.id, tradespace_id=mock_tradespace.id
    )
    assert isinstance(res[0], ManeuverPlan)


@responses.activate
def test_get_maneuver_plan(
    mock_avoidance_client: AvoidanceClient,
    mock_scenario: Scenario,
    mock_tradespace: Tradespace,
    mock_maneuver_plan: ManeuverPlan,
):
    mock_tradespace_json = json.dumps(
        mock_maneuver_plan.dict(),
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/avoidance_maneuvers/{mock_scenario.id}/tradespaces/{mock_tradespace.id}/plans/{mock_maneuver_plan.id}",
        json=json.loads(mock_tradespace_json),
    )

    res = mock_avoidance_client.get_maneuver_plan(
        scenario_id=mock_scenario.id,
        tradespace_id=mock_tradespace.id,
        plan_id=mock_maneuver_plan.id,
    )
    assert isinstance(res, ManeuverPlan)


@responses.activate
def test_create_maneuver_plan_screening(
    mock_avoidance_client: AvoidanceClient,
    mock_scenario: Scenario,
    mock_tradespace: Tradespace,
    mock_maneuver_plan: ManeuverPlan,
    mock_gemini_screening: GeminiScreening,
    mock_screening: Screening,
):
    mock_gemini_screening_json = json.dumps(
        mock_gemini_screening.dict(),
        default=str,
    )

    mock_screening_json = json.dumps(mock_screening.dict(), default=str)

    responses.add(
        responses.POST,
        f"{mock_base_url}/avoidance_maneuvers/{mock_scenario.id}/tradespaces/{mock_tradespace.id}/plans/{mock_maneuver_plan.id}/screening",
        json=json.loads(mock_gemini_screening_json),
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/screenings/{mock_screening.id}",
        json=json.loads(mock_screening_json),
    )
    res = mock_avoidance_client.create_maneuver_plan_screening(
        scenario_id=mock_scenario.id,
        tradespace_id=mock_tradespace.id,
        plan_id=mock_maneuver_plan.id,
    )
    assert isinstance(res, Screening)


@responses.activate
def test_get_maneuver_plan_screening(
    mock_avoidance_client: AvoidanceClient,
    mock_scenario: Scenario,
    mock_tradespace: Tradespace,
    mock_maneuver_plan: ManeuverPlan,
    mock_gemini_screening: GeminiScreening,
    mock_screening: Screening,
):
    mock_gemini_screening_json = json.dumps(
        mock_gemini_screening.dict(),
        default=str,
    )

    mock_screening_json = json.dumps(mock_screening.dict(), default=str)

    responses.add(
        responses.GET,
        f"{mock_base_url}/avoidance_maneuvers/{mock_scenario.id}/tradespaces/{mock_tradespace.id}/plans/{mock_maneuver_plan.id}/screening",
        json=json.loads(mock_gemini_screening_json),
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/screenings/{mock_screening.id}",
        json=json.loads(mock_screening_json),
    )

    res = mock_avoidance_client.get_maneuver_plan_screening(
        scenario_id=mock_scenario.id,
        tradespace_id=mock_tradespace.id,
        plan_id=mock_maneuver_plan.id,
    )
    assert isinstance(res, Screening)


@responses.activate
def test_download_maneuver_plan(
    mock_avoidance_client: AvoidanceClient,
    mock_scenario: Scenario,
    mock_tradespace: Tradespace,
    mock_maneuver_plan: ManeuverPlan,
):
    mock_string_io_json = json.dumps(StringIO.__dict__, default=str)

    responses.add(
        responses.GET,
        f"{mock_base_url}/avoidance_maneuvers/{mock_scenario.id}/tradespaces/{mock_tradespace.id}/plans/{mock_maneuver_plan.id}/screening/file",
        json=json.loads(mock_string_io_json),
    )
    resp = mock_avoidance_client.download_maneuver_plan(
        mock_scenario.id,
        mock_tradespace.id,
        mock_maneuver_plan.id,
        file_format="opm",
        file_name="test.txt",
    )
    assert isinstance(resp, StringIO)


@responses.activate
def test_create_scenario(
    mock_avoidance_client: AvoidanceClient,
    mock_scenario: Scenario,
    mock_scenario_config: ScenarioConfiguration,
):
    mock_scenario_json = json.dumps(
        mock_scenario.dict(),
        default=str,
    )

    responses.add(
        responses.POST,
        f"{mock_base_url}/avoidance_maneuvers",
        json=json.loads(mock_scenario_json),
    )
    res = mock_avoidance_client.create_scenario(config=mock_scenario_config)
    assert isinstance(res, Scenario)


@responses.activate
def test_create_scenario_with_tradespace_configuration(
    mock_avoidance_client: AvoidanceClient,
    mock_scenario: Scenario,
    mock_tradespace_config_run: TradespaceRun,
):
    mock_scenario_json = json.dumps(
        mock_scenario.dict(),
        default=str,
    )

    mock_cdm_key = "CDM_1"

    responses.add(
        responses.PUT,
        f"{mock_base_url}/avoidance_maneuvers/cdm/{mock_cdm_key}/generate",
        json=json.loads(mock_scenario_json),
    )
    res = mock_avoidance_client.create_scenario_with_tradespace_configuration(
        cdm_key_or_id=mock_cdm_key, config=mock_tradespace_config_run
    )
    assert isinstance(res, Scenario)


@responses.activate
def test_add_scenario_tradespace(
    mock_avoidance_client: AvoidanceClient,
    mock_scenario: Scenario,
    mock_tradespace: Tradespace,
    mock_tradespace_config: TradespaceConfiguration,
):

    mock_tradespace_json = json.dumps(
        mock_tradespace.dict(),
        default=str,
    )

    responses.add(
        responses.POST,
        f"{mock_base_url}/avoidance_maneuvers/{mock_scenario.id}/tradespaces",
        json=json.loads(mock_tradespace_json),
    )

    res = mock_avoidance_client.add_scenario_tradespace(
        scenario_id=mock_scenario.id, config=mock_tradespace_config
    )
    assert isinstance(res, Tradespace)


@responses.activate
def test_append_scenario_tradespace(
    mock_avoidance_client: AvoidanceClient,
    mock_scenario: Scenario,
    mock_tradespace_config_run: TradespaceRun,
):

    mock_scenario_json = json.dumps(
        mock_scenario.dict(),
        default=str,
    )

    responses.add(
        responses.POST,
        f"{mock_base_url}/avoidance_maneuvers/{mock_scenario.id}/tradespaces/append",
        json=json.loads(mock_scenario_json),
    )

    res = mock_avoidance_client.append_scenario_tradespace(
        scenario_id=mock_scenario.id, config=mock_tradespace_config_run
    )
    assert isinstance(res, Scenario)


@responses.activate
def test_list_maneuver_profiles(
    mock_avoidance_client: AvoidanceClient, mock_avoidance_maneuver_profile: Profile
):
    mock_profile_json = json.dumps(
        {
            "pagination": {"offset": 0, "count": 1},
            "items": [mock_avoidance_maneuver_profile.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/profiles",
        json=json.loads(mock_profile_json),
    )
    res = mock_avoidance_client.list_maneuver_profiles(count=1)
    assert isinstance(res[0], Profile)
    assert len(res) == 1


@responses.activate
def test_get_maneuver_profile(
    mock_avoidance_client: AvoidanceClient, mock_avoidance_maneuver_profile: Profile
):
    mock_profile_json = json.dumps(
        mock_avoidance_maneuver_profile.dict(),
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/profiles/{mock_avoidance_maneuver_profile.id}",
        json=json.loads(mock_profile_json),
    )
    res = mock_avoidance_client.get_maneuver_profile(mock_avoidance_maneuver_profile.id)
    assert isinstance(res, Profile)


@responses.activate
def test_create_maneuver_profile(
    mock_avoidance_client: AvoidanceClient,
    mock_avoidance_maneuver_profile: Profile,
    mock_avoidance_maneuver_profile_config: ProfileConfiguration,
):
    mock_profile_json = json.dumps(
        mock_avoidance_maneuver_profile.dict(),
        default=str,
    )

    responses.add(
        responses.POST,
        f"{mock_base_url}/profiles",
        json=json.loads(mock_profile_json),
    )
    res = mock_avoidance_client.create_maneuver_profile(
        config=mock_avoidance_maneuver_profile_config
    )
    assert isinstance(res, Profile)


@responses.activate
def test_update_maneuver_profile(
    mock_avoidance_client: AvoidanceClient,
    mock_avoidance_maneuver_profile: Profile,
    mock_avoidance_maneuver_profile_config: ProfileConfiguration,
):
    mock_profile_json = json.dumps(
        mock_avoidance_maneuver_profile.dict(),
        default=str,
    )

    responses.add(
        responses.PATCH,
        f"{mock_base_url}/profiles/{mock_avoidance_maneuver_profile.id}",
        json=json.loads(mock_profile_json),
    )
    res = mock_avoidance_client.update_maneuver_profile(
        profile_id=mock_avoidance_maneuver_profile.id,
        config=mock_avoidance_maneuver_profile_config,
    )
    assert isinstance(res, Profile)


@responses.activate
def test_delete_maneuver_profile(
    mock_avoidance_client: AvoidanceClient, mock_avoidance_maneuver_profile: Profile
):
    mock_profile_json = json.dumps(
        mock_avoidance_maneuver_profile.dict(),
        default=str,
    )

    responses.add(
        responses.DELETE,
        f"{mock_base_url}/profiles/{mock_avoidance_maneuver_profile.id}",
        json=json.loads(mock_profile_json),
    )
    res = mock_avoidance_client.delete_maneuver_profile(
        mock_avoidance_maneuver_profile.id
    )
    assert res == 200


@responses.activate
def test_create_ephemeris_from_maneuver_plan(
    mock_avoidance_client: AvoidanceClient,
    mock_scenario: Scenario,
    mock_tradespace: Tradespace,
    mock_maneuver_plan: ManeuverPlan,
    mock_ephemeris: Ephemeris,
):
    mock_ephemeris_json = json.dumps(mock_ephemeris.dict())

    responses.add(
        responses.PUT,
        f"{mock_base_url}/avoidance_maneuvers/{mock_scenario.id}/tradespaces/{mock_tradespace.id}/plans/{mock_maneuver_plan.id}/create_ephemeris",
        json=json.loads(mock_ephemeris_json),
    )

    res = mock_avoidance_client.create_ephemeris_from_maneuver_plan(
        scenario_id=mock_scenario.id,
        tradespace_id=mock_tradespace.id,
        plan_id=mock_maneuver_plan.id,
    )
    assert isinstance(res, Ephemeris)


@responses.activate
def test_list_triggers(mock_avoidance_client: AvoidanceClient, mock_trigger: Trigger):
    mock_trigger_json = json.dumps(
        {
            "pagination": {"offset": 0, "count": 1},
            "items": [mock_trigger.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/triggers",
        json=json.loads(mock_trigger_json),
    )
    res = mock_avoidance_client.list_triggers(count=1)
    assert isinstance(res[0], Trigger)
    assert len(res) == 1


@responses.activate
def test_get_trigger(mock_avoidance_client: AvoidanceClient, mock_trigger: Trigger):
    mock_trigger_json = json.dumps(
        mock_trigger.dict(),
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/triggers/{mock_trigger.id}",
        json=json.loads(mock_trigger_json),
    )
    res = mock_avoidance_client.get_trigger(mock_trigger.id)
    assert isinstance(res, Trigger)


@responses.activate
def test_create_trigger(
    mock_avoidance_client: AvoidanceClient,
    mock_trigger: Trigger,
    mock_trigger_config: TriggerConfiguration,
):
    mock_trigger_json = json.dumps(
        mock_trigger.dict(),
        default=str,
    )

    responses.add(
        responses.POST,
        f"{mock_base_url}/triggers",
        json=json.loads(mock_trigger_json),
    )
    res = mock_avoidance_client.create_trigger(config=mock_trigger_config)
    assert isinstance(res, Trigger)


@responses.activate
def test_update_trigger(
    mock_avoidance_client: AvoidanceClient,
    mock_trigger: Trigger,
    mock_trigger_config: TriggerConfiguration,
):
    mock_trigger_json = json.dumps(
        mock_trigger.dict(),
        default=str,
    )

    responses.add(
        responses.PATCH,
        f"{mock_base_url}/triggers/{mock_trigger.id}",
        json=json.loads(mock_trigger_json),
    )
    res = mock_avoidance_client.update_trigger(
        trigger_id=mock_trigger.id,
        config=mock_trigger_config,
    )
    assert isinstance(res, Trigger)


@responses.activate
def test_delete_trigger(mock_avoidance_client: AvoidanceClient, mock_trigger: Trigger):
    mock_trigger_json = json.dumps(
        mock_trigger.dict(),
        default=str,
    )

    responses.add(
        responses.DELETE,
        f"{mock_base_url}/triggers/{mock_trigger.id}",
        json=json.loads(mock_trigger_json),
    )
    res = mock_avoidance_client.delete_trigger(mock_trigger.id)
    assert res == 200
