import json

import responses


from kayhan.sdk.asset_management.asset_management_client import AssetManagementClient
from kayhan.sdk.asset_management.models import (
    ConstellationConfiguration,
    Constellation,
    RSOConfiguration,
    RSOConstellationUpdate,
    RSO,
)

mock_base_url = "http://test.app.kayhan.io"


@responses.activate
def test_get_constellation(
    mock_asset_management_client: AssetManagementClient,
    mock_constellation: Constellation,
):
    mock_constellation_json = json.dumps(
        mock_constellation.dict(),
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/constellations/{mock_constellation.slug}",
        json=json.loads(mock_constellation_json),
    )
    res = mock_asset_management_client.get_constellation(mock_constellation.slug)
    assert isinstance(res, Constellation)


@responses.activate
def test_get_default_constellation(
    mock_asset_management_client: AssetManagementClient,
    mock_constellation: Constellation,
):
    mock_constellation_json = json.dumps(
        mock_constellation.dict(),
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/profile/organization/default_constellation",
        json=json.loads(mock_constellation_json),
    )
    res = mock_asset_management_client.get_default_constellation()
    assert isinstance(res, Constellation)


@responses.activate
def test_create_constellation(
    mock_asset_management_client: AssetManagementClient,
    mock_constellation: Constellation,
    mock_constellation_config: ConstellationConfiguration,
):
    mock_constellation_config_json = json.dumps(
        mock_constellation.dict(),
        default=str,
    )

    responses.add(
        responses.POST,
        f"{mock_base_url}/profile/organization/constellations",
        json=json.loads(mock_constellation_config_json),
    )
    res = mock_asset_management_client.create_constellation(
        config=mock_constellation_config
    )
    assert isinstance(res, Constellation)


@responses.activate
def test_update_constellation(
    mock_asset_management_client: AssetManagementClient,
    mock_constellation: Constellation,
    mock_constellation_config: ConstellationConfiguration,
):
    mock_constellation_json = json.dumps(
        mock_constellation.dict(),
        default=str,
    )

    responses.add(
        responses.PATCH,
        f"{mock_base_url}/constellations/{mock_constellation.slug}",
        json=json.loads(mock_constellation_json),
    )
    res = mock_asset_management_client.update_constellation(
        constellation_slug=mock_constellation.slug, config=mock_constellation_config
    )
    assert isinstance(res, Constellation)


@responses.activate
def test_delete_constellation(
    mock_asset_management_client: AssetManagementClient,
    mock_constellation: Constellation,
):
    mock_constellation_config = json.dumps(
        mock_constellation.dict(),
        default=str,
    )

    responses.add(
        responses.DELETE,
        f"{mock_base_url}/constellations/{mock_constellation.slug}",
        json=json.loads(mock_constellation_config),
    )
    res = mock_asset_management_client.delete_constellation(mock_constellation.slug)
    assert res == 200


@responses.activate
def test_list_constellations(
    mock_asset_management_client: AssetManagementClient,
    mock_constellation: Constellation,
):
    mock_constellation_json = json.dumps(
        {
            "pagination": {"offset": 0, "count": 1},
            "items": [mock_constellation.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/profile/organization/constellations",
        json=json.loads(mock_constellation_json),
    )
    res = mock_asset_management_client.list_organization_constellations(count=1)
    assert isinstance(res[0], Constellation)
    assert len(res) == 1


@responses.activate
def test_list_constellation_rsos(
    mock_asset_management_client: AssetManagementClient,
    mock_constellation: Constellation,
    mock_rso: RSO,
):
    mock_rso_json = json.dumps(
        {
            "pagination": {"offset": 0, "count": 1},
            "items": [mock_rso.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/constellations/{mock_constellation.slug}/rsos",
        json=json.loads(mock_rso_json),
    )
    res = mock_asset_management_client.list_constellation_rsos(
        count=1, constellation_slug=mock_constellation.slug
    )
    assert isinstance(res[0], RSO)
    assert len(res) == 1


@responses.activate
def test_get_rso(
    mock_asset_management_client: AssetManagementClient,
    mock_rso: RSO,
):
    mock_rso_json = json.dumps(
        mock_rso.dict(),
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/rsos/{mock_rso.id}",
        json=json.loads(mock_rso_json),
    )
    res = mock_asset_management_client.get_rso(rso_id=mock_rso.id)
    assert isinstance(res, RSO)


@responses.activate
def test_list_rsos(
    mock_asset_management_client: AssetManagementClient,
    mock_rso: RSO,
):
    mock_rso_json = json.dumps(
        {
            "pagination": {"offset": 0, "count": 1},
            "items": [mock_rso.dict()],
            "total_count": 1,
        },
        default=str,
    )

    responses.add(
        responses.GET,
        f"{mock_base_url}/rsos",
        json=json.loads(mock_rso_json),
    )
    res = mock_asset_management_client.list_rsos(
        count=1,
    )
    assert isinstance(res[0], RSO)
    assert len(res) == 1


@responses.activate
def test_batch_update_rsos(
    mock_asset_management_client: AssetManagementClient,
    mock_rso: RSO,
    mock_rso_config: RSOConfiguration,
):
    mock_rso_json = json.dumps(
        [mock_rso.dict()],
        default=str,
    )

    responses.add(
        responses.PATCH,
        f"{mock_base_url}/rsos",
        json=json.loads(mock_rso_json),
    )
    res = mock_asset_management_client.batch_update_rsos(rsos_config=[mock_rso_config])
    assert isinstance(res[0], RSO)
    assert len(res) == 1
