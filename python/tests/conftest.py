import io
import kayhan
import pytest
import responses

from datetime import datetime
from uuid import uuid4

from kayhan.sdk.administration.administration_client import AdministrationClient
from kayhan.sdk.administration.models import (
    APIKey,
    DirectoryType,
    Organization,
    OrganizationConfiguration,
    OrganizationDirectoryRead,
    OrgLevel,
    OrgType,
    ServiceTier,
    Role,
    UserProfile,
    UserProfileConfiguration,
)
from kayhan.sdk.asset_management.asset_management_client import AssetManagementClient
from kayhan.sdk.asset_management.models import (
    ConstellationConfiguration,
    Constellation,
    RSOConfiguration,
    RSOConstellationUpdate,
    RSO,
    SpacecraftSafetyClassification,
)
from kayhan.sdk.avoidance.avoidance_client import AvoidanceClient
from kayhan.sdk.avoidance.models import (
    BurnType,
    DragProfile,
    ManeuverAlgorithm,
    ProfileConfiguration,
    TradespaceAlgorithm,
    ManeuverPlan,
    Scenario,
    ScenarioConfiguration,
    Tradespace,
    TradespaceFidelityType,
    TradespaceConfiguration,
    Profile,
    JobStatus,
    TradespaceRun,
    RSOPhysicalAttributes,
    TriggerConfiguration,
    Trigger,
    TriggerType,
)
from kayhan.sdk.client import KayhanClient
from kayhan.sdk.propagation.models import Propagation
from kayhan.sdk.screening.models import (
    Catalog,
    Conjunction,
    Ephemeris,
    EphemerisSource,
    GeminiScreening,
    Screenable,
    Screening,
)
from kayhan.sdk.screening.screening_client import ScreeningClient
from kayhan.sdk.settings import Settings, settings


@pytest.fixture(autouse=True)
def mock_client() -> kayhan.KayhanClient:
    with KayhanClient() as client:
        Settings.api_base_url = "http://test.app.kayhan.io"
        settings.api_ca_base_url_override = "http://test.app.kayhan.io"
        settings.api_ca_prefix = "/"
        settings.auth_method = "token"

        responses.add(
            responses.POST,
            "https://ksc.auth0.com/oauth/token",
            json={"access_token": "mock_token"},
        )
        return client


@pytest.fixture(autouse=True)
def mock_administration_client():
    with KayhanClient() as client:
        Settings.api_base_url = "http://test.app.kayhan.io"
        settings.api_authorization_base_url_override = "http://test.app.kayhan.io"
        settings.auth_method = "token"
        settings.api_authorization_prefix = "/"

        responses.add(
            responses.POST,
            "https://ksc.auth0.com/oauth/token",
            json={"access_token": "mock_token"},
        )
        mock_client = AdministrationClient(client)
        return mock_client


@pytest.fixture(autouse=True)
def mock_asset_management_client():
    with KayhanClient() as client:
        Settings.api_base_url = "http://test.app.kayhan.io"
        settings.api_authorization_base_url_override = "http://test.app.kayhan.io"
        settings.auth_method = "token"
        settings.api_authorization_prefix = "/"

        responses.add(
            responses.POST,
            "https://ksc.auth0.com/oauth/token",
            json={"access_token": "mock_token"},
        )
        mock_client = AssetManagementClient(client)
        return mock_client


@pytest.fixture(autouse=True)
def mock_avoidance_client():
    with KayhanClient() as client:
        Settings.api_base_url = "http://test.app.kayhan.io"
        settings.api_avm_base_url_override = "http://test.app.kayhan.io"
        settings.auth_method = "token"
        settings.api_avm_prefix = "/"

        responses.add(
            responses.POST,
            "https://ksc.auth0.com/oauth/token",
            json={"access_token": "mock_token"},
        )
        mock_client = AvoidanceClient(client)
        return mock_client


@pytest.fixture(autouse=True)
def mock_screening_client():
    with KayhanClient() as client:
        Settings.api_base_url = "http://test.app.kayhan.io"
        settings.api_ca_base_url_override = "http://test.app.kayhan.io"
        settings.auth_method = "token"
        settings.api_ca_prefix = "/"

        responses.add(
            responses.POST,
            "https://ksc.auth0.com/oauth/token",
            json={"access_token": "mock_token"},
        )
        mock_client = ScreeningClient(client)
        return mock_client


@pytest.fixture(autouse=True)
def mock_catalog():
    return Catalog(
        id="mock_id",
        catalog_type="SP",
        archived=False,
        ready=False,
        epoch=datetime.now(),
    )


@pytest.fixture(autouse=True)
def mock_screening():
    return Screening(id="mock_id", status="CREATED", created_at=datetime.now())


@pytest.fixture(autouse=True)
def mock_ephemeris():
    return Ephemeris(id="mock_id", ephemeris_source=EphemerisSource.CATALOG)


@pytest.fixture(autouse=True)
def mock_conjunction():
    return Conjunction(id="mock_id", tca=datetime.now(), miss_distance_km=1.0)


@pytest.fixture(autouse=True)
def mock_screenable(mock_ephemeris: Ephemeris):
    return Screenable(ephemeris_id="mock_id", ephemeris=mock_ephemeris)


@pytest.fixture(autouse=True)
def mock_propagation():
    return Propagation(
        id="mock_id",
        status="CREATED",
        created_at=datetime.now(),
    )


@pytest.fixture(autouse=True)
def mock_scenario():
    return Scenario(
        id="mock_id",
        cdm_id=1,
        created_at=datetime.now(),
        asset_norad_cat_id=12345,
        autogenerated=True,
        name="Mock Scenario",
        cdm_key="CDM_1",
        status=JobStatus.PENDING,
    )


@pytest.fixture(autouse=True)
def mock_tradespace():
    return Tradespace(
        id="mock_id",
        scenario_id="mock_id",
        start_time=datetime.now(),
        end_time=datetime.now(),
        fidelity_type=TradespaceFidelityType.PODAL.value,
        fidelity_value=1.0,
        fidelity_unit="m",
        tradespace_algorithm=TradespaceAlgorithm.ORBIT_INTERVAL.value,
        algorithm_type=ManeuverAlgorithm.FIXED_DV.value,
    )


@pytest.fixture(autouse=True)
def mock_maneuver_plan():
    return ManeuverPlan(
        id="mock_id",
        collision_probability=0.01,
        miss_distance_km=1.1,
        tca=datetime.now(),
        conjunction_out_of_bounds=False,
        maneuvers_count=0,
        maneuvers=[],
    )


@pytest.fixture(autouse=True)
def mock_avoidance_maneuver_profile():
    return Profile(
        id="mock_id",
        name="Mock Profile",
        maneuver_algo_type=ManeuverAlgorithm.DIFF_DRAG,
        burn_type=BurnType.IMPULSIVE,
        tradespace_algorithm=TradespaceAlgorithm.ORBIT_INTERVAL,
        fidelity_type=TradespaceFidelityType.PODAL,
        fidelity_value=1.0,
        fidelity_unit="m",
        organization_id="1",
        maneuver_algo_id="1",
        maneuver_algo={
            "drag_profile": DragProfile.LOW,
            "max_drag_time_h": 12,
            "drag_area_min_m_2": 1.0,
            "drag_area_max_m_2": 2.0,
        },
    )


@pytest.fixture(autouse=True)
def mock_scenario_config():
    return ScenarioConfiguration(
        name="mock_scenario",
        autogenerated=True,
        cdm_id=0,
        constellation="test_constellation",
        asset_id="mock_id",
        asset_norad_cat_id=12345,
        cdm_key="CDM_0",
    )


@pytest.fixture(autouse=True)
def mock_tradespace_config():
    return TradespaceConfiguration(
        start_time=datetime.now(),
        end_time=datetime.now(),
        fidelity_type=TradespaceFidelityType.PODAL.value,
        fidelity_value=1.0,
        fidelity_unit="m",
        tradespace_algorithm=TradespaceAlgorithm.ORBIT_INTERVAL.value,
        algorithm_type=ManeuverAlgorithm.FIXED_DV.value,
    )


@pytest.fixture(autouse=True)
def mock_tradespace_config_run():
    return TradespaceRun(
        profile_id=uuid4(),
        rso_physical_attributes=RSOPhysicalAttributes(
            mass_kg=1.0, thrust_n=1.0, isp_s=1.0, override_hbr_m=5.0
        ),
    )


@pytest.fixture(autouse=True)
def mock_avoidance_maneuver_profile_config():
    return ProfileConfiguration(
        name="profile",
        maneuver_algo_type=ManeuverAlgorithm.DIFF_DRAG,
        burn_type=BurnType.IMPULSIVE,
        tradespace_algorithm=TradespaceAlgorithm.ORBIT_INTERVAL,
        fidelity_type=TradespaceFidelityType.PODAL,
        fidelity_unit="m",
        fidelity_value=1,
        maneuver_algo={
            "drag_profile": DragProfile.LOW,
            "max_drag_time_h": 12,
            "drag_area_min_m_2": 1.0,
            "drag_area_max_m_2": 2.0,
        },
    )


@pytest.fixture(autouse=True)
def mock_trigger_config():
    return TriggerConfiguration(
        title="trigger",
        is_paused=False,
        types=[
            {
                "type": TriggerType.AVOIDANCE_MANEUVER,
                "max_miss_distance": 1,
                "max_miss_distance_unit": "km",
            }
        ],
        assets=[{"constellation_slug": "slug 1", "link_constellation": True}],
    )


@pytest.fixture(autouse=True)
def mock_trigger():
    return Trigger(
        id="mock_trigger",
        triggered_count=1,
        is_paused=False,
        created_at=datetime.now(),
        types=[
            {
                "type": TriggerType.AVOIDANCE_MANEUVER,
                "max_miss_distance": 1,
                "max_miss_distance_unit": "km",
            }
        ],
        assets=[{"constellation_slug": "slug 1", "link_constellation": True}],
    )


@pytest.fixture(autouse=True)
def mock_gemini_screening():
    return GeminiScreening(
        id="mock_id",
        status="TEST",
        conjunctions_under_thresholds_count=1,
    )


@pytest.fixture()
def mock_opm_file() -> io.StringIO:
    opm_file = """CCSDS_OPM_VERS = 2.0

CREATION_DATE = 2000-01-01T00:00:00.000
ORIGINATOR = KAYHAN

OBJECT_NAME = TEST-SPACECRAFT
OBJECT_ID = 2000-000A
CENTER_NAME = EARTH
REF_FRAME = ITRF-93
TIME_SYSTEM = UTC

COMMENT State Vector
EPOCH = 2006-06-03T00:00:00.000
X = 6500.000 [km]
Y = -40000.000 [km]
Z = -80.000 [km]
X_DOT = 3.000 [km/s]
Y_DOT = 0.000 [km/s]
Z_DOT = 0.000 [km/s]

COMMENT Spacecraft parameters
MASS = 1000.000 [kg]
SOLAR_RAD_AREA = 10.000 [m**2]
SOLAR_RAD_COEFF = 1.300
DRAG_AREA = 10.000 [m**2]
DRAG_COEFF = 2.00

MAN_EPOCH_IGNITION = 2000-00-00T01:00:00.000
MAN_DURATION = 100.00 [s]
MAN_DELTA_MASS = -10.000 [kg]
MAN_REF_FRAME = RTN
MAN_DV_1 = 0.00 [km/s]
MAN_DV_2 = 0.01 [km/s]
MAN_DV_3 = 0.00 [km/s]
"""
    return io.StringIO(opm_file)


@pytest.fixture(autouse=True)
def mock_user_profile_config():
    return UserProfileConfiguration(
        role=Role.USER,
        username="test",
    )


@pytest.fixture(autouse=True)
def mock_user_profile():
    return UserProfile(
        id=uuid4(),
        created_at=datetime.now(),
        role=Role.USER,
    )


@pytest.fixture(autouse=True)
def mock_user_organization_config():
    return OrganizationConfiguration(contact_name="test")


@pytest.fixture(autouse=True)
def mock_user_organization():
    return Organization(
        id="test-org",
        created_at=datetime.now(),
        name="test org",
        slug="test org",
        space_track_org_name="test org",
        level=OrgLevel.STANDARD,
        service_tier=ServiceTier.ESSENTIALS,
        org_type=OrgType.COMMERCIAL,
        rso_count=0,
        ssa_sharing_agreement=False,
    )


@pytest.fixture(autouse=True)
def mock_organization_directory():
    return OrganizationDirectoryRead(
        id=uuid4(),
        name="test org",
        slug="test org",
        directory_name="test org",
        type=DirectoryType.ORGANIZATION,
    )


@pytest.fixture(autouse=True)
def mock_api_key():
    return APIKey(
        id=uuid4(),
        client_id="test key",
        client_secret="test key secret",
        created_at=datetime.now(),
        username="test key",
    )


@pytest.fixture(autouse=True)
def mock_constellation_config():
    return ConstellationConfiguration(name="test constellation")


@pytest.fixture(autouse=True)
def mock_constellation(mock_user_organization):
    return Constellation(
        id=uuid4(),
        created_at=datetime.now(),
        organization=mock_user_organization,
        is_default=True,
        can_delete=False,
        rso_count=1,
        name="constellation-one",
    )


@pytest.fixture(autouse=True)
def mock_rso_constellation_config():
    return RSOConstellationUpdate(slug="test-constellation")


@pytest.fixture(autouse=True)
def mock_rso_config(mock_rso_constellation_config):
    return RSOConfiguration(
        norad_cat_id=12345,
        constellation=mock_rso_constellation_config,
        mass_kg=None,
        thrust_n=None,
        isp_s=None,
        override_maneuverable=False,
        override_hbr_m=None,
        srp_coefficient_m=None,
        srp_area_m=None,
        drag_coefficient_m=None,
        drag_area_m=None,
        mobility_type=None,
        on_board_autonomous=None,
    )


@pytest.fixture(autouse=True)
def mock_rso(mock_constellation):
    return RSO(
        id=uuid4(),
        norad_cat_id=12345,
        constellation=mock_constellation,
        maneuverable=None,
        directory_maneuverable=None,
        directory_last_updated=None,
        directory_status=None,
        object_name="12345",
        object_type=None,
        international_designator="12345",
        hbr_m=5,
        maneuverability_classification=SpacecraftSafetyClassification.UNDETERMINED,
        is_decayed=None,
        is_temporary=None,
        last_updated_by_user=None,
        satcat_last_updated=None,
    )
