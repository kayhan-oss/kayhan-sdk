import json
from io import StringIO
from pathlib import Path
import tempfile

import responses

import kayhan
from kayhan.sdk.propagation.models import Propagation

mock_base_url = "http://test.app.kayhan.io"

@responses.activate
def test_create_propagation_with_config(
    mock_client: kayhan.KayhanClient,
    mock_propagation: kayhan.Propagation,
):
    mock_propagation_json = mock_propagation.json()

    responses.add(
        responses.POST,
        f"{mock_base_url}/propagations",
        json=json.loads(mock_propagation_json),
    )

    config = kayhan.PropagationConfiguration(start_time=kayhan.utc("2023-01-01T00:00:00.000Z"))
    resp = mock_client.propagation.create_propagation(config)
    assert isinstance(resp, kayhan.Propagation)

@responses.activate
def test_create_propagation_with_opm_buf(
    mock_client: kayhan.KayhanClient,
    mock_propagation: kayhan.Propagation,
    mock_opm_file: StringIO,
):
    mock_propagation_json = mock_propagation.json()

    responses.add(
        responses.POST,
        f"{mock_base_url}/propagations/{mock_propagation.id}/opm",
        json=json.loads(mock_propagation_json),
    )

    responses.add(
        responses.POST,
        f"{mock_base_url}/propagations",
        json=json.loads(mock_propagation_json),
    )

    resp = mock_client.propagation.create_propagation(opm_file=mock_opm_file)
    assert isinstance(resp, kayhan.Propagation)

@responses.activate
def test_create_propagation_with_opm_path(
    mock_client: kayhan.KayhanClient,
    mock_propagation: kayhan.Propagation,
    mock_opm_file: StringIO,
):
    mock_propagation_json = mock_propagation.json()

    responses.add(
        responses.POST,
        f"{mock_base_url}/propagations/{mock_propagation.id}/opm",
        json=json.loads(mock_propagation_json),
    )

    responses.add(
        responses.POST,
        f"{mock_base_url}/propagations",
        json=json.loads(mock_propagation_json),
    )

    with tempfile.NamedTemporaryFile("w+") as f:
        mock_opm_file.seek(0)
        f.write(mock_opm_file.read())
        resp = mock_client.propagation.create_propagation(opm_file=f.name)
    assert isinstance(resp, kayhan.Propagation)

@responses.activate
def test_add_opm_to_propagation(
    mock_client: kayhan.KayhanClient,
    mock_propagation: kayhan.Propagation,
    mock_opm_file: StringIO,
):
    mock_propagation_json = mock_propagation.json()

    responses.add(
        responses.POST,
        f"{mock_base_url}/propagations/{mock_propagation.id}/opm",
        json=json.loads(mock_propagation_json),
    )

    resp = mock_client.propagation.add_opm_to_propagation(mock_propagation, mock_opm_file)

    assert isinstance(resp, kayhan.Propagation)