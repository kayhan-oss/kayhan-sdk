"""
Kayhan GAMUT
Sample Application - Launch Screening

Version: 0.1
Author: [Derek Strobel](derek@kayhan.space)

Brief: 
    Upload a set of CALIPERv2 launch trajectory files to Gamut and screen them
    against the latest SP Ephemeris catalog across a 15-minute launch window
    within the Catalog's usable time interval. Output the Conjunction results
    to the terminal as an ALL_SORTED conjunction results plaintext file.

Usage: 
    * Install Kayhan SDK ($ pip install -r requirements.txt)
    * Configure Kayhan SDK Authentication (see the Configuration section of the Kayhan SDK Documentation)
    * Define CALIPERv2 ephemeris files in the `ephemeris_uploads` list
    * If desired, modify the logic to define the `launch_window_start`, `launch_window_end`, and `threshold_radius_*` values
    * Run the program ($ python gamut_screening_sample.py)
"""

import datetime
import sys
import time
from pathlib import Path

from dataclasses import dataclass
import kayhan
from kayhan.cli.utils import cli_output_model, screening_await_cli, cli_output_list

@dataclass
class EphemerisUpload:
    filename: Path
    hbr_m: float
    file_format: str = "caliper"

# Define CALIPER v2.0 Launch trajectory ephemeris files, along with their corresponding Hard Body Radius (HBR) values
ephemeris_uploads = [
    EphemerisUpload("23_KAYHAN_DEMO_LAUNCH_Stage1_PostSeparation_COV.txt", 15.0),
    EphemerisUpload("23_KAYHAN_DEMO_LAUNCH_Stage2_PostSeparation_COV.txt", 10.0),
    EphemerisUpload("23_KAYHAN_DEMO_LAUNCH_Stage3_FailedRelight_COV.txt", 10.0),
    EphemerisUpload("23_KAYHAN_DEMO_LAUNCH_Stage3_PostBurn_COV.txt", 10.0),
]

def log_stage(s, end=''):
    print(f"[] {s}...", end=end)

def log_note(s):
    print(f"\t{s}")

def log_done():
    print(f"Done")

def upload_ephemerides(client: kayhan.KayhanClient):
    ephemerides = []
    for eph_upload in ephemeris_uploads:
        ephemerides.append(
             client.screening.create_ephemeris(
                eph_upload.filename,
                file_format=eph_upload.file_format,
                hbr_m=eph_upload.hbr_m,
            )
        )
        print(".", end='')
    return ephemerides

def main():
    with kayhan.KayhanClient() as client:
        # Upload Primary launch ephemerides describing the mission trajectories
        log_stage(f"Uploading primary launch ephemerides")
        ephemerides = upload_ephemerides(client)
        log_done()


        # Retrieve the metadata for the latest ephemeris catalog to screen against
        log_stage("Retrieving latest catalog")
        catalog = client.screening.list_catalogs(
            latest=True, filters=[{"field": "catalog_type", "value": "SP"}]
        )[0]
        log_done()
        log_note(f"Screening against {catalog.catalog_type} catalog {catalog.epoch.isoformat()}.")

        # Arbitrarily select a launch window which overlaps with the catalog
        # (Can replace with mission-specific launch window parameters)
        launch_window_start = catalog.epoch + datetime.timedelta(days=2)
        launch_window_end = launch_window_start + datetime.timedelta(minutes=15)
        launch_cadence_s = 1

        log_note("Screening launch window:")
        log_note(f"    Start: {launch_window_start.isoformat()}")
        log_note(f"    End: {launch_window_end.isoformat()}")
        log_note(f"    Cadence: {launch_cadence_s} s")


        # Create the configuration object for the screening,
        # including the Threshold radii to use
        configuration = kayhan.ScreeningConfiguration(
            title="Kayhan SDK Launch Screening Sample",
            threshold_radius_km=2.50,
            threshold_radius_active_km=25.0,
            threshold_radius_debris_km=25.0,
            threshold_radius_manned_km=200.0,
            screening_type="LAUNCH",
            launch_window_start=launch_window_start,
            launch_window_end=launch_window_end,
            launch_window_cadence_s=launch_cadence_s,
        )

        # Upload the screening to the Gamut server and submit to begin processing
        log_stage("Initializing Gamut Screening", end='\n')
        screening = client.screening.create_screening(
            config=configuration,
            primaries=ephemerides,
            secondaries=[catalog],
            submit=True,
        )
        log_done()

        # Refresh the updated screening metadata from the server and output it for the user
        screening = client.screening.get_screening(screening.id)
        cli_output_model(screening)

        # Poll the screening for completion and provide a progress bar command-line interface
        tst = time.time()
        screening = screening_await_cli(client, screening)
        tend = time.time()

        if screening.status == "SUCCESS":
            # Screening completed processing successfully
            log_note(f"Completed screening in {tend - tst:.2f} s.")

            # Download the conjunction results with PC above 1e-6 
            log_stage("Retrieving conjunction results")
            filters = [{"field": "collision_probability", "value": 1e-6, "op": "ge"}]
            conjunctions = client.screening.list_conjunctions(screening.id, filters, sort_field="launch_time", sort_direction="asc")
            log_done()

            # Output conjunction results as a table in the terminal
            if len(conjunctions) > 0:
                cli_output_list(conjunctions)
            else:
                log_note("No conjunctions found within reporting criteria.")
        else:
            # Screening encountered some runtime issue; output an error message to the user
            log_note(f"Screening ended in status {screening.status}.")
            sys.exit(-1)

main()
