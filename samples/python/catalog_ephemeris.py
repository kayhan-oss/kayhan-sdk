"""
This is a sample for using the Kayhan SDK to retrieve the SP
Ephemeris file from the latest catalog for a specific object
by its NORAD catalog ID as an OEM ephemeris. This code may be adapted or
transformed to fit the user's purpose.

Author: Derek Strobel (Kayhan Space) derek@kayhan.space
Usage: python catalog_ephemeris.py {norad_cat_id}

Example:
python catalog_ephemeris.py 31403

Result:
File `MEME_31403_SP_EPHEMERIS_{date of catalog}_OEM.txt is written to working directory
"""


import sys

from kayhan.sdk.client import KayhanClient
from kayhan.sdk.screening.models import Ephemeris


def get_latest_ephemeris(client: KayhanClient, norad_cat_id: int) -> Ephemeris:
    catalog = client.screening.get_latest_catalog()
    filters = [{"field": "norad_cat_id", "value": norad_cat_id}]
    ephemerides = client.screening.list_catalog_ephemerides(catalog.id, filters=filters)
    try:
        return ephemerides[0]
    except IndexError:
        raise ValueError(
            f"Ephemeris for object {norad_cat_id} not found in latest catalog."
        )


if __name__ == "__main__":
    norad_cat_id = int(sys.argv[-1])

    with KayhanClient() as client:
        ephemeris = get_latest_ephemeris(client, norad_cat_id)
        content = client.screening.download_ephemeris(ephemeris.id, "oem")
        epoch_jday = ephemeris.solution_time.strftime("%y%j")
        output_filename = f"MEME_{norad_cat_id}_SP_EPHEMERIS_{epoch_jday}_OEM.txt"

    with open(output_filename, "w+") as outf:
        outf.write(content.read())
