"""
This is a sample for using the Kayhan SDK to retrieve a 
covariance tuned Ephemeris file from the Kayhan servers for
a specific object by its NORAD ID and non-tuned covariance filename. 
This code may be adapted or transformed to fit the user's purpose.

Author: Jacob Garlick (Kayhan Space) jacob@kayhan.space
Usage: python ephemeris_with_tuned_covariance.py {norad_cat_id} {non_tuned_covariance_filename}

Example:
python ephemeris_with_tuned_covariance.py 5383 MEME_5383_Kayhan_operational_nomnvr_unclassified.txt

Result:
File `MEME_5383_Kayhan_operational_nomnvr_unclassified_tuned_example_OEM.txt is written to working directory
"""

import sys

from kayhan.sdk.client import KayhanClient
from kayhan.sdk.screening.models import Ephemeris


def get_ephemeris_with_tuned_covariance(
    client: KayhanClient, norad_cat_id: int, filename: str
) -> Ephemeris:
    filters = [
        {"field": "norad_cat_id", "value": norad_cat_id},
        {"field": "has_covariance", "value": True},
        {"field": "ephemeris_source", "value": "KAYHAN_INTERNAL_PROCESS"},
        {
            "field": "filename",
            "value": filename,
        },
    ]
    ephemerides = client.screening.list_ephemerides(filters=filters)
    try:
        return ephemerides[0]
    except IndexError:
        raise ValueError(
            f"Ephemeris with covariance for object {norad_cat_id} not found."
        )


if __name__ == "__main__":
    norad_cat_id = int(sys.argv[1])
    filename = sys.argv[2]

    with KayhanClient() as client:
        ephemeris = get_ephemeris_with_tuned_covariance(client, norad_cat_id, filename)
        content = client.screening.download_ephemeris(ephemeris.id, "oem")
        output_filename = f"{filename.rstrip('.txt')}_tuned_example_OEM.txt"

    with open(output_filename, "w+") as outf:
        outf.write(content.read())
